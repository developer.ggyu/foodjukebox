#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

cd $SHELL_PATH



fjboxconf=$(cat /home/pi/farmosv2-script/common_api/conf/config.json)

if [[ $fjboxconf == *backpathImg* ]] ; then
    echo -e "already added"
else
    backpathImg="    \"backpathImg\": \"backImg\","
    sed -i'' -r -e "/backupImage/a\\$backpathImg" /home/pi/farmosv2-script/common_api/conf/config.json
    echo -e "Add backpathImg"
fi

