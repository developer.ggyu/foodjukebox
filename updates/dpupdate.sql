SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- DATABASE 선택
USE farmos;

DROP TABLE observations;
DROP TABLE current_observations;
DROP TABLE device_field;
DROP TABLE experiment_comments;
DROP TABLE experiment_sample_map;
DROP TABLE experiment_sample;
DROP TABLE gallery;
DROP TABLE dataindexes;
DROP TABLE core_rule_applied;
DROP TABLE fields;
DROP TABLE devices;


-- ----------------------------
-- Table structure for core_rule_applied
-- ----------------------------
CREATE TABLE `core_rule_applied` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `field_id` int(11) DEFAULT NULL,
  `used` int(1) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `constraints` text,
  `configurations` text,
  `inputs` text,
  `controllers` text,
  `outputs` text,
  `autoapplying` int(1) NOT NULL DEFAULT '0',
  `sched` int(1) NOT NULL,
  `advanced` int(1) NOT NULL DEFAULT '0',
  `groupname` varchar(255) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_core_rule_applied_fields_1` (`field_id`),
  CONSTRAINT `fk_core_rule_applied_fields_1` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for current_observations
-- ----------------------------
CREATE TABLE `current_observations` (
  `data_id` int(11) NOT NULL,
  `obs_time` datetime NOT NULL,
  `nvalue` double DEFAULT NULL,
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`data_id`) USING BTREE,
  CONSTRAINT `fk_current_observations_dataindexes` FOREIGN KEY (`data_id`) REFERENCES `dataindexes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dataindexes
-- ----------------------------
CREATE TABLE `dataindexes` (
  `id` int(11) NOT NULL,
  `rule_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `sigdigit` int(11) DEFAULT '0',
  `device_id` int(11) DEFAULT NULL,
  `field_id` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for device_field
-- ----------------------------
CREATE TABLE `device_field` (
  `device_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `sort_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`device_id`,`field_id`),
  KEY `FK_device_field_field_id_fields_id` (`field_id`),
  CONSTRAINT `FK_device_field_device_id_devices_id` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  CONSTRAINT `FK_device_field_field_id_fields_id` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for devices
-- ----------------------------
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `spec` text NOT NULL,
  `gateid` varchar(255) NOT NULL,
  `coupleid` varchar(255) NOT NULL,
  `nodeid` int(11) NOT NULL,
  `compcode` int(11) NOT NULL,
  `devcode` int(11) DEFAULT NULL,
  `devindex` int(11) DEFAULT NULL,
  `nodetype` int(11) DEFAULT NULL,
  `deleted` int(1) DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for fields
-- ----------------------------
CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `fieldtype` text NOT NULL,
  `uiinfo` text,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_fields_farm_1` (`farm_id`),
  CONSTRAINT `fk_fields_farm_1` FOREIGN KEY (`farm_id`) REFERENCES `farm` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for observations
-- ----------------------------
CREATE TABLE `observations` (
  `data_id` int(11) NOT NULL,
  `obs_time` datetime NOT NULL,
  `nvalue` double DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`data_id`,`obs_time`),
  KEY `obs_index_dataid` (`data_id`),
  CONSTRAINT `fk_observations_dataindexes_1` FOREIGN KEY (`data_id`) REFERENCES `dataindexes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL,
  `obs_time` datetime NOT NULL,
  `path` varchar(255) NOT NULL,
  `meta` text,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_gallery_dataindexes` (`data_id`),
  CONSTRAINT `fk_gallery_dataindexes` FOREIGN KEY (`data_id`) REFERENCES `dataindexes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for experiment_comments
-- ----------------------------
CREATE TABLE `experiment_comments` (
  `data_id` int(11) NOT NULL,
  `obs_time` datetime NOT NULL,
  `nvalue` text,
  `source_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`data_id`,`obs_time`),
  CONSTRAINT `fk_observations_etc_dataindexes_1` FOREIGN KEY (`data_id`) REFERENCES `dataindexes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for experiment_sample
-- ----------------------------
CREATE TABLE `experiment_sample` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_id` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_experiment_sample_fields_1` (`field_id`),
  CONSTRAINT `fk_experiment_sample_fields_1` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for experiment_sample_map
-- ----------------------------
CREATE TABLE `experiment_sample_map` (
  `experiment_id` int(11) NOT NULL,
  `sample_id` int(11) NOT NULL,
  `deleted` int(1) DEFAULT '0',
  `sample_item` text,
  PRIMARY KEY (`experiment_id`,`sample_id`),
  KEY `fk_experiment_sample_map_experiment_1` (`experiment_id`) USING BTREE,
  KEY `fk_experiment_sample_map_experiment_sample_1` (`sample_id`),
  CONSTRAINT `fk_experiment_sample_map_experiment_1` FOREIGN KEY (`experiment_id`) REFERENCES `experiment` (`id`),
  CONSTRAINT `fk_experiment_sample_map_experiment_sample_1` FOREIGN KEY (`sample_id`) REFERENCES `experiment_sample` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO `fields` VALUES
(0,1,'외부','local','{\"index\":{\"local\":{\"isFull\":false,\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":\"max\",\"device\":{\"7\":[10000700,10000701],\"8\":[10000800,10000801]},\"data\":[],\"isWeather\":false},\"greenhouse\":{\"isFull\":true,\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":\"max\",\"device\":{},\"data\":[]},\"actuator\":{\"isFull\":true,\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":\"max\",\"device\":{},\"data\":[]}},\"dashboard\":{\"ventilation\":{\"idfmt\":{\"device\":\"\",\"data\":\"3[0-9][0-9][0-9][0-9][0-9]26\"},\"max\":1,\"device\":{},\"data\":[]},\"heating\":{\"idfmt\":{\"device\":\"\",\"data\":\"3[0-9][0-9][0-9][0-9][0-9]25\"},\"max\":1,\"device\":{},\"data\":[]},\"temp\":{\"idfmt\":{\"deviceType\":[\"temperature-sensor\",\"humidity-sensor\"],\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]1\",\"data\":\"\",\"timespan\":1},\"max\":\"max\",\"device\":{},\"data\":[],\"isFull\":false},\"retractable\":{\"isImage\":false,\"type\":{\"select\":\"time\",\"list\":[\"time\",\"ratio\"]},\"time\":{\"max\":5,\"idfmt\":{\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]4\",\"data\":\"\"},\"device\":{},\"data\":[],\"isFull\":false},\"ratio\":{\"max\":5,\"idfmt\":{\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]2\",\"data\":\"\"},\"device\":{},\"data\":[],\"isFull\":false}},\"switch\":{\"max\":5,\"idfmt\":{\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]4\",\"data\":\"\"},\"device\":{},\"data\":[],\"isFull\":false}},\"visible\":false}',0),
(1,1,'푸드 쥬크박스','greenhouse','{\"index\":{\"local\":{\"isFull\":true,\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":\"max\",\"device\":{},\"data\":[],\"isWeather\":true},\"greenhouse\":{\"isFull\":false,\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":\"max\",\"device\":{\"2\":[10000200,10000201],\"3\":[10000300,10000301,10000321],\"4\":[10000400,10000401],\"5\":[10000501,10000500],\"6\":[10000600,10000601]},\"data\":[100004,30100092,30100094,30100096,30100095,30100093,30120051]},\"actuator\":{\"isFull\":false,\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":\"max\",\"device\":{\"10\":[10001000,10001004],\"11\":[10001100,10001104],\"12\":[10001200,10001204],\"13\":[10001300,10001304],\"14\":[10001400,10001404],\"15\":[10001500,10001505,10001504],\"16\":[10001600,10001604,10001605],\"17\":[10001700,10001704,10001705],\"18\":[10001800]},\"data\":[]}},\"dashboard\":{\"ventilation\":{\"idfmt\":{\"device\":\"\",\"data\":\"3[0-9][0-9][0-9][0-9][0-9]26\"},\"max\":1,\"device\":{},\"data\":[30090026],\"isFull\":false},\"heating\":{\"idfmt\":{\"device\":\"\",\"data\":\"3[0-9][0-9][0-9][0-9][0-9]25\"},\"max\":1,\"device\":{},\"data\":[30090025],\"isFull\":false},\"temp\":{\"idfmt\":{\"deviceType\":[\"temperature-sensor\",\"humidity-sensor\"],\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]1\",\"data\":\"\",\"timespan\":3},\"max\":\"max\",\"device\":{\"2\":[10000201],\"7\":[10000701]},\"data\":[],\"isFull\":false,\"timespan\":4},\"retractable\":{\"isImage\":true,\"type\":{\"select\":\"time\",\"list\":[\"time\",\"ratio\"]},\"time\":{\"max\":5,\"idfmt\":{\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]4\",\"data\":\"\"},\"device\":{},\"data\":[],\"isFull\":false},\"ratio\":{\"max\":5,\"idfmt\":{\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]2\",\"data\":\"\"},\"device\":{},\"data\":[],\"isFull\":false}},\"switch\":{\"max\":5,\"idfmt\":{\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]4\",\"data\":\"\"},\"device\":{\"11\":[10001104],\"12\":[10001204],\"15\":[10001504],\"16\":[10001604],\"17\":[10001704]},\"data\":[],\"isFull\":false}},\"visible\":true}',0);

