#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

cd $SHELL_PATH



host=$(cat /etc/hostname)


fjboxserial=${host:5}

echo -e $fjboxserial

if [[ ${fjboxserial} -lt 1000 ]];then
    connServer=fjboxtest.jinong.co.kr
    temp=$fjboxserial
else
    temp=$(echo $fjboxserial | cut -c 2-)
    serverNum=`expr $temp / 15 + 1`
    if [ ${serverNum} -lt 10 ];then
        connServer=fjbox00$serverNum.farmos.co.kr
    else
        connServer=fjbox0$serverNum.farmos.co.kr
    fi
fi

if [[ ${fjboxserial} -lt 2000 ]];then
    rsyncHour=1
else
    rsyncHour=2
fi

rsyncMinute=$((`expr $temp % 15` * 4))

echo -e $rsyncMinute , $rsyncHour
echo -e $connServer

(crontab -l 2>/dev/null | sed 's/nidana/farmosv2-script\/nidana/' ) | crontab -
cat <(crontab -l) <(echo '10 0 * * * cd /home/pi/farmosv2-script/nidana; python3 boximgbackup.py') | crontab

if [ -f "/home/pi/nidana/data/*.csv" ] ; then
    mv /home/pi/nidana/data/*.csv /home/pi/farmosv2-script/nidana/data/
fi


rm -rf /home/pi/nidana

su - pi -c "
cat <(crontab -l) <(echo '$rsyncMinute $rsyncHour  * * * rsync -avzh -e \"ssh -p 22222\" /home/pi/farmosv2-script/nidana/data/ fjbox@$connServer:/home/fjbox/nidana/data/$fjboxserial') | crontab
"

