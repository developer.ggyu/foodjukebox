#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath

cd ../../
SHELL_PATH=`pwd -P`

echo -e $SHELL_PATH

if [ -d olddirectory ] ; then
    echo -e "check olddirectory" >> /home/pi/foodjukebox/updates/log/temp
    exit
fi

echo -e '\n\n 0. install library'

apt-get install libatlas-base-dev -y -q
pip3 install opencv-python
pip3 install --upgrade psutil
pip3 install scikit-learn

echo -e '\n\n 1. change repository'

function mvDirectory(){
    mv /home/pi/farmosv2-script/backup/ /home/pi/temp/.
    mv /home/pi/farmosv2-script/common_api/api/public/menu_m.json temp/.
    mv /home/pi/farmosv2-script/common_api/api/public/menu_pc.json temp/.
    mv /home/pi/farmosv2-script/common_api/conf/config.json temp/.
    mv /home/pi/farmosv2-script/cvtgate/fjbox/status.txt temp/.
    mv /home/pi/farmosv2-script/cvtgate/gate/conf/cpmng.conf temp/.
}

function makeMenu(){
    if [ -s temp/menu_pc.json ] ; then
        cat << "EOF" > "temp/menu_pc.json"
["data/reference","data/correlation","data/statistics","research","device","setting/process","setting/farm","control$
EOF
        cat << "EOF" > "temp/menu_m.json"
["device/state","setting/place","setting/farm"]
EOF
    fi
}


fjboxname=$(cat /etc/hostname)
echo -e $fjboxname

echo -e '\n\n 1-1 file mv temp directory'

if [ ! -d temp ] ; then
    echo -e "aaaaa"
    mkdir temp
    mvDirectory
elif [ -s temp/status.txt ] ; then
    echo -e "bbbbb"
    makeMenu
    echo -e "cccccc"
else
    rmdir temp
    mkdir temp
    mvDirectory
    makeMenu
fi


cd $SHELL_PATH

mv farmosv2-script olddirectory

if [ ! -d farmosv2-script ] ; then
    git clone https://gitlab.com/farmos_public/foodjukebox.git farmosv2-script
else
    echo -e "Fail"
    echo -e "*$fjboxname : Fail (File Exist, Need Check)*" >> log/temp
    exit
fi



echo -e '\n\n 1-2 UI install '
#cd farmosv2-script
#git clone -b ui --single-branch https://gitlab.com/jinong/opensource_ui.git
#mv opensource_ui/ui common_api/api/public
#sudo npm install --prefix ./common_api --unsafe-perm


echo -e '\n\n 1-3 File mv current repository'
cd $SHELL_PATH


rm -rf /home/pi/farmosv2-script/backup
cp -r temp/backup/ /home/pi/farmosv2-script/.

#rm /home/pi/farmosv2-script/common_api/api/public/menu_m.json
#rm /home/pi/farmosv2-script/common_api/api/public/menu_pc.json
#cp -r temp/menu_m.json /home/pi/farmosv2-script/common_api/api/public/menu_m.json
#cp -r temp/menu_pc.json /home/pi/farmosv2-script/common_api/api/public/menu_pc.json

cp -r temp/config.json /home/pi/farmosv2-script/common_api/conf/config.json

cp -r temp/status.txt /home/pi/farmosv2-script/cvtgate/fjbox/status.txt 
cp -r temp/cpmng.conf /home/pi/farmosv2-script/cvtgate/gate/conf/cpmng.conf 

#rm -rf temp

#cp -r /home/pi/foodjukebox/updates/last_update_version.txt /home/pi/farmosv2-script/updates/last_update_version.txt
rm /home/pi/farmosv2-script/scripts/checkautossh.sh
cp -r /home/pi/foodjukebox/scripts/checkautossh.sh /home/pi/farmosv2-script/scripts/checkautossh.sh

echo -e '\n\n 1-3 File cp -r current repository'

(crontab -l 2>/dev/null | sed 's/foodjukebox/farmosv2-script/' ) | crontab -
(crontab -l 2>/dev/null | sed 's/python3 daily_stats.py/python3 daily_stats.py/' ) | crontab -

su - pi -c "
echo -e '*/5 * * * * cd /home/pi/farmosv2-script/scripts; ./checkautossh.sh' | crontab 
"


echo -e '\n\n 1-2 UI install '

sed -i '/복제/d' /home/pi/foodjukebox/updates/log/temp 
sed -i '/Cache/d' /home/pi/foodjukebox/updates/log/temp
sed -i '/debconf/d' /home/pi/foodjukebox/updates/log/temp
sed -i '/dpkg-preconfigure/d' /home/pi/foodjukebox/updates/log/temp


if [ -s /home/pi/foodjukebox/updates/log/temp ];then
    exit
else
    cd farmosv2-script
    git clone -b ui --single-branch https://gitlab.com/jinong/opensource_ui.git
    mv opensource_ui/ui common_api/api/public
    sudo npm install --prefix ./common_api --unsafe-perm

    rm /home/pi/farmosv2-script/common_api/api/public/menu_m.json
    rm /home/pi/farmosv2-script/common_api/api/public/menu_pc.json
    cp -r /home/pi/temp/menu_m.json /home/pi/farmosv2-script/common_api/api/public/menu_m.json
    cp -r /home/pi/temp/menu_pc.json /home/pi/farmosv2-script/common_api/api/public/menu_pc.json
    cat /dev/null > /home/pi/foodjukebox/updates/log/temp
fi

#rm -rf temp

