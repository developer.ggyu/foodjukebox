#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd /home/pi/farmosv2-script/updates
SHELL_PATH=`pwd -P`

sudo service monit stop
sudo /home/pi/farmosv2-script/scripts/fcore stop
sudo /home/pi/farmosv2-script/scripts/cvtgate stop
sudo forever stopall

git pull origin master

function arrangeLog(){
    sed -i '/gitlab.com/d' log/temp
    sed -i '/branch/d' log/temp
    sed -i '/origin/d' log/temp
    sed -i '/복제/d' log/temp
}

function monitStart(){
    sudo service monit start
    sudo monit reload
    sudo /etc/init.d/fui start
}

function initSlack(){
    cat << "EOF" > "pyslack.py"
#! /usr/bin/python
# -*- coding: utf-8 -*-

import requests

def post_message(token, channel, text):
    response = requests.post("https://slack.com/api/chat.postMessage",
        headers={"Authorization": "Bearer "+token},
        data={"channel": channel,"text": text}
    )
    print(response)

EOF
}

function failSlack(){
    echo "r=open('log/$today.log')" >> "pyslack.py"
    cat << "EOF" >> "pyslack.py"
data=r.read()
file.close
post_message('xoxb-1699645670614-2741113048611-IGiBCHLtN2HrRiRn6zXIjSWZ', '#fjbox', data)
EOF
    ret=$(python pyslack.py)

}

function successSlack(){
    echo -e "$updateFileName success"
    sed -i '/xoxb/d' pyslack.py
    echo "post_message('xoxb-1699645670614-2741113048611-IGiBCHLtN2HrRiRn6zXIjSWZ', '#fjbox', '*$fjboxname : $updateFileName success*')" >> "pyslack.py"
    echo $updateFileName > last_update_version.txt
    ret=$(python pyslack.py)
}

if [ -s last_update_version.txt ];then
    lastUpdateVer=`cat last_update_version.txt`
    lastUpdate=${lastUpdateVer:13:6}
else
    lastUpdate="000000"
fi
echo -e $lastUpdate

today=$(date +%y%m%d)
echo -e $today

updateFileNameList=`ls fjbox*.sh`
updateFileNames=$(echo $updateFileNameList | tr " " "\n")

fjboxname=$(cat /etc/hostname)

if [ -s log/$today.log ];then
    rm log/$today.log
fi

touch log/$today.log
chmod 777 log/$today.log

initSlack

for updateFileName in $updateFileNames
do
    updateFile=${updateFileName:13:6}
    if [ ${updateFile} -gt ${lastUpdate} -a ${updateFile} -le ${today} ];then
        echo -e "$updateFileName update"
        bash ./$updateFileName 2>> log/temp
        arrangeLog
        if [ -s log/temp ];then
            sed -i 's/^/>/g' log/temp
            echo "*$fjboxname : $updateFileName fail*" | cat - log/temp >log/$today.log
            sed -i '1,1000d' log/temp
            updateFile="999999"
            failSlack $today
            monitStart
            exit

        else
            successSlack $updateFileName $fjboxname
        fi
    fi
done

monitStart
