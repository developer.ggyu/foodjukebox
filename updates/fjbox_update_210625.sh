#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`


echo -e '\n\n 1. Load Device Information \n'

cd /home/pi/farmosv2-script/cvtgate/fjbox

statustxt=$(awk '/FJDevice Serial/' status.txt)

fjboxserial=${statustxt:18}

if [ ${fjboxserial} -lt 1000 ];then
    connServer=fjboxtest.jinong.co.kr
    temp=$fjboxserial
else
    temp=$(echo $fjboxserial | cut -c 2-)
    serverNum=`expr $temp / 15 + 1`
    if [ ${serverNum} -lt 10 ];then
        connServer=fjbox00$serverNum.jinong.co.kr
    else
        connServer=fjbox0$serverNum.jinong.co.kr
    fi
fi

sshport=`expr $temp + 2000`
uiport=`expr $temp + 8000`
mqttport=`expr $temp + 9000`



echo -e 'SSH Connect Port : ' $sshport, ' UI Connect Port : ' $uiport, ' MQTT Connect Port : '$mqttport
echo -e 'Connect Server :' $connServer ;

echo -e '\n\n 2. Set Up Crontab \n'

su - pi -c "
echo -e '*/5 * * * * cd /home/pi/farmosv2-script/scripts; ./checkautossh.sh' | crontab
"

echo -e '\n\n 3. Set Up Autossh \n'


serial_port=$(echo $sshport | cut -c 3-)

serial_port=$(echo $sshport | cut -c 3-)
if [ $serial_port -le 2000 ]; then
    num1=`expr $serial_port \* 6 + 20000`
    num2=`expr $num1 + 2`
    num3=`expr $num1 + 4`
else
    num1=`expr $serial_port \* 6 + 21000`
    num2=`expr $num1 + 2`
    num3=`expr $num1 + 4`
fi

echo -e $num1 , $num2 , $num3


echo "#!/bin/bash" > "/home/pi/foodjukebox/scripts/checkautossh.sh"
echo "curl -I $connServer:$uiport" >> "/home/pi/foodjukebox/scripts/checkautossh.sh"

cat << "EOF" >> "/home/pi/foodjukebox/scripts/checkautossh.sh"
if [ $? -eq 0 ]
then
    echo "No problems"
else
    echo "Restart autossh"
EOF

echo "    pkill -ef $connServer" >> "/home/pi/foodjukebox/scripts/checkautossh.sh"
echo "    autossh -N -f -p 22222 -M $num1 -R $sshport:localhost:22 fjbox@$connServer" >> "/home/pi/foodjukebox/scripts/checkautossh.sh"
echo "    autossh -N -f -p 22222 -M $num2 -R 0.0.0.0:$uiport:localhost:8081 fjbox@$connServer" >> "/home/pi/foodjukebox/scripts/checkautossh.sh"
echo "    autossh -N -f -p 22222 -M $num3 -R 0.0.0.0:$mqttport:localhost:9001 fjbox@$connServer" >> "/home/pi/foodjukebox/scripts/checkautossh.sh"

cat << "EOF" >> "/home/pi/foodjukebox/scripts/checkautossh.sh"
fi
EOF

