#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#
# data manager for DP data sync and making datasets

import pandas as pd
import csv
import os
import json
import uuid
from datetime import datetime
from lib.dbmng import MySQLManager

class DPMasterManager(MySQLManager):
    def loadfarms(self, opt):
        if "farms" in opt:
            _QUERY = "select farm_id, datainfo, data_node, name from farm where deleted = 0 and farm_id in %s and data_node = %s"
            rows = self._select(_QUERY, [opt["farms"], opt["host"]])
        else:
            _QUERY = "select farm_id, datainfo, data_node, name from farm where deleted = 0 and data_node = %s"
            rows = self._select(_QUERY, [opt["host"]])
        farms = {}

        for row in rows:
            try:
                info = json.loads(row[1])
            except Exception as ex:
                self._logger.info ("Wrong data information : " + str(row) + str(ex))
                continue

            if 'db' in info and 'host' not in info['db']:
                info['db']['host'] = row[2]

            info['farmid'] = row[0]
            info['name'] = row[3]
            info['fields'] = self.loadfields(row[0])
            info['devices'] = self.loaddevices(row[0], info['fields'])
            info['dataids'] = self.loaddataids(row[0])
            # UI에서 설정이 id, pw 로 되어 있는데, 기존 cvtgate와의 호환성을 위해서 정보 생성
            info['db']['user'] = info['db']['id']
            info['db']['password'] = info['db']['pw']

            farms[row[0]] = info
        return farms

    def loadcouples(self, farms):
        _QUERY = "select g.farm_id, c.gateid, c.id, g.hwid from couple c, gate g where c.deleted = 0 and g.deleted = 0 and c.gateid = g.id and g.farm_id in %s"
        if len(farms) == 0:
            return {}, {}

        keys = list(farms.keys())
        rows = self._select(_QUERY, [keys])
        couples = {}
        gates = {}
        for row in rows:
            couples[row[2]] = {"farmid": row[0], "gateid": row[1]}
            gates[row[1]] = {"farmid": row[0], "hwid": row[3]}
        return couples, gates

    def loaddevices(self, farmid, fseasons):
        classstr = {"gateway" : "gw", "node" : "nd", "sensor" : "sen", "actuator" : "act"}

        _QUERY = "select d.id, f.id, d.spec from device_field df, fields f, devices d where f.farm_id = %s and d.farm_id = f.farm_id and df.field_id = f.id and df.farm_id = f.farm_id and d.id = df.device_id and f.deleted = 0 and d.deleted = 0"
        rows = self._select(_QUERY, [farmid])
        devices = {}
        for row in rows:
            try:
                spec = json.loads(row[2])
                devtype = classstr[spec["Class"]] + "/" + spec["Type"]
            except Exception as ex:
                self._logger.info("Fail to load device spec : " + str(row) + str(ex))
                continue

            devices[row[0]] = {"type" : devtype, "fields" : []}
            if row[1] == 0: # 외부라면 모든 작기 데이터에 추가되어야 함
                for fid, info in fseasons.items():
                    devices[row[0]]["fields"].append({'fieldid' : fid, 'postfix': info['postfix']})
            else:
                if row[1] in fseasons:
                    devices[row[0]]["fields"].append({'fieldid' : row[1], 'postfix': fseasons[row[1]]['postfix']})
                else:
                    self._logger.info(str(row[0]) + " devices was installed on farm-field " + str(farmid) + "-" + str(row[1]) + ", but no season.")
        return devices

    def loadfields(self, farmid):
        _QUERY = "select id, name from fields where farm_id = %s and id != 0 and deleted = 0"
        rows = self._select(_QUERY, [farmid])
        fields = {}
        for row in rows:
            fields[row[0]] = self.loadlastseason(farmid, row[0])
            fields[row[0]]["name"] = row[1]
        return fields

    def loadlastseason(self, farmid, fldid):
        _QUERY = "select season_id, postfix, datastatus, start_date from season where deleted = 0 and field_id = %s and farm_id = %s order by season_id desc limit 1"
        rows = self._select(_QUERY, [fldid, farmid])
        if len(rows) == 0:
            return {'seasonid' : 0, 'postfix' : '_' + str(farmid) + '_' + str(fldid), 'datastatus': 0, 'start_date': None}
        else:
            return {'seasonid' : rows[0][0], 'postfix' : rows[0][1], 'datastatus': rows[0][2], 'start_date': rows[0][3]}

    def loaddataids(self, farmid):
        _QUERY = "select id, field_id, name, unit, sigdigit from dataindexes where farm_id = %s and deleted = 0"
        rows = self._select(_QUERY, [farmid])
        dataids = {}
        for row in rows:
            dataids[row[0]] = row
        return dataids

    def loadongoingseasons(self, farms):
        # 진행중인 작기
        _QUERY = "select season_id, farm_id, field_id, start_date, end_date, datastatus, postfix from season where deleted = 0 and end_date is null and farm_id in %s"
        return self._select(_QUERY, [farms])

    def loadfinishedseasons(self, farms):
        # 종료되었으나 데이터셋이 만들어지지 않은 작기
        _QUERY = "select season_id, farm_id, field_id, start_date, end_date, datastatus, postfix, name from season where deleted = 0 and end_date is not null and datastatus = 0 and farm_id in %s"
        return self._select(_QUERY, [farms])

    def updateseasonfordataset(self, seasonid, farmid, fldid):
        _QUERY = "update season set datastatus = 1 where season_id = %s and farm_id = %s and field_id = %s"
        return self._execute(_QUERY, [seasonid, farmid, fldid])

    def writedatasetinfo(self, farmid, datameta, imagemeta):
        uid = uuid.uuid1()
        _QUERY = "insert into data_archive(id, obs_time, deleted, metadata, opend, uuid, farm_id, dataset_type) values ("
        return self._exectue(_QUERY, [])

class DPDataManager(MySQLManager):
    def __init__(self, option, logger):
        super(DPDataManager, self).__init__(option, logger)
        #self._farms = None
        #self._couples = None
        #self._gates = None
        #self._seasons = None

    """
    def setseasons(self, seasons):
        #self._seasons = seasons

    def setfarms(self, farms):
        #self._farms = farms 

    def setcouples(self, couples, gates):
        self._couples = couples
        self._gates = gates 
    """

    def _writedata(self, params, postfix):
        # params = [time, nvalue, dataid, farmid]

        _INSOBS_QUERY = "insert ignore observations" + postfix + "(obs_time, nvalue, data_id, farm_id) values(%s, %s, %s, %s)"
        #print(_INSOBS_QUERY, params)
        self._execute(_INSOBS_QUERY, params)
        return 0

    def datafilesync(self, data, farmid, fldid, season):
        idxes = range(1, len(data["header"]))
        print (idxes, season)
        for row in data["values"]:
            for idx in idxes:
                self._writedata([row[0], row[idx], data["header"][idx], farmid], season["postfix"]) 

    def getdata(self, sdate, postfix):
        _QUERY = "select data_id, DATE_FORMAT(obs_time, '%%Y-%%m-%%d %%H:%%i'), nvalue from observations" + postfix + " where DATE(obs_time) = %s order by data_id, obs_time" 
        print (_QUERY,sdate)
        return self._select(_QUERY, [sdate])

        
class DPManager:
    def __init__(self, optpath, logger):
        self._option = self.loadjson(optpath)
        self._logger = logger
        self._masterdbm = None
        self._datadbm = None
        self._farms = None

    def connect(self):
        option = {"db": self._option["masternode"]}

        self._masterdbm = DPMasterManager(option, self._logger)
        self._masterdbm.connect()

        farms = self._masterdbm.loadfarms(self._option['datanode'])
        self._logger.info(str(len(farms)) + " farms are loaded.")
        #couples, gates = self._masterdbm.loadcouples(farms)
        #self._logger.info(str(len(couples)) + " couples are loaded.")
        #self._logger.info(str(len(gates)) + " gates are loaded.")
        #seasons = self._masterdbm.loadseasonfordataset()
        #self._logger.info(str(len(seasons)) + " seasons are loaded.")

        #print (seasons)

        self._datadbm = DPDataManager(farms[next(iter(farms))], self._logger)
        #self._datadbm.setcouples(couples, gates)
        #self._datadbm.setfarms(farms)
        #self._datadbm.setseasons(seasons)
        #self._seasons = seasons
        self._datadbm.connect()
        self._farms = farms

    def loaddata(self, path):
        header = None
        rows = []
        with open(path, "r") as file:
            csvreader = csv.reader(file)
            header = next(csvreader)
            for row in csvreader:
                rows.append(row)
        return {"header" : header, "values" : rows}

    def backupfilesync(self, root, fname):
        print (root, fname)
        try:
            farmid = int(root.split("/")[1])
            fldid = int(fname.split(".")[0].split("-")[3])
            date = fname[:10]
            season = self._farms[farmid]["fields"][fldid]
            path = root + "/" + fname
        except Exception as ex:
            self._logger.warn("Fail to data sync : " + fname)
            return None

        if season["start_date"] is None or season["start_date"].strftime("%Y-%m-%d") >= date:
            self._logger.warn("Data file is not for a proper season : " + fname + " " + str(season))
        else:
            data = self.loaddata(path)
            print ("start sync", path, datetime.now())
            self._datadbm.datafilesync(data, farmid, fldid, season)
            print ("finish sync", path, datetime.now())

        os.rename(path, self._option["path"]["trash"] + "/" + str(farmid) + "-" + fname)

    def makenewbackup(self, farmid, fldid, date):
        season = self._farms[farmid]["fields"][fldid]
        dirpath = self._option["path"]["backup"] + "/" + str(farmid)
        if os.path.exists(dirpath) is False:
            os.mkdir(dirpath)

        path = dirpath + "/" + str(fldid) + "-" + date.strftime("%Y-%m-%d")
        data = self._datadbm.getdata(date, season["postfix"])
        if len(data) > 0:
            print ("load new data", path, datetime.now())
            self.makenewbackup(path, data)
            self.datasetmerge(path, farmid, fldid)
            print ("merge dataset", path, datetime.now())

    def makenewbackup(self, path, data):
        tmp = {}
        for datum in data:
            k = datum[0] // 10000000
            if (k != 3 and k != 1) or (k == 1 and 20 < datum[0] % 100 < 30):
                continue

            if datum[1] not in tmp:
                tmp[datum[1]] = {"time": datum[1]}
            tmp[datum[1]][str(datum[0])] = datum[2]

        df = pd.DataFrame(tmp.values())
        df.set_index('time')
        df.sort_index(axis=1, inplace=True)
        col = df.pop('time')
        df.insert(0, 'time', col)
        df.to_csv(path, encoding='utf-8', index=False)
        
    def _comparedataset(self, path, dspath):
        fp = open(path, "r")
        dsfp = open(dspath, "r")
        line = fp.readline().rstrip()
        dsline = dsfp.readline().rstrip()
        fp.close()
        dsfp.close()
        if line == dsline:
            return True
        else:
            self._logger.warn("Two files' headers are not matched. " + path + " " + dspath)
            self._logger.warn(line)
            self._logger.warn(dsline)
            return False

    def _concatdataset(self, path, dspath):
        with open(dspath, "a") as fp:
            with open(path, "r") as infp:
                line = infp.readline()
                while True:
                    line = infp.readline()
                    if line:
                        fp.write(line)
                    else:
                        break

    def gettmpdatasetpath(self, farmid, fldid):
        return self._option["path"]["backup"] + "/dataset-" + str(farmid) + "-" + str(fldid) + ".csv"

    def datasetmerge(self, path, farmid, fldid):
        dspath = self.gettmpdatasetpath(farmid, fldid)
        if os.path.exists(dspath):
            if self._comparedataset(path, dspath):
                self._concatdataset(path, dspath)
        else:
            os.system("cp " + path + " " + dspath)

    def loadjson(self, path):
        fp = open(path, "r")
        _json = json.loads(fp.read())
        fp.close()
        return _json

    def getimagemetafilename(self, path, season):
        # season_id, farm_id, field_id, start_date, end_date, datastatus, postfix
        fname = path + "/" + str(season[2]) + ".imgmeta"
        return fname

    def loadimagemeta(self, path, season):
        fname = self.getimagemetafilename(path, season)
        if os.path.exists(fname):
            meta = self.loadjson(fname)
        else:
            if os.path.exists(path) is False:
                os.mkdir(path)
            meta = {"images": {"list" : [], "img_count" : 0}, "search_condition": {"season_id" : season[0], "farm_id" : season[1], "field_id" : season[2], "datatype" : "image"}}
        return meta

    def saveimagemeta(self, path, season, meta):
        fname = self.getimagemetafilename(path, season)
        fp = open(fname, "w")
        json.dump(meta, fp)
        fp.close()

    def makedatameta(self, season, path):
        meta = {"search_condition": {
            "season_id" : season[0], 
            "farm_id" : season[1], 
            "field_id" : season[2], 
            "datatype" : "numeric", 
            "farm_name" : self._farms[season[1]]["name"], 
            "field_name" : self._farms[season[1]]["fields"][season[2]]["name"], 
            "season_name": season[7], 
            "start_date" : season[3], 
            "end_date" : season[4]
            }}

        data = pd.read_csv(path)
        stat = os.stat(path)

        nmeta = {"info" : {"inode" : stat.st_ino, "file_size" : stat.st_size, "unit" : "byte", "encoding": "utf8"}, "format":"csv", "filename" : path.split("/")[-1]}
        nmeta["column"] = []
        nmeta["dataindexes"] = []
        nmeta["unit"] = []
        nmeta["significant_digit"] = []

        dmeta = {}
        for dataid in data.columns[1:]:
            index = self._farms[season[1]]["dataids"][int(dataid)]
            nmeta["dataindexes"].append(index[0])
            nmeta["column"].append(index[2])
            nmeta["unit"].append(index[3])
            nmeta["significant_digit"].append(index[4])

            dmeta[index[2] + "평균"] = df[dataid].mean()
            dmeta[index[2] + "최저"] = df[dataid].min()
            dmeta[index[2] + "최대"] = df[dataid].max()
            dmeta[index[2] + "표준편차"] = df[dataid].stddev()

        meta["numeric"] = nmeta
        meta["data_summary"] = dmeta

        fname = self._option["path"]["backup"] + "/metadata.json"
        fp = open(fname , "w")
        json.dump(meta, fp)
        fp.close()

        return fname

    def addimagetoset(self, fname, path, meta, season):
        startdate = season[3].strftime("%Y%m%d")

        if fname.endswith(".jpg") and startdate < fname:
            for item in meta["images"]["list"]:
                if item["filename"] == fname:
                    return None
            obstime = fname[:4] + "-" + fname[4:6] + "-" + fname[6:8] + " " + fname[9:11] + ":" + fname[11:13] + ":00"
            meta["images"]["list"].append({"filename" : fname, "obs_time" : obstime})
            #meta["images"]["img_count"] = len(meta["images"]["list"])
            #add images the zip file 
            zippath = self._option["path"]["backup"] + "/" + str(season[1]) + "/" + str(season[2]) + ".zip"
            os.system("zip -rv " + zippath + " " + path + "/" + fname)
            #print("zip -rv " + zippath + " " + path + "/" + fname)
            return meta
        else:
            return None

    def synchronize(self):
        # data files
        for root, dirs, files in os.walk(self._option["path"]["upload"]):
            for fname in sorted(files):
                if fname.endswith(".csv") and int(root.split("/")[1]) in self._farms.keys():
                    self.backupfilesync(root, fname)

    def makeimageset(self):
        # image files
        for season in self._masterdbm.loadongoingseasons(list(self._farms.keys())):
            # season_id, farm_id, field_id, start_date, end_date, datastatus, postfix
            path = self._option["path"]["image"] + "/" + str(season[1])
            meta = self.loadimagemeta(path, season)
            for root, dirs, files in os.walk(path):
                for fname in files:
                    self.addimagetoset(fname, root, meta, season)
            self.saveimagemeta(path, season, meta)

    def makedataset(self):
        # season finised, make dataset
        for season in self._masterdbm.loadfinishedseasons(list(self._farms.keys())):
            # season_id, farm_id, field_id, start_date, end_date, datastatus, postfix, name
            path = self.gettmpdatasetpath(season[1], season[2])
            metapath = self.makedatameta(season, path)
            zippath = self._option["path"]["dataset"] + "/dataset.zip"

            os.system("zip " + zippath + " " + path + " " + metapath)

            inode = os.stat(zippath).st_ino
            os.rename(zippath, self._option["path"]["dataset"] + "/dataset_" + str(inode) + ".zip")

            self._masterdbm.writedatasetinfo(season, meta, info)

    def close(self):
        self._masterdbm.close()
        self._datadbm.close()

if __name__ == "__main__":
    import time
    import logging
    import sys

    if len(sys.argv) != 2 or sys.argv[1] not in ("dataset", "sync", "both"):
        print("usage: python3 dpmng.py [dataset|sync|both]")
        print("    dataset : It makes a data set for a finished season.")
        print("    sync : It synchronizes with Database and backup files.")
        print("    both : It does both things.")
        sys.exit(2)

    dpm = DPManager("conf/dp.conf", logging.getLogger('nidana'))
    dpm.connect()
    if sys.argv[1] == "both":
        dpm.synchronize()
        dpm.makeimageset()
        dpm.makedataset()
    elif sys.argv[1] == "sync":
        dpm.synchronize()
    elif sys.argv[1] == "dataset":
        dpm.makeimageset()
        dpm.makedataset()

    dpm.close()

