#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#
# SHVT250TH driver for KIST
#

import time
from mate import Mate
from mate_fixedlen import FixedLengthSerialMate

'''
option : {
    "conn" : {
        "port" : "/dev/ttyUSB0",
        "baudrate": 9600,
    },
    "msgfmt" : {
        "STX" : [0x02],
        "ETX" : None,
        "size" : 18,
        "ididx" : 3,
        "info" : [
            {"len" : 5, "scale" : 1},
            {"len" : 5, "scale" : 1}
        ]
    }
}

devinfo : [
    {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "sen"}
    ]}
]
'''
class SHVT250THMate(FixedLengthSerialMate):
    def __init__(self, option, devinfo, coupleid, logger):
        option["msgfmt"]["STX"][0] = chr(option["msgfmt"]["STX"][0])
        super(SHVT250THMate, self).__init__(option, devinfo, coupleid, logger)


if __name__ == "__main__":
    option = {
        "conn" : {
            "port" : "/dev/ttyUSB0",
            "baudrate": 9600,
        },
        "msgfmt" : {
            "STX" : [2],
            "ETX" : None,
            "size" : 24,
            "ididx" : 3,
            "info" : [
                {"len" : 5, "scale" : 1},
                {"len" : 6, "scale" : 1},
                {"len" : 5, "scale" : 1}
            ]
        }
    }

    devinfo = [
        {"id" : "1", "dk" : "1", "dt": "gw", "children" : [
            {"id" : "2", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "3", "dk" : "0", "dt": "sen"},
                {"id" : "4", "dk" : "1", "dt": "sen"},
                {"id" : "5", "dk" : "2", "dt": "sen"}
            ]}
        ]}
    ]

    mate = SHVT250THMate(option, devinfo, "1", None)
    mate2 = Mate({}, [], "1", None)
    mate.start(mate2.writeblk)
    print("mate started")
    time.sleep(30)
    #mate.readmsg()
    #mate.sendobs()
    mate.stop()
    print("mate stopped")

