#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
import json
from enum import IntEnum
from threading import Thread, Lock

from imports import *
from mate import Mate, ThreadMate, DevType

class KISTFoodJukeBoxActuatorMate(ThreadMate):
    _INDIR = "/home/pi/Heitu/input/"
    _OUTDIR = "/home/pi/Heitu/output/"
    _FILES = [("PWMcmd1", "PWMstat1"), ("PWMcmd2", "PWMstat2"), ("PWMcmd3", "PWMstat3")]

    def __init__(self, option, devinfo, coupleid, logger):
        super(KISTFoodJukeBoxActuatorMate, self).__init__(option, devinfo, coupleid, logger)

    def readvaluefromfile(self, fname):
        try:
            with open(fname) as f:
                return [int(val) for val in f.readline().split(',')]
        except Exception as ex:
            self._logger.warn("exception : " + str(ex))
            return None

    def readmsg(self):
        pass

    def processrequest(self, dev, req, nd):
        operation = req.getcommand()
        opid = req.getopid()
        param = req.getparams()
        if "ratio" in param:
            ratio = param["ratio"]
        else:
            ratio = 0
        idx = int(dev["dk"])
        fname = KISTFoodJukeBoxActuatorMate._INDIR + KISTFoodJukeBoxActuatorMate._FILES[idx][0]
        try:
            with open(fname, "w") as f:
                print((",".join([str(opid), str(operation), str(ratio)])))
                f.write(",".join([str(opid), str(operation), str(ratio)]))
                return ResCode.OK
        except Exception as ex:
            self._logger.warn("fail to process request exception : " + str(ex))
            return ResCode.FAIL

    def writeblk(self, blk):
        print("received message", blk.getdevid(), self._coupleid)
        if BlkType.isrequest(blk.gettype()) is False:
            self._logger.warn("The message is not request. " + str(blk.gettype()))
            return False

        response = Response(blk)
        cmd = blk.getcommand()
        nd = self._devinfo.finddevbyid(blk.getnodeid())
        dev = self._devinfo.finddevbyid(blk.getdevid())

        if dev is None:
            self._logger.warn("There is no device. " + str(blk.getdevid()))
            code = ResCode.FAIL_NO_DEVICE

        elif DevType.ispropercommand(dev['dt'], cmd) is False:
            self._logger.warn("The request is not proper. " + str(cmd) + " " + str(dev['dt']))
            code = ResCode.FAIL_NOT_PROPER_COMMAND

        elif DevType.isactuator(dev['dt']) or DevType.isnode(dev['dt']):
            # modbus
            code = self.processrequest(dev, blk, nd)
            self._logger.info("Actuator processed : " + str(code))

        elif DevType.isgateway(dev['dt']):
            self._logger.info("Gateway does not receive a request")
            code = ResCode.FAIL

        else:
            self._logger.warn("Unknown Error. " + str(blk) + ", " + str(dev))
            code = ResCode.FAIL

        response.setresult(code)
        self._logger.info("write response: " + str(response))
        self.writecb(response)
        return True #if code == ResCode.OK else False

    def sendobs(self):
        pass

    def sendnoti(self):
        for gw in self._devinfo:
            for nd in gw["children"]:
                notiblk = Notice(nd["id"], NotiCode.ACTUATOR_STATUS,  nd["id"], {"status" : StatCode.READY.value})
                for dev in nd["children"]:
                    if DevType.isactuator(dev["dt"]):
                        idx = int(dev["dk"])
                        fname = KISTFoodJukeBoxActuatorMate._OUTDIR + KISTFoodJukeBoxActuatorMate._FILES[idx][1]
                        value = self.readvaluefromfile(fname)
                        if value is not None:
                            notiblk.setcontent(dev["id"], {"status" : value[1], "opid" : value[0], "ratio" : value[2]})
                        else:
                            notiblk.setcontent(dev["id"], {"status" : StatCode.ERROR.value})
                self.writecb(notiblk)

    def start(self, writecb):
        super(KISTFoodJukeBoxActuatorMate, self).start(writecb)
        return True

    def stop(self):
        super(KISTFoodJukeBoxActuatorMate, self).stop()
        return True

if __name__ == "__main__":
    opt = {
    }
    
    devinfo = [{
        "id" : "1", "dk" : "JND2", "dt": "gw", "children" : [{
            "id" : "101", "dk" : '', "dt": "nd", "children" : [
                {"id" : "102", "dk" : '0', "dt": "act/switch/level2"},
                {"id" : "103", "dk" : '1', "dt": "act/switch/level2"},
                {"id" : "104", "dk" : '2', "dt": "act/switch/level2"}
             ]
        }]
    }]

    kmate = KISTFoodJukeBoxActuatorMate(opt, devinfo, "1", None)

    mate = Mate ({}, [], "1", None)
    kmate.start (mate.writeblk)
    print("mate started")

    time.sleep(10)
    req = Request(101)
    req.setcommand(102, CmdCode.DIRECTIONAL_ON, {'ratio':10})
    kmate.writeblk(req)

    time.sleep(30)
    kmate.stop()
    print("mate stopped") 
