#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#
# A driver for Fixed length mblock
#

import time
import traceback

from imports import *

from mate import Mate, ThreadMate, DevType
from connection import SerialConnection

'''
option : {
    "conn" : {
        "port" : "/dev/ttyUSB0",
        "baudrate": 9600,
    },
    "msgfmt" : {
        "STX" : "str",
        "ETX" : None,
        "size" : 24,
        "ididx" : 1,
        "info" : [
            { "len" : 6, "scale" : None},
            { "len" : 3, "scale" : 1},
            { "len" : 3, "scale" : 0.1},
            { "len" : 3, "scale" : 0.1},
            { "len" : 3, "scale" : 1},
            { "len" : 3, "scale" : 1}
        ]
    }
}

'''

class FixedLengthSerialMate(ThreadMate):
    def __init__(self, option, devinfo, coupleid, logger):
        super(FixedLengthSerialMate, self).__init__(option, devinfo, coupleid, logger)
        self._conn = SerialConnection(option["conn"], logger)
        self._parsed = []
        self._msgfmt = option["msgfmt"]
        try:
            self._nd = devinfo[0]["children"][0]
        except:
            self._nd = None

        if self._msgfmt["STX"] is None:
            self._msgfmt["STXlen"] = 0
        else:
            self._msgfmt["STXlen"] = len(self._msgfmt["STX"])
            if isinstance(self._msgfmt["STX"], list):
                self._msgfmt["STX"] = "".join(self._msgfmt["STX"]) 

        if self._msgfmt["ETX"] is None:
            self._msgfmt["ETXlen"] = 0
        else:
            self._msgfmt["ETXlen"] = len(self._msgfmt["ETX"])
            if isinstance(self._msgfmt["ETX"], list):
                self._msgfmt["ETX"] = "".join(self._msgfmt["ETX"]) 

    def sendobs(self):
        if self._nd is None:
            self._logger.warn("node infomation is not available.")
            return
        if "children" not in self._nd:
            self._logger.warn("device infomation is not available.")
            return

        if self._parsed is not None and len(self._parsed) > 0:
            obsblk = Observation(self._nd["id"])
            for dev in self._nd["children"]:
                idx = int(dev["dk"])
                print("sendobs", dev["id"], idx)
                obsblk.setobservation(dev["id"], self._parsed[idx], StatCode.READY)
            self.writecb(obsblk)

    def parsemsg(self, buf):
        #st = self._msgfmt["STXlen"]
        st = self._msgfmt["ididx"]
        print("parsemsg", st)
        ed = 0
        tm = time.time()
        obs = []
        print("message " + buf[st:])

        for info in self._msgfmt["info"]:
            ed = st + info["len"]
            print(buf[st:ed], info)
            if info["scale"] is None:
                obs.append(float(buf[st:ed]))
            else:
                obs.append(float(buf[st:ed]) * info["scale"])
            st = ed
        print("obs", obs)
        return obs
            

    def readmsg(self):
        try:
            buf = self._conn.read(self._msgfmt["size"])
            idx = buf.index(self._msgfmt["STX"])
            print("buf idx", idx)
            if idx > 0:
                buf = buf[idx:]
                buf = buf + (self._conn.read(idx))
            elif idx == 0:
                pass
            else:
                return None
            self._parsed = self.parsemsg(buf)
        except Exception as ex:
            self._logger.warn ("fail to read msg : " + str(ex))
            self._logger.warn (traceback.format_exc())
            self._parsed = []
            return None

    def writeblk(self, blk):
        pass

    def start(self, writecb):
        self._conn.open()
        return super(FixedLengthSerialMate, self).start(writecb)

    def stop(self):
        super(FixedLengthSerialMate, self).stop()
        self._conn.close()

if __name__ == "__main__":
    #option = {"conn" : {"port" : "/dev/ttyUSB0", "baudrate": 9600, "type":"CTRL"}}
    #devinfo = [{"id" : "1010", "dk" : "-1", "dt": "nd", "chd" : [{"id" : "1001", "dk" : "0", "dt": "sen"}, {"id" : "1002", "dk" : "1", "dt": "sen"}, {"id" : "1004", "dk" : "2", "dt":"sen"}, {"id" : "1551", "dk" : "3", "dt": "sen"},{"id" : "1552", "dk" : "4", "dt": "sen"},{"id" : "1", "dk" : "5", "dt": "sen"},{"id" : "3", "dk" : "6", "dt": "sen"},{"id" : "4", "dk" : "7", "dt": "sen"}, {"id" : "5", "dk" : "8", "dt": "sen"},{"id" : "1632", "dk" : "9", "dt": "sen"},{"id" : "1642", "dk" : "10", "dt": "sen"},{"id" : "1662", "dk" : "11", "dt": "sen"},{"id" : "1672", "dk" : "12", "dt": "sen"},{"id" : "1612", "dk" : "13", "dt": "sen"},{"id" : "1622", "dk" : "14", "dt": "sen"},{"id" : "1651", "dk" : "15", "dt": "sen"}]}]

    option = {
        "conn" : {"port" : "/dev/ttyUSB1", "baudrate": 9600}, 
        "msgfmt" : {
            "STX" : "str",
            "ETX" : None,
            "size" : 15,
            "info" : [
                {"len" : 3, "scale" : 0.1},
                {"len" : 3, "scale" : 0.1},
                {"len" : 3, "scale" : 1},
                {"len" : 3, "scale" : 1}
            ]
        }
    }
    devinfo = [{"id" : "3", "dk" : "1", "dt": "nd", "children" : [{"id" : "4", "dk" : "0", "dt": "sen"}, {"id" : "5", "dk" : "1", "dt": "sen"}, {"id" : "6", "dk" : "2", "dt": "sen"}, {"id" : "7", "dk" : "3", "dt": "sen"}]}]

    mate = FixedLengthSerialMate(option, devinfo, "1", None)
    mate2 = Mate({}, [], "1", None)
    mate.start(mate2.writeblk)
    print("mate started")
    time.sleep(10)
    mate.stop()
    print("mate stopped")

