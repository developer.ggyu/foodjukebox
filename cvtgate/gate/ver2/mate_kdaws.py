#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
from threading import Thread

from imports import *
from mate import Mate, ThreadMate, DevType

class KDAWSMate(ThreadMate):
    _STX = [0xA5, 0xF1, 0xAA, 0x55]
    _ETX = [0x55, 0xF0, 0x55, 0xAA]

    _VERSION = "KDAWS_0.1"


    def __init__(self, option, devinfo, coupleid, logger):
        super(KDAWSMate, self).__init__(option, devinfo, coupleid, logger)
        self._session = 0
        self._conn = None
        self._buf = None
        self._relay = {}

        # 농장 여러개를 한꺼번에 다루지 않는다.
        # 그래서 GW를 하나만 사용한다.
        self._gw = self._devinfo.getgw()
        for nd in self._gw["children"]:
            if "children" in nd:
                for dev in nd["children"]:
                    if DevType.isactuator(dev["dt"]):
                        self._relay[nd["id"]] = {}
        print("relay", self._relay)

    def getactdevkey(self, devid): 
        #for gw in self._devinfo:
        #    if "children" in gw:
        #        for nd in gw["children"]:
        for nd in self._gw["children"]:
            if "children" in nd:
                for dev in nd["children"]:
                    if DevType.isactuator(dev["dt"]) and str(dev["id"]) == str(devid):
                        return (nd["id"], int(nd["dk"]), int(dev["dk"]))

        self._logger.warn ("Fail to find device key of device : " + str(devid))
        return (None, None, None)

    def find(self, haystack, needle):
        h = len(haystack)
        n = len(needle)
        skip = {needle[i]: n - i - 1 for i in range(n - 1)}
        i = n - 1
        while i < h:
            for j in range(n):
                if haystack[i - j] != needle[-j - 1]:
                    i += skip.get(haystack[i], n)
                    break
            else:
                return i - n + 1
        return -1
    
    def getpacket(self, cmd):
        for i in range(5):
            try:
                tmp = self._conn.recv(1024)
            except Exception as ex:
                self.close()
                self._logger.warn("exception : " + str(ex))
                print(traceback.format_exc())
                return -1

            self._buf = self._buf + tmp
            idx = self.find(self._buf, KDAWSMate._ETX)
            if idx > 0:
                msg = self._buf[:idx+4]
                self._buf = self._buf[idx+4:]
                print('recv packet', len(msg), '[{}]'.format(', '.join(hex(x) for x in msg)))
                if [msg[4], msg[5]] == cmd:
                    return msg
                elif [msg[4], msg[5]] == [0x01, 0x00]:
                    return msg[6] + msg[7] * 256
            time.sleep(1)
        self._logger.warn("tried to recv 5 times. But no message get.")
        self.close()
        return -2

    def getpayload(self, msg):
        return msg[10:-8]

    def calculatechecksum(self, msg):
        #print "checksum", sum(msg)
        return sum(msg)

    def sendpacket(self, cmd, length, payload):
        msg = bytearray(KDAWSMate._STX)
        msg.extend(cmd)
        msg.extend(struct.pack("<I", length))
        msg.extend(payload)
        msg.extend(struct.pack("<I", self.calculatechecksum(msg)))
        msg.extend(KDAWSMate._ETX)
        print('send packet', len(msg), '[{}]'.format(', '.join(hex(x) for x in msg)))
        try:
            self._conn.send(msg)
        except Exception as ex:
            self.close()
            self._logger.warn("exception : " + str(ex))
            return False
        return True

    def sendmessage(self, cmd, length, payload):
        while True:
            if self.sendpacket(cmd[:2], length, payload) == False:
                return None
            ret = self.getpacket(cmd[2:4])
            if ret == -1:
                return None
            elif ret == -2: 
                continue
            elif isinstance(ret, int):
                self._logger.warn("fail to send packet. " + str(cmd) + " retcode: " + str(ret))
                return None
            return ret

    def close(self):
        super(KDAWSMate, self).close()
        self._conn.close()

    def connect(self):
        try:
            self._buf = bytearray([])
            soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            soc.connect((self._option["login"]["host"], self._option["login"]["port"]))
            self._logger.info("connected to login: " + str(self._option['login']))
            self._conn = soc
            self._session = self.loginpacket()
            soc.close()
            if self._session == False:
                msg = "fail to login."
                self._logger.warn(msg)
                noti = Notice(None, NotiCode.TCP_FAILED_CONNECTION, content={"msg":msg}) # fail to connect
                noti.setextra("gwid", self._gw["id"])
                self.writecb(noti)
                return False

            self._logger.info("login: " + str(struct.unpack("i", self._session)[0]))
            time.sleep(3)

            soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            if "timeout" in self._option["conn"]:
                soc.settimeout(self._option["conn"]["timeout"])
            else:
                soc.settimeout(3)
            soc.connect((self._option["conn"]["host"], self._option["conn"]["port"]))
            self._conn = soc
            if self.connecttofarm(self._gw) == False:
                msg = "fail to connect to farm : " + str(self._gw["dk"])
                self._logger.warn(msg)
                noti = Notice(None, NotiCode.TCP_FAILED_CONNECTION, content={"msg":msg}) # fail to connect
                noti.setextra("gwid", self._gw["id"])
                self.writecb(noti)
                soc.close()
                return False
            self._logger.info("connected to farm")

        except socket.error as exc:
            self._logger.warn("exception : " + str(exc))
            soc.close()
            return False
        except Exception as ex:
            soc.close()
            self._logger.warn("exception : " + str(ex))
            print(traceback.format_exc())
            return False

        super(KDAWSMate, self).connect()
        return True

    def loginpacket(self):
        cmd = [0x70, 0x00, 0x71, 0x00]
        m = hashlib.md5()
        m.update(self._option["login"]["password"])
        payload = struct.pack("<H", len(self._option["login"]["account"]))
        payload = payload + struct.pack("<H", len(self._option["login"]["devname"]))
        payload = payload + m.digest()
        payload = payload + chr(0)
        payload = payload + self._option["login"]["account"].encode('utf-8')
        payload = payload + self._option["login"]["devname"].encode('utf-8')
        msg = self.sendmessage(cmd, len(payload), payload)
        if msg is None:
            return False
        payload = self.getpayload(msg)
        unpacked = [payload[0], struct.unpack("i", payload[1:5])[0], payload[5:7]]
        unpacked.append(payload[5:7]) 
        unpacked.append(payload[7:]) 
        print("login", unpacked)
        return payload[1:5]

    def connecttofarm(self, gw):
        cmd = [0x78, 0x00, 0x79, 0x00]
        payload = self._session + gw["dk"].encode('utf-8')
        msg = self.sendmessage(cmd, len(payload), payload)
        if msg is None:
            return False
        return True

    def getsensornodelist(self):
        cmd = [0x10, 0x00, 0x11, 0x00]
        msg = self.sendmessage(cmd, 2, [1, 0])
        if msg is None:
            return None
        payload = self.getpayload(msg)
        nodes = {} 
        for i in range(payload[0]):
            idx = i * 41
            devnode = self._devinfo.finddevbydevkey(payload[idx + 1], DevType.NODE)
            if devnode is None:
                print('no nid', payload[idx + 1])
                continue

            if devnode['id'] in nodes:
                node = nodes[devnode['id']]
            else:
                node = {"nid" : devnode['id'], "devices":[]}

            for j in range(8):
                dev = self._devinfo.finddevbydevkey(payload[idx + 2 + j * 5], DevType.SENSOR, devnode["children"])
                if dev is None:
                    print('no dev', devnode['id'], payload[idx + 2 + j * 5])
                    continue
                val = struct.unpack("f", payload[idx + 3 + j * 5: idx + j * 5 + 7])[0]
                node["devices"].append ({"id": dev["id"], "stat": StatCode.READY, "obs":val, "dt": dev["dt"]})
            nodes[devnode['id']] = node
        print("getsensornodelist", nodes)
        return nodes

    def getactuatorstat(self, dev, val):
        dt = dev['dt'].split("/")
        assert dt[0] == DevType.ACTUATOR
        if len(dt) == 1:
            dt = DevType.DEFAULT_ACTUATOR #["act", "switch", "level0"]
        if dt[1] == "switch":
            if val == 0:
                return StatCode.READY.value
            elif val == 1:
                return StatCode.WORKING.value
            self._logger.warn("unknown status value for switch. " + str(val))
        elif dt[1] == "retractable":
            if val == 0:
                return StatCode.READY.value
            elif val == 2:
                return StatCode.OPENING.value
            elif val == 3:
                return StatCode.CLOSING.value
            self._logger.warn("unknown status value for retractable. " + str(val))
        elif dt[1] == "nutrient supplier":
            self._logger.warn("nutrient supplier is not supported.")
            pass
        else:
            self._logger.warn("unknown device type. " + str(dt))
        return StatCode.ERROR.value

    def getoutput(self):
        cmd = [0x44, 0x01, 0x45, 0x01]
        msg = self.sendmessage(cmd, 0, [])
        if msg is None:
            return None
        payload = self.getpayload(msg)
        nodes = {}
        for i in range(128):
            idx = i * 3

            if payload[idx] == 0 or payload[idx + 1] == 0:
                #print 'unknown id ', payload
                continue

            ver = 1 if payload[idx] >= 128 else 0
            pos = payload[idx] & 127
            hid = (payload[idx + 1] >> 5) + 1
            did = payload[idx + 1] & 0x1F

            #nid = self._devinfo.findid(hid, DevType.NODE)
            nid = self._devinfo.findid(0, DevType.NODE)
            if nid is None:
                self._logger.warn("There is no node id for " + str(hid))
                continue

            if nid in nodes:
                node = nodes[nid]
            else:
                node = {"nid" : nid, "devices":[]}

            dev = self._devinfo.finddevbydevkey(did, DevType.ACTUATOR)
            if dev is None:
                self._logger.warn("There is no device id for " + str(did))
                continue
            val = self.getactuatorstat(dev, payload[idx+2] & 0x0F)
            print("getoutput payload", ver, pos, hid, did, val)

            if DevType.getretractablelevel(dev["dt"]) == 2:
                node["devices"].append ({"id": dev["id"], "status":val, "dt": dev["dt"], "pos": pos})
            else:
                node["devices"].append ({"id": dev["id"], "status":val, "dt": dev["dt"]})
            nodes[nid] = node
        print("getoutput", nodes)
        return nodes

    def setoutput(self, houseid, groupid, state):
        cmd = [0x30, 0x01, 0x31, 0x01]
        #payload = [houseid, groupid, 0, state]
        payload = [1, groupid, 0, state]
        print("setoutput", payload)
        msg = self.sendmessage(cmd, len(payload), payload)
        if msg is None:
            return False
        return True

    def setreq(self, request):
        (ndid, nddk, devdk) = self.getactdevkey(request.getdevid())
        response = Response(request)
        if ndid is None:
            self._logger.warn("fail to get actuator information.")
            self.writecb(response)
            return False

        print("setreq", ndid, nddk, devdk)
        if request.getcommand() == CmdCode.OFF:
            self._relay[ndid][devdk] = 0
        elif request.getcommand() == CmdCode.ON:
            self._relay[ndid][devdk] = 1
        elif request.getcommand() == CmdCode.OPEN:
            self._relay[ndid][devdk] = 2
        elif request.getcommand() == CmdCode.CLOSE:
            self._relay[ndid][devdk] = 3
        else:
            self._logger.warn("Wrong command : " + str(request.getcommand()))
            self.writecb(response)
            return False

        print("set request : ", self._relay[ndid])

        if self.setoutput(int(nddk), int(devdk), self._relay[ndid][devdk]):
            response.setresult(ResCode.OK)
        else:
            self._relay[ndid][devdk] = 0
            response.setresult(ResCode.FAIL)
        self.writecb(response)
        return True

    def readmsg(self):
        pass

    def writeblk(self, blk):
        if self.isconnected() is False:
            self._logger.warn("It's not logged in.")
            return False

        if BlkType.isrequest(blk.gettype()):
            return self.setreq(blk)

        self._logger.warn("The message is not request. " + str(blk.gettype()))
        return False

    def makeobservation(self, node):
        print("makeobservation", node)
        obsblk = Observation(node['nid'])
#statblk = Status(node['nid'], StatCode.READY)
        msgs = [obsblk]
        for info in node['devices']:
            if "obs" in info:
                obsblk.setobservation(info["id"], info["obs"], info["stat"])
            else:
                obsblk.setobservation(info["id"], 0, info["stat"])

        return msgs

    def start(self, writecb):
        super(KDAWSMate, self).start(writecb)
        return True

    def stop(self):
        super(KDAWSMate, self).stop()
        return True

    def sendmsgs(self, msgs):
        for m in msgs:
            self.writecb(m)

    def sendobs(self):
        nodeinfo = self.getsensornodelist()
        if nodeinfo is None:
            # sensor node status
            nodeid = self._gw["children"][0]["id"]
            obsblk = Observation(nodeid, StatCode.ERROR)
            msgs = [obsblk]
            self._logger.warn("nodeid (" + str(nodeid) + ") is not existed")
        else:
            self._logger.info("nodeinfo " + str(nodeinfo))
            msgs = []
            for nid, node in nodeinfo.items():
                msgs.extend(self.makeobservation(node))
        self.sendmsgs(msgs)

    def sendnoti(self):
        nodeinfo = self.getoutput()
        msgs = []
        if nodeinfo is None:
            return None

        for nid, node in nodeinfo.items():
            noti = Notice(nid, NotiCode.ACTUATOR_STATUS)
            for info in node['devices']:
                if "pos" in info:
                    noti.setcontent(info['id'], {"status": info["status"], "pos": info['pos']})
                else:
                    noti.setcontent(info['id'], {"status": info["status"]})
            msgs.append(noti)
        self.sendmsgs(msgs)

if __name__ == "__main__":
    opt = {'conn' : {
        'host' : '52.78.109.164', 'port' : 8883, 'timeout' : 3
    }, 'login' : {
        'host' : '52.78.109.164', 'port' : 8882,
        'account' : 'jngrapeb', 'password' : '1234', 'devname' : 'cvtgate'
    }}
    
    devinfo = [{"id" : "1", "dk" : "BC0042", "dt": "gw", "children" :[
                 {"id" : "2", "dk" : 1, "dt": "nd", "children" : [ ]},
                 {"id" : "3", "dk" : 17, "dt": "nd", "children" : [ ]},
                 {"id" : "4", "dk" : 18, "dt": "nd", "children" : [ ]},
                 {"id" : "13", "dk" : 2, "dt": "nd", "children" : [
# actuator dk == output relay group
                   {"id" : "14", "dk" : 1, "dt": "act/switch/level0"},
                   {"id" : "15", "dk" : 2, "dt": "act/switch/level0"},
                   {"id" : "16", "dk" : 3, "dt": "act/retractable/level0"}
                 ]}
              ]}]

    kdmate = KDAWSMate(opt, devinfo, None)
    mate = Mate ({}, [], None)
    kdmate.start (mate.writeblk)
    print("mate started")

    """
    time.sleep(10)
    req = Request(13)
    req.setcommand(14, CmdCode.ON, {})
    kdmate.writeblk(req)

    time.sleep(5)
    req = Request(13)
    req.setcommand(14, CmdCode.OFF, {})
    kdmate.writeblk(req)
    """

    time.sleep(10)
    kdmate.stop()
    print("mate stopped") 
