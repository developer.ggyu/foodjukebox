#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import sys
import time
import json
import struct
import datetime
import queue
import traceback

from imports import *
from mate import Mate, ThreadMate, DevType
from connection import TCPServerConnection

sys.path.insert(0, '../cdtp/cdtpy')
import cdtp

'''
option : {
    "gatewaykey" : "JS0",
    "conn" : {
        "port" : 10010
    }
}

devinfo : [ ]

'''

class TCPCDTPMate(ThreadMate):
    def __init__(self, option, devinfo, coupleid, logger):
        super(TCPCDTPMate, self).__init__(option, devinfo, coupleid, logger)
        self._cdtp = cdtp.CDTPServer(option['gatewaykey'])
        self._conn = TCPServerConnection(option, logger)
        #self._conndict = {'snid' : 'socket'}
        self._lastlog = {}
        self._idtbl = {}

    def start(self, writecb):
        self._conn.open()
        return super(TCPCDTPMate, self).start(writecb)

    def stop(self):
        super(TCPCDTPMate, self).stop()
        self._logger.info("TCPCDTPMate stopped.")
        self._conn.close()

    def writeblk(self, blk):
        # external callback
        snid = int(blk.getnodeid()[2:])
        if BlkType.isresponse(blk.gettype()):
            #self._cdtp.response()
            #self.send(snid, msg)
            print("response from ssmate")
        elif BlkType.isrequest(blk.gettype()):
            current = datetime.datetime.now()
            body = blk.getcontent()

            try:
                payload = struct.pack(">IHHHHHHHHHHH", 0, 0,
                    current.year, current.month, current.day, current.hour, current.minute,
                    int(body["onoff"]), int(float(body["fnd1"]) * 10 + 1000), int(float(body["fnd2"]) * 10 + 1000),
                    int(float(body["fnd3"]) * 10 + 1000), int(float(body["fnd4"]) * 10 + 1000))
                payload = payload + chr(0) * 18

                data = self._cdtp.gencustomreq(0x64, snid, payload)
                self.send(snid, ''.join(map(chr, data)))
                # finish to send
                self.send(snid, None)
                if snid in self._lastlog:
                    self._logger.info("send fnd value " + str(snid) + " time : " + str(datetime.datetime.now() - self._lastlog[snid]))
            except Exception as ex:
                self._logger.warn ("received content but fail to send : " + str(ex) + " : " + str(body))

    def send(self, snid, msg):
        #print "sent : ", len(msg), " "
        #if msg is not None:
        #    for i in range(len(msg)):
        #        try:
        #            print int(msg[i]),
        #        except:
        #            print ord(msg[i]), 
        #    print ""
        soc = self._idtbl[snid]
        self._conn.write(msg, soc)

    def wrapblk(self, noti):
        try:
            node = self._devinfo.finddevbydevkey(noti["SNID"], DevType.NODE)
            obsblk = Observation(node["id"])
            for k, v in noti["observations"].items():
                sid = self._devinfo.findid(k, DevType.SENSOR, node['children'])
                if sid is None:
                    self._logger.warn("fail to find id. " + str(k))
                    continue
                obsblk.setobservation(sid, self.getvalue(k, v), StatCode.READY)
            return obsblk
        except Exception as ex:
            self._logger.warn("fail to wrap. " + str(ex))
            self._logger.warn(str(traceback.format_exc()))
            return None

    def readmsg(self):
        try:
            bufarr = self._conn.read(cdtp.CDTP_BINMSGLEN)
            for buf in bufarr:
                self._logger.info("received " + ''.join(format(ord(x), '02x') for x in buf['data']))
                parsed = self._cdtp.parse(buf['data'])
                if parsed:
                    self._logger.info("parsed " + str(parsed["type"]) + " " + str(parsed["values"]))
                    if parsed['type'] == 'Notification':
                        if parsed["values"]["SNID"] in self._lastlog:
                            obs = self.wrapblk(parsed["values"])
                            if obs:
                                self._writecb(obs)
                        else:
                            self._logger.info("Message from a not registered device." + str(parsed["values"]))

                    elif parsed['type'] == 'Request':
                        body = parsed["values"]
                        snid = parsed["values"]["SNID"]
                        self._lastlog[snid] = datetime.datetime.now()
                        if body["gatewaykey"] != self._option["gatewaykey"]:
                            self._logger.info("unknown gateway : " + body["gatewaykey"])
                        else:
                            if body["sleeptime"] != self._option["sleeptime"]:
                                req = self._cdtp.reconfigure(body["SNID"], body["gatewaykey"], self._option["gatewaykey"], self._option["sleeptime"])
                                self._idtbl[snid] = buf['soc']
                                self._logger.info("send request to reset sleeptime " + str(snid))
                                self.send(snid, req)
                            else:
                                res = self._cdtp.response(parsed, cdtp.CDTP_OK)
                                self._idtbl[snid] = buf['soc']
                                self._logger.info("send response " + str(snid))
                                res[12] = 0
                                self.send(snid, res)
                                content = {'status': StatCode.READY}
                                nd = self._devinfo.finddevbydevkey(snid, DevType.NODE)
                                #self._writecb(Request(nd["id"]))
                    else:
                        self._logger.warn("weired mblock." + str(parsed))
                else:
                    self._logger.warn("wrong mblock." + str(buf))

        except Exception as ex:
            self._logger.warn("fail to read msg : " + str(ex))
            self._logger.warn(str(traceback.format_exc()))
        return None

if __name__ == "__main__":
    option = {
        "conn" : {
            "port" : 10010
        },
        "gatewaykey" : "JS0",
        "sleeptime" : 53
    }

    devinfo = [
        {"id" : "3", "dk" : "1", "dt" : "gw", "children" : [
            {"id" : "3", "dk" : "1", "dt" : "nd", "children" : [
                {"id" : "4", "dk" : "1", "dt" : "sen"},
                {"id" : "5", "dk" : "2", "dt" : "sen"},
                {"id" : "6", "dk" : "3", "dt" : "sen"}
            ]}
        ]}
    ]

    mate = TCPCDTPMate(option, devinfo, "1", None)
    mate2 = Mate({}, [], "1", None)
    mate.start(mate2.writeblk)
    print("mate started.")
    time.sleep(60)
    mate.stop()
    print("mate stopped.")
