#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
from threading import Thread

from imports import *
from mate import Mate, ThreadMate, DevType

class KDRRWMateWorker(Thread):
    _STX = [0xA5, 0xF1, 0xAA, 0x55]
    _ETX = [0x55, 0xF0, 0x55, 0xAA]

    _VERSION = "IoTRRW_0.1"

    def __init__(self, option, devinfo, logger):
        super(KDRRWMateWorker, self).__init__()
        self._gwid = ""
        self._nodes = []
        self._conn = option['conn']
        self._writecb = option['writecb']
        self._logger = logger
        self._executing = False
        self._gw = None
        self._buf = bytearray([])
        self._relay = {}

    def setgateway(self, gateway):
        self._gw = gateway
        self._relay = {}
        assert "children" in gateway
        for nd in gateway["children"]:
            if "children" in nd:
                for dev in nd["children"]:
                    if DevType.isactuator(dev["dt"]):
                        self._relay[int(nd["id"])] = [0, 0, 0, 0]

    def getactdevkey(self, devid): 
        assert "children" in self._gw
        for nd in self._gw["children"]:
            if "children" in nd:
                for dev in nd["children"]:
                    if DevType.isactuator(dev["dt"]) and str(dev["id"]) == str(devid):
                        return (int(nd["id"]), int(nd["dk"]), int(dev["dk"]))

        self._logger.warn ("Fail to find device key of device : " + str(devid))
        return (None, None, None)

    def find(self, haystack, needle):
        h = len(haystack)
        n = len(needle)
        skip = {needle[i]: n - i - 1 for i in range(n - 1)}
        i = n - 1
        while i < h:
            for j in range(n):
                if haystack[i - j] != needle[-j - 1]:
                    i += skip.get(haystack[i], n)
                    break
            else:
                return i - n + 1
        return -1
    
    def getpacket(self, cmd):
        for i in range(5):
            tmp = self._conn.recv(1024)
            self._buf = self._buf + tmp
            idx = self.find(self._buf, KDRRWMateWorker._ETX)
            if idx > 0:
                msg = self._buf[:idx+4]
                self._buf = self._buf[idx+4:]
                print('recv packet', len(msg), '[{}]'.format(', '.join(hex(x) for x in msg)))
                if [msg[4], msg[5]] == cmd:
                    return msg
                elif [msg[4], msg[5]] == [0x01, 0x00]:
                    return None
            time.sleep(1)
        raise IOError

    def getpayload(self, msg):
        return msg[10:-8]

    def calculatechecksum(self, msg):
        #print "checksum", sum(msg)
        return sum(msg)

    def sendpacket(self, cmd, length, payload):
        msg = bytearray(KDRRWMateWorker._STX)
        msg.extend(cmd)
        msg.extend(struct.pack("<I", length))
        msg.extend(payload)
        msg.extend(struct.pack("<I", self.calculatechecksum(msg)))
        msg.extend(KDRRWMateWorker._ETX)
        print('send packet', len(msg), '[{}]'.format(', '.join(hex(x) for x in msg)))
        self._conn.send(msg)

    def getgwdk(self):
        cmd = [0x00, 0x01]
        self.sendpacket(cmd, 0, [])
        msg = self.getpacket([0x01, 0x01])
        self._gwid = self.getpayload(msg)
        print("gateway id", self._gwid)
        return ''.join(chr(x) for x in self._gwid)

    def getnodeids(self):
        cmd = [0x02, 0x01]
        self.sendpacket(cmd, 0, [])
        msg = self.getpacket([0x03, 0x01])
        self._nodes = self.getpayload(msg)[1:]
        if len(self._nodes) != self.getpayload(msg)[0]:
            self._logger.warn("numbers of nodes are mismatched. : " + str(len(self._nodes))
                              + " "  + str(self.getpayload(msg)[0]))
        print("getnodeids", list(self._nodes))
        return self._nodes

    def finddevbydevkey(self, devkey, dt, lst=None):
        """ 
        장비키와 장비타입으로 장비정보를 획득한다.
        :param devkey: 장비키
        :param dt: 장비타입
        :param lst: 장비정보를 검색할 시점. 전체에 대해 검색한다면 None 으로 하거나 입력하지 않음.
        """
        try:
            if lst is None:
                lst = self._gw["children"]

            for one in lst:
                if str(one["dk"]) == str(devkey) and DevType.issameclass(one["dt"], dt):
                    return one
                elif "children" in one:
                    dev = self.finddevbydevkey(devkey, dt, one["children"])
                    if dev is not None:
                        return dev
        except Exception as ex:
            print("Fail to find dev by devkey ?", devkey, dt, ex)

    def getnodeinfo(self, nodeid):
        cmd = [0x04, 0x01]
        self.sendpacket(cmd, 1, [nodeid])
        msg = self.getpacket([0x05, 0x01])
        if msg is None:
            self._logger.warn("fail to get packet " + str([self._gwid, nodeid]))
            return None
        payload = self.getpayload(msg)
        unpacked = struct.unpack('<' + 'BBf' * 8 + 'B' * 4, payload)
        print("unpacked", unpacked)
        nodeinfo = []
        devnode = self.finddevbydevkey(nodeid, DevType.NODE)
        for i in range(8):
            if unpacked[i * 3] != 0:
                dev = self.finddevbydevkey([nodeid, unpacked[i * 3], unpacked[i * 3 + 1]], DevType.SENSOR, devnode["children"])
                if dev == None:
                    self._logger.warn("fail to find dev : " + str([nodeid, i]))
                    continue
                nodeinfo.append({'value': unpacked[i * 3 + 2], 'id': dev["id"]})
        act = []
        for i in range(4):
            dev = self.finddevbydevkey(i, DevType.ACTUATOR, devnode["children"])
            if dev == None:
                self._logger.warn("fail to find dev : " + str(i))
                continue
            nodeinfo.append({'status': unpacked[24 + i], 'id': dev["id"]})
        return {"node": devnode, "info": nodeinfo}

    def setnodecmd(self, nodeid, nodekey, devkey):
        #cmd = [0x30, 0x01]
        cmd = [0x06, 0x01]
        payload = [nodekey] + self._relay[nodeid]
        print("set node command", payload)
        self.sendpacket(cmd, 5, payload)
        msg = self.getpacket([0x07, 0x01])
        return True

    def setreq(self, request):
        (ndid, nddk, devdk) = self.getactdevkey(request.getdevid())
        response = Response(request)
        if ndid is None:
            self._logger.warn("fail to get actuator information.")
            self._writecb(response)
            return False

        if request.getcommand() == CmdCode.OFF:
            self._relay[ndid][devdk] = 0
        elif request.getcommand() == CmdCode.ON:
            self._relay[ndid][devdk] = 1
        else:
            self._logger.warn("Wrong command : " + str(request.getcommand()))
            self._writecb(response)
            return False

        print("set request : ", self._relay[ndid])

        if self.setnodecmd(ndid, nddk, devdk):
            response.setresult(ResCode.OK)
        self._writecb(response)
        return True

    def writeblk(self, blk):
        if BlkType.isrequest(blk.gettype()):
            return self.setreq(blk)
        self._logger.warn("The message is not request. " + str(blk.gettype()))
        return False

    def sendobservation(self, nodeinfo):
        node = nodeinfo["node"]
        obsblk = Observation(node['id'])
        for info in nodeinfo['info']:
            print("info", info)
            if "value" in info:
                obsblk.setobservation(info['id'], info['value'], StatCode.READY)
            else:
                obsblk.setobservation(info['id'], 0, StatCode.WORKING if info['status'] == 1 else StatCode.READY)
        self._writecb(obsblk)
        return obsblk

    def sendnotice(self, nodeinfo):
        node = nodeinfo["node"]
        notiblk = Notice(node['id'], NotiCode.ACTUATOR_STATUS)
        for info in nodeinfo['info']:
            print("info", info)
            if "value" not in info:
                notiblk.setcontent(info['id'], {"status" : info['status']})
        self._writecb(notiblk)
        return notiblk

    def start(self):
        self._executing = True
        return super(KDRRWMateWorker, self).start()

    def stop(self):
        self._executing = False
        self._conn.close()
        return True

    def run(self):
        i = 0
        while self._executing:
            try:
                time.sleep(5)
                for ndk in self.getnodeids():
                    nodeinfo = self.getnodeinfo(ndk)
                    if nodeinfo is None:
                        self._logger.warn("fail to read from nodedk (" + str(ndk) + ").")
                    else:
                        if i % 11 == 0:
                            msg = self.sendobservation(nodeinfo)
                        if i % 2 == 0:
                            print("nodeinfo", nodeinfo)
                            msg = self.sendnotice(nodeinfo)
                i = i + 1
            
            except socket.error as exc:
                self._logger.warn("exception : " + str(exc))
                self._executing = False
                self._conn.close()
            except Exception as ex:
                self._logger.warn("exception : " + str(ex))
                print(traceback.format_exc())

    def isexecuting(self):
        return self._executing

class KDRRWMate(Mate):
    def __init__(self, option, devinfo, coupleid, logger):
        super(KDRRWMate, self).__init__(option, devinfo, coupleid, logger)
        #self._conn = option['conn']
        self._workers = {}

    def readmsg(self):
        pass

    def getnumofworkers(self):
        return len(self._workers)

    def findworkerbynodeid(self, nodeid):
        print("find worker : ", list(self._workers.keys()))

        for gw in self._devinfo:
            assert gw["dt"] == "gw"
            assert "children" in gw
            #print "gw : ", gw
            for nd in gw["children"]:
                assert nd["dt"] == "nd"
                #print "node : ", nd
                if str(nd["id"]) == str(nodeid):
                    return self._workers[gw["id"]]

        self._logger.warn("fail to find a worker by node id " + str(nodeid))
        return None

    def findgateway(self, gwdk):
        for gw in self._devinfo:
            if gw['dt'] == 'gw' and gw['dk'] == gwdk:
                return gw
        self._logger.warn("fail to find gateway - " + str(gwdk))
        return None

    def writeblk(self, blk):
        worker = self.findworkerbynodeid(blk.getnodeid())
        if worker:
            if worker.writeblk(blk) is True:
                self._logger.info("A message was sent successfully.")
                return True

        self._logger.warn("Failed to send a message to node " + str(blk.getnodeid()))
        return False

    def start(self, writecb):
        super(KDRRWMate, self).start(writecb)
        self._thd = Thread(target=self.run)
        self._thd.start()
        return True

    def stop(self):
        super(KDRRWMate, self).stop()
        for key in self._workers:
            print("worker", key, self._workers[key])
            self._workers[key].stop()
            self._workers[key].join()

        self._thd.join()
        return True

    def makeworker(self, servsoc):
        clisoc, address = servsoc.accept()
        self._logger.info("client connected from " + str(address))
        time.sleep(3)

        worker = KDRRWMateWorker({'conn':clisoc, 'writecb':self._writecb}, self._devinfo, self._logger)

        try:
            gwdk = worker.getgwdk()
        except Exception as ex:
            #worker.stop()
            #worker.join()
            self._logger.warn("fail to get gateway id " + str(ex))
            clisoc.close()
            return False

        gateway = self.findgateway(gwdk)

        if gateway:
            self._logger.info("gateway connected. " + str(gateway))
            gwid = gateway["id"]
            if gwid in self._workers and self._workers[gwid] is not None:
                self._logger.info("A worker is executing. It should be reset. " + str(gwid))
                self._workers[gwid].stop()
                self._workers[gwid].join()

            worker.setgateway(gateway)
            worker.daemon = True
            worker.start()
            self._workers[gwid] = worker
            self._logger.info("A worker is started. " + str(gwid) + " : " + str(self._workers))
            while worker.isexecuting() == False:
                self._logger.info("Waiting for the worker starting.")
                time.sleep(1)
            return True
        else:
            self._logger.warn("fail to get gateway information. " + str(gwdk))
            worker.stop()
            worker.join()
            return False

    def run(self):
        print("run", self.isexecuting())
        while self.isexecuting():
            try:
                servsoc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                servsoc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                servsoc.bind((self._option['conn']['host'], self._option['conn']['port']))
                servsoc.listen(1)
                self._logger.info("listen : " + str(self._option['conn']))

                while self.isexecuting():
                    rsoc, wsoc, esoc = select.select([servsoc], [], [], 10)
                    for sock in rsoc:
                        if sock == servsoc:
                            self._logger.info("connected a client")
                            self.makeworker(servsoc)

                    self._logger.info("Check workers.")
                    for gwid, worker in self._workers.items():
                        self._logger.info("Check worker : " + str(gwid))
                        if worker.isexecuting() == False:
                            self._logger.info("Check joining: " + str(gwid))
                            worker.join()
                    self._logger.info("server waiting : " + str(self.isexecuting()))

            except Exception as ex:
                servsoc.close()
                self._logger.warn("exception : " + str(ex))
                for gwid in list(self._workers.keys()):
                    worker = self._workers.pop(gwid, None)
                    worker.stop()
                    worker.join()
        print("finish to run")

if __name__ == "__main__":
    opt = {'conn' : {'host' : '', 'port' : 8880}}
    # 센서의 dk 설정이 중요함. (type, index) 로 됨
    # BD0012 - 우리도가, BD0034 - 우리술
    #devinfo = [{"id" : "1", "dk" : "BD0034", "dt": "gw", "children" :[
    devinfo = [{"id" : "1", "dk" : "BD0012", "dt": "gw", "children" :[
                 {"id" : "11", "dk" : 1, "dt": "nd", "children" : [
                   {"id" : "12", "dk" : (1, 0), "dt": "sen"},
                   {"id" : "13", "dk" : (1, 1), "dt": "sen"},
                   {"id" : "14", "dk" : (1, 2), "dt": "sen"},
                   {"id" : "15", "dk" : 0, "dt": "act"},
                   {"id" : "16", "dk" : 1, "dt": "act"},
                   {"id" : "17", "dk" : 2, "dt": "act"},
                   {"id" : "18", "dk" : 3, "dt": "act"}
                 ]},
                 {"id" : "21", "dk" : 2, "dt": "nd", "children" : [
                   {"id" : "22", "dk" : (2, 0), "dt": "sen"},
                   {"id" : "23", "dk" : (2, 1), "dt": "sen"},
                   {"id" : "24", "dk" : (2, 2), "dt": "sen"},
                   {"id" : "25", "dk" : 0, "dt": "act"},
                   {"id" : "26", "dk" : 1, "dt": "act"},
                   {"id" : "27", "dk" : 2, "dt": "act"},
                   {"id" : "28", "dk" : 3, "dt": "act"}
                 ]}
              ]}]
    kdmate = KDRRWMate(opt, devinfo, None)
    mate = Mate ({}, [], None)
    kdmate.start (mate.writeblk)
    print("mate started")
    time.sleep(30)

    if kdmate.getnumofworkers() > 0:
        req = Request(13)
        req.setcommand(15, CmdCode.ON, {})
        kdmate.writeblk(req)
        time.sleep(30)
        req.setcommand(15, CmdCode.OFF, {})
        kdmate.writeblk(req)
        time.sleep(10)
        req.setcommand(16, CmdCode.ON, {})
        kdmate.writeblk(req)
        time.sleep(30)
        req.setcommand(16, CmdCode.OFF, {})
        kdmate.writeblk(req)
        time.sleep(10)
        req.setcommand(17, CmdCode.ON, {})
        kdmate.writeblk(req)
        time.sleep(30)
        req.setcommand(17, CmdCode.OFF, {})
        kdmate.writeblk(req)
        #req.setcommand(5, CmdCode.ONCE_WATERING, {})
        #kdmate.writeblk(req)
        #time.sleep(3)
    else:
        print("worker is not executing.")

    time.sleep(10)
    kdmate.stop()
    print("mate stopped")

