#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#
# Cube Mxx mate for KNU
#

import time
from mate import Mate
from mate_fixedlen import FixedLengthSerialMate

'''
option : {
    "conn" : {
        "port" : "/dev/ttyUSB0",
        "baudrate": 9600,
    },
    "msgfmt" : {
        "STX" : None,
        "ETX" : None,
        "size" : 151,
        "ididx" : 110,
        "info" : [
            {"len" : 2, "scale" : 1},
            {"len" : 2, "scale" : 1},
            {"len" : 2, "scale" : 1},
            {"len" : 2, "scale" : 1},
            {"len" : 2, "scale" : 1},
            {"len" : 2, "scale" : 1},
            {"len" : 2, "scale" : 1},
            {"len" : 2, "scale" : 1}
        ]
    }
}

devinfo : [
    {"id" : "1", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "11", "dk" : "0", "dt": "sen"},
        {"id" : "12", "dk" : "1", "dt": "sen"},
        {"id" : "13", "dk" : "2", "dt": "sen"},
        {"id" : "14", "dk" : "3", "dt": "sen"},
        {"id" : "15", "dk" : "4", "dt": "sen"},
        {"id" : "16", "dk" : "5", "dt": "sen"},
        {"id" : "17", "dk" : "6", "dt": "sen"},
        {"id" : "18", "dk" : "7", "dt": "sen"}
    ]}
]
'''
class CubeMxxMate(FixedLengthSerialMate):
    def __init__(self, option, devinfo, coupleid, logger):
        super(CubeMxxMate, self).__init__(option, devinfo, coupleid, logger)

if __name__ == "__main__":
    option = {
        "conn" : {
            "port" : "/dev/ttyUSB0",
            "baudrate": 9600,
        },
        "msgfmt" : {
			"STX" : None,
			"ETX" : None,
			"size" : 151,
			"ididx" : 110,
			"info" : [
				{"len" : 2, "scale" : 1},
				{"len" : 2, "scale" : 1},
				{"len" : 2, "scale" : 1},
				{"len" : 2, "scale" : 1},
				{"len" : 2, "scale" : 1},
				{"len" : 2, "scale" : 1},
				{"len" : 2, "scale" : 1},
				{"len" : 2, "scale" : 1}
            ]
        }
    }

    devinfo = [
        {"id" : "1", "dk" : "1", "dt": "gw", "children" : [
            {"id" : "2", "dk" : "1", "dt": "nd", "children" : [
				{"id" : "11", "dk" : "0", "dt": "sen"},
				{"id" : "12", "dk" : "1", "dt": "sen"},
				{"id" : "13", "dk" : "2", "dt": "sen"},
				{"id" : "14", "dk" : "3", "dt": "sen"},
				{"id" : "15", "dk" : "4", "dt": "sen"},
				{"id" : "16", "dk" : "5", "dt": "sen"},
				{"id" : "17", "dk" : "6", "dt": "sen"},
				{"id" : "18", "dk" : "7", "dt": "sen"}
            ]}
        ]}
    ]

    mate = CubeMxxMate(option, devinfo, "1", None)
    mate2 = Mate({}, [], "1", None)
    mate.start(mate2.writeblk)
    print("mate started")
    time.sleep(30)
    mate.readmsg()
    mate.sendobs()
    mate.stop()
    print("mate stopped")

