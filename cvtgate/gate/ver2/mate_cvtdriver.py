#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

"""
    CvtDriver용 Mate를 정의함.
"""

import time
import traceback
import pycvt
from enum import IntEnum
from threading import Thread, Lock

from imports import *

from mate import Mate, ThreadMate, DevType

class CvtDriverMate(Mate):
    """
    CvtDriverMate를 정의함. PyCvtDriver 를 사용함.
    """

    def __init__(self, option, devinfo, coupleid, logger=None):
        """
        Mate 의 Constructor. option과 devinfo를 주요 입력으로 함.
        
        :param option: 작동을 위한 설정을 딕셔너리로 전달함
        :param devinfo: 처리하는 장비의 아이디를 딕셔너리 형식으로 전달함. 다음과 같은 형식임.
        id 는 장비의 아이디, dk 는 장비를 확인하기 위한 키값, dt는 장비의 타입, children은 하위 장비가 있는 경우에 하위 장비를 표현하기 위한 용도임.
        devinfo : [
            {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "4", "dk" : "0", "dt": "sen"},
                {"id" : "5", "dk" : "1", "dt": "sen"},
                {"id" : "6", "dk" : "2", "dt": "act"},
                {"id" : "7", "dk" : "3", "dt": "act/retractable/level0"}
            ]}
        ]
        :param logger: 로깅을 위한 로거. 없다면 내부적으로 만듬.
        """
        super(CvtDriverMate, self).__init__(option, devinfo, coupleid, logger)
        self._pycvt = pycvt.PyCvtDriver(option['config'])

    def writeblk(self, blk):
        """ 외부에서 데이터 전달을 위해 호출되는 메소드. CvtDriver 로 제어는 수행하지 않음 """
        # external callback
        print("received message", blk.getdevid(), self._coupleid)
        if BlkType.isrequest(blk.gettype()) is False:
            self._logger.warn("The message is not request. " + str(blk.gettype()))
            return False

        response = Response(blk)
        response.setresult(ResCode.OK)
        self.writecb(response)
        return True

    def readmsg(self):
        """ Mate가 메세지를 읽는 함수. 직접구현해야함.  """
        pass
        
    def sendobs(self):
        """ 관측치를 전송한다. writecb를 사용함. """
        self._pycvt.preprocess()
        # 첫번째 게이트웨이의 첫번째 노드의 센서 
        gw = self._devinfo.getgw()
        obs = Observation(gw["children"][0]["id"])
        for dev in gw["children"][0]["children"]:
            if DevType.issensor(dev["dt"]):
                val = self._pycvt.getvalue(self._pycvt.getdevice(int(dev["dk"])))
                obs.setobservation(dev["id"], val)
        self._pycvt.postprocess()
        self.writecb(obs)

if __name__ == "__main__":
    opt = {"config" : "conf/pycvtdriver.json"}
    devinfo = [{
        "id" : "1", "dk" : "", "dt": "gw", "children" : [{
            "id" : "101", "dk" : '', "dt": "nd", "children" : [
                {"id" : "102", "dk" : '0', "dt": "sen"},
                {"id" : "103", "dk" : '1', "dt": "sen"},
                {"id" : "104", "dk" : '2', "dt": "sen"}
            ]
        }]
    }]

    mate = CvtDriverMate(opt, devinfo, "1", None)
    mate2 = Mate({}, [], "1", None)
    mate.start(mate2.writeblk)
    print("mate started")
    time.sleep(10)
    mate.stop()
    print("mate stopped")
