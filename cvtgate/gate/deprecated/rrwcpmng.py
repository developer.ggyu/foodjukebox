#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import sys
import time
import importlib
import json
#import paho.mqtt.subscribe as subscribe
from queue import Queue
from daemon import Daemon, Runner
from mate import Mate
from mblock import MBlock, BlkType

class CoupleManager(Runner):
    _couples = []

    def __init__(self, configfile):
        fp = open(configfile, 'r')
        self._option = json.loads(fp.read())
        fp.close()

    def loadcandidates(self):
        couples = [{
            'id': '1',
            'ssmate': {
                'mod':'mate_rrwdb', 
                'class':'RRWDBMate', 
                'opt':{
                    "conn" : {"host" : "localhost", "user" : "root", "password" : "jinong", "db" : "iot_rrw"}
                }
            },
            'dsmate': {
                'mod':'mate_kdrrw',
                'class':'KDRRWMate', 
                'opt':{'conn' : {'host' : '', 'port' : 8880}}
            },
            'devinfo': [{"id" : "1", "dk" : "BD0034", "dt": "gw", "children" :[
                {"id" : "11", "dk" : 1, "dt": "nd", "children" : [
                   {"id" : "12", "dk" : (1, 0), "dt": "sen"},
                   {"id" : "13", "dk" : (1, 1), "dt": "sen"},
                   {"id" : "14", "dk" : (1, 2), "dt": "sen"},
                   {"id" : "15", "dk" : 0, "dt": "act"},
                   {"id" : "16", "dk" : 1, "dt": "act"},
                   {"id" : "17", "dk" : 2, "dt": "act"},
                   {"id" : "18", "dk" : 3, "dt": "act"}
                 ]},
                 {"id" : "21", "dk" : 2, "dt": "nd", "children" : [
                   {"id" : "22", "dk" : (2, 0), "dt": "sen"},
                   {"id" : "23", "dk" : (2, 1), "dt": "sen"},
                   {"id" : "24", "dk" : (2, 2), "dt": "sen"},
                   {"id" : "25", "dk" : 0, "dt": "act"},
                   {"id" : "26", "dk" : 1, "dt": "act"},
                   {"id" : "27", "dk" : 2, "dt": "act"},
                   {"id" : "28", "dk" : 3, "dt": "act"}
                 ]}
            ]}, {"id" : "2", "dk" : "BD0012", "dt": "gw", "children" :[
                {"id" : "31", "dk" : 1, "dt": "nd", "children" : [
                   {"id" : "32", "dk" : (1, 0), "dt": "sen"},
                   {"id" : "33", "dk" : (1, 1), "dt": "sen"},
                   {"id" : "34", "dk" : (1, 2), "dt": "sen"},
                   {"id" : "35", "dk" : 0, "dt": "act"},
                   {"id" : "36", "dk" : 1, "dt": "act"},
                   {"id" : "37", "dk" : 2, "dt": "act"},
                   {"id" : "38", "dk" : 3, "dt": "act"}
                 ]},
                 {"id" : "41", "dk" : 2, "dt": "nd", "children" : [
                   {"id" : "42", "dk" : (2, 0), "dt": "sen"},
                   {"id" : "43", "dk" : (2, 1), "dt": "sen"},
                   {"id" : "44", "dk" : (2, 2), "dt": "sen"},
                   {"id" : "45", "dk" : 0, "dt": "act"},
                   {"id" : "46", "dk" : 1, "dt": "act"},
                   {"id" : "47", "dk" : 2, "dt": "act"},
                   {"id" : "48", "dk" : 3, "dt": "act"}
                 ]}
            ]}]
        }]

        return couples

    def popmate(self, candy):
        for idx in range(len(self._couples)):
            if self._couples[idx]['id'] == candy['id']:
                return self._couples.pop(idx)
        return None

    def execute(self, candy):
        ssmate = candy['ssmate']
        dsmate = candy['dsmate']
        devinfo = candy['devinfo']
        couple = {}
        couple['id'] = candy['id']
        couple['ssmate'] = self.loadmate(ssmate, devinfo)
        couple['dsmate'] = self.loadmate(dsmate, devinfo)
        couple['ssmate'].start(couple['dsmate'].writeblk)
        couple['dsmate'].start(couple['ssmate'].writeblk)
        return couple

    def loadmate(self, conf, devinfo):
        module = importlib.import_module(conf['mod'])
        class_ = getattr(module, conf['class'])
        mate = class_(conf['opt'], devinfo, self.logger)
        return mate

    def stopold(self):
        for couple in self._couples:
            couple['ssmate'].stop()
            couple['dsmate'].stop()
            print(couple['id'], "stopped")

    def stop(self):
        print("Couple Manager tries to stop")
        self._isrunning = False

    def run(self):
        self._isrunning = True
        i = 0
        while self._isrunning:
            if i % 10 == 0:
                newcouples = []
                candidates = self.loadcandidates()
                for candy in candidates:
                    couple = self.popmate(candy)
                    if couple is None:
                        couple = self.execute(candy)
                    newcouples.append(couple)

                self.stopold()
                self._couples = newcouples
                i = 0
            i = i + 1
            time.sleep(self._option['sleep'])
        self.stopold()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage : python couplemanager.py [start|stop|restart|run]")
        sys.exit(2)

    mode = sys.argv[1]
    runner = CoupleManager('cpmng.conf')
    adaemon = Daemon('cpmng', runner)
    if 'start' == mode:
        adaemon.start()
    elif 'stop' == mode:
        adaemon.stop()
    elif 'restart' == mode:
        adaemon.restart()
    elif 'run' == mode:
        adaemon.run()
    else:
        print("Unknown command")
        sys.exit(2)
    sys.exit(0)
