#!/usr/bin/env python -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc. 
# All right reserved.
#
# Online AWS
#

import sys
import requests
import json
import time

from .. import *

sys.path.append("./kist")

from sensor_climate import SqliteDB
import sqlite3

class KistSensorMate(DSMate):
    def read100(self, db):
        try:
            data = db._readclimate()
        except Exception as ex:
            self._logger.warn ("Fail to make Kist sensor class : " + str(ex))
            data = [None] * 10
        return data

    def read200(self, db):
        try:
            data = db._readall()
        except Exception as ex:
            self._logger.warn ("Fail to make Kist sensor class : " + str(ex))
            data = [None] * 11
        return data

    def read300(self, db):
        data = []
        for key in range(4, 10):
            try:
                temp = db.read3('sensor-' + str(key))
                data.extend(list(temp))
            except Exception as ex:
                self._logger.warn ("Fail to make Kist sensor class : " + str(ex))
                data.extend([None, None, None])
        return data

    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE) is False:
            return

        self._updatetime(Mate.OBSTYPE)

        try:
            db = SqliteDB()
            if "method" in self._option and self._option["method"] == "all":
                data = self.read200(db)

            elif "method" in self._option and self._option["method"] == "soil":
                data = self.read300(db)

            else:
                data = self.read100(db)

            gw = self._devinfo.getgw()
            nd = gw["children"][0]
            obsblk = Observation(nd["id"])

            for dev in nd["children"] :
                if data[int(dev["dk"])] is None:
                    obsblk.setobservation(dev["id"], None, StatCode.ERROR)
                else:
                    obsblk.setobservation(dev["id"], self.getvalue(dev["id"], data[int(dev["dk"])]), StatCode.READY)

            self._writeobs(obsblk)

        except Exception as ex:
            self._logger.warn ("Fail to make Kist sensor class : " + str(ex))

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
    }

    devinfo = [{
        "id" : "1", "dk" : '', "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id" : "3", "dk" : "1", "dt": "sen"},
                {"id" : "4", "dk" : "2", "dt": "sen"},
                {"id" : "5", "dk" : "3", "dt": "sen"},
                {"id" : "6", "dk" : "4", "dt": "sen"},
                {"id" : "7", "dk" : "5", "dt": "sen"}
            ]
        }]
    }]

    quelist = [Queue(), Queue(), Queue(), Queue()] 
    mate = KistSensorMate(option,devinfo,"1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()
