#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 FarmOS, Inc.
# All right reserved.
#

import os
import subprocess
import traceback
import shlex
import time
from datetime import datetime

from .. import *
from lib.dsmate.mate_extproc import _ExtProcMate

class WebCamDahuaMate(_ExtProcMate):
    def makecmdline(self, fname):
        if "user" in self._option["extproc"] and "passwd" in self._option["extproc"]:
            url = self._option["extproc"]["user"] + ":" + self._option["extproc"]["passwd"] + "@"
        else:
            url = "admin:jinong1234%"
        url = url + self._option["extproc"]["ip"]
        cmdline = self._option["extproc"]["cmdfmt"] % (url, fname)
        print (cmdline)
        return cmdline

    def processrequest(self, req):
        response = Response(req)
        cmd = req.getcommand()

        print ("processrequest of WebCam")
        if cmd == CmdCode.TAKE_PICTURE:
            devid = str(req.getdevid())
            self._stats[devid] = StatCode.WORKING
            self.sendnotiforact(req.getnodeid(), devid)
            
            fname = datetime.now().strftime("%Y%m%d-%H%M") + ".jpg"
            path = self._option["extproc"]["base"] + fname
            cmdline = self.makecmdline(path)
            ret = self.execute(cmdline)
            extra = {
                "deviceId": req.getdevid(), 
                "date" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"), 
                "meta" : None
            }
            if self.upload(Mate.IMGTYPE, path, extra):
                os.remove(path)
            # process rep 
            self._stats[devid] = StatCode.READY
            self.sendnotiforact(req.getnodeid(), devid)
            response.setkeyvalue("fname", fname)
            code = ResCode.OK
        else:
            code = ResCode.FAIL
        response.setresult(code)
        self._writeres(response)
        return response

if __name__ == "__main__":
    from multiprocessing import Queue

    opt = {
        "upload" : {"URL" : "http://localhost:8081/common/v1/upload"},
        "extproc" : {
            "cmdfmt" : "ffmpeg -y -loglevel debug -rtsp_transport tcp -i 'rtsp://%s' %s",
            "stdout" : False,
            "base" : "picture/",
            "user" : "admin",
            "passwd" : "jinong1234%",
            "ip" : "169.254.5.201"
        }
    }
    devinfo = [
        {"id" : "1", "dk" : "1", "dt": "gw", "children" : [
            {"id" : "2", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "47", "dk" : "0", "dt": "act/camera/level0"}
            ]}
        ]}
    ]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    req = Request(2)
    req.setcommand(47, CmdCode.TAKE_PICTURE, {})

    ssmate = SSMate ({}, [], "1", quelist)
    dsmate = WebCamDahuaMate(opt, devinfo, "1", quelist)
    ssmate.start ()
    dsmate.start ()
    print("mate started")
    ssmate._writereq(req)
    time.sleep(60)
    ssmate.stop()
    dsmate.stop()
    print("mate stopped")


