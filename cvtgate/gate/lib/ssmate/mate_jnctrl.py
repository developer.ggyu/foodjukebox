#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#
# 지농 관제 연동을 위한 메이트

import json
import sys
import time
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from collections import deque

from .. import *

from .fdev import *
from .dbmng import MySQLManager
from .rdaiot import RDAIoTManager

class JNCDBManager(MySQLManager):
    def loaddevs(self, serials = None):
        if serials is None:
            query = "select INSTL_EQPMN_ID, LINK_SRVC_ID, LINK_GTWY_ID, GTWY_ID, NODE_ID, EQPMN_ID, EQPMN_ETC_CN from TB_CNTC_FARMOS_EQPMN_M where USE_YN = 'Y'"
            rows = self._select(query, [])
        else:
            query = "select INSTL_EQPMN_ID, LINK_SRVC_ID, LINK_GTWY_ID, GTWY_ID, NODE_ID, EQPMN_ID, EQPMN_ETC_CN from TB_CNTC_FARMOS_EQPMN_M where USE_YN = 'Y' and LINK_SRVC_ID in %s"
            rows = self._select(query, [serials])
        eqs = {}
        devs = {}
        for dev in rows:
            eqs[dev[0]] = dev
            if dev[5]:
                devs[":".join([str(dev[2]), str(dev[5])])] = dev[0]    # couple_id, dev_id
            elif dev[4]:
                devs[":".join([str(dev[2]), str(dev[4])])] = dev[0]    # couple_id, node_id
            elif dev[3]:
                devs[":".join([str(dev[2]), str(dev[3])])] = dev[0]    # couple_id, gw_id
            else:
                devs[":".join([str(dev[2]), str(None)])] = dev[0]    # couple_id, None
        print ("load devices : ", devs)
        return (eqs, devs)

    def readrequests(self, serials):
        query = "select CMMND_ID, INSTL_EQPMN_ID, CMMND_VALUE, CMMND_ETC_CN, REQUST_DT from TB_CNTC_FARMOS_EQPMN_CTRL_CMMND_M where (CMMND_ID, INSTL_EQPMN_ID) NOT IN (SELECT CMMND_ID, INSTL_EQPMN_ID FROM TB_CNTC_FARMOS_EQPMN_CTRL_CMMND_RSPNS_M) and INSTL_EQPMN_ID IN (SELECT INSTL_EQPMN_ID FROM TB_CNTC_FARMOS_EQPMN_M where LINK_SRVC_ID in %s) order by REQUST_DT desc"
        rows = self._select(query, [serials])
        self._commit()

        return rows

    def processrequest(self, request, stat=ResCode.WAITING):
        request.append (stat.value)
        query = "insert into TB_CNTC_FARMOS_EQPMN_CTRL_CMMND_RSPNS_M(CMMND_ID, INSTL_EQPMN_ID, TRNSMIS_DT, RSPNS_DT, RSPNS_STTUS_VALUE, RSPNS_ETC_CN) values (%s, %s, now(), NULL, %s, NULL)"
        self._execute(query, request)

    def updateresponse(self, response):
        query = "update TB_CNTC_FARMOS_EQPMN_CTRL_CMMND_RSPNS_M set RSPNS_DT = now(), RSPNS_STTUS_VALUE = %s, RSPNS_ETC_CN = %s where CMMND_ID = %s and INSTL_EQPMN_ID = %s"
        self._execute(query, response)

    def updateflowsensorobservation(self, obs):
        query = "insert into TB_CNTC_FARMOS_EQPMN_OBSR_VALUE_H(INSTL_EQPMN_ID, OBSR_DT, OBSR_VALUE, INSERT_DT) select %s, %s, %s - OBSR_VALUE, now() from TB_CNTC_FARMOS_EQPMN_OBSR_VALUE_H where INSTL_EQPMN_ID = %s and date(OBSR_DT) = date(DATE_ADD(%s, INTERVAL -1 DAY)) and OBSR_VALUE is not null order by OBSR_DT desc limit 1"
        self._execute(query, obs)

    def selectflowsensorobservation(self, eqid, tm):
        query = "select OBSR_VALUE from TB_CNTC_FARMOS_EQPMN_OBSR_VALUE_H where INSTL_EQPMN_ID = %s and date(OBSR_DT) = date(DATE_ADD(%s, INTERVAL -1 DAY)) and OBSR_VALUE is not null order by OBSR_DT desc limit 1"
        return self._select(query, [eqid, tm])

    def updateobservation(self, obs):
        #query = "insert into TB_CNTC_FARMOS_EQPMN_OBSR_VALUE_H(INSTL_EQPMN_ID, OBSR_DT, OBSR_VALUE) values (%s, %s, %s)"
        query = "insert into TB_CNTC_FARMOS_EQPMN_OBSR_VALUE_H(INSTL_EQPMN_ID, OBSR_DT, OBSR_VALUE, INSERT_DT, ESTIMATED_VALUE) values (%s, %s, %s, now(), %s)"
        #print (query, obs)
        self._execute(query, obs)

    def updatestatus(self, stat):
        query = "insert into TB_CNTC_FARMOS_EQPMN_STTUS_H(INSTL_EQPMN_ID, RECPTN_DT, STTUS_VALUE, STTUS_ETC_CN) values (%s, %s, %s, %s)"
        self._execute(query, stat)

    def updatemate(self, eqid, coupleid, stat):
        if stat == StatCode.DISCONNECTED:
            query = "insert into TB_CNTC_FARMOS_EQPMN_STTUS_H(INSTL_EQPMN_ID, RECPTN_DT, STTUS_VALUE, STTUS_ETC_CN) select INSTL_EQPMN_ID, now(), %s, '' from TB_CNTC_FARMOS_EQPMN_M where LINK_GTWY_ID = %s and USE_YN = 'Y'"
            self._execute(query, [stat, coupleid])

            query = "update TB_CNTC_FARMOS_EQPMN_CTRL_CMMND_RSPNS_M set RSPNS_DT = now(), RSPNS_STTUS_VALUE = %s where INSTL_EQPMN_ID IN (select INSTL_EQPMN_ID from TB_CNTC_FARMOS_EQPMN_M where LINK_GTWY_ID = %s and USE_YN = 'Y') AND RSPNS_STTUS_VALUE = 1001"
            self._execute(query, [ResCode.DISCONNECTED.value, coupleid])
        else:
            query = "insert into TB_CNTC_FARMOS_EQPMN_STTUS_H(INSTL_EQPMN_ID, RECPTN_DT, STTUS_VALUE, STTUS_ETC_CN) values (%s, now(), %s, '')"
            self._execute(query, [eqid, stat])

'''
option : {
    "conn" : {"host" : "dev.jinong.co.kr", "port" : 1883, "keepalive" : 60},
    "mqtt" : {"svc" : "cvtgate", "id" : "1"},
    "area" : "local"
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

# bypass 만 하는걸로 일단 정리한다.
class JNCtrlMate(SSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(JNCtrlMate, self).__init__(option, devinfo, coupleid, quelist)
        self._jnmqtt = SimpleJNMQTT(option, self._logger)
        self._dbm = JNCDBManager(option, self._logger)
        self._rda = RDAIoTManager(option, self._logger)
        self._msgq = deque()
        self._serials = option["serials"]
        if "timelimit" in option:
            self._timelimit = option["timelimit"]
        else:
            self._timelimit = 5
        if "flowsensors" not in self._option:
            self._option["flowsensors"] = []
            self._option["dailyflowsensors"] = []
        self._eqs = None
        self._devs = None 

    def connect(self):
        self._jnmqtt.connect()
        self._dbm.connect()
        super(JNCtrlMate, self).connect()
        self._eqs, self._devs = self._dbm.loaddevs(self._serials)
        return True

    def close(self):
        self._jnmqtt.close()
        self._dbm.close()
        super(JNCtrlMate, self).close()

    def matestart(self):
        self._jnmqtt.start(self.onmsg)
        super(JNCtrlMate, self).matestart()
        self.connect()

    def matestop(self):
        self.close()
        self._jnmqtt.stop()
        super(JNCtrlMate, self).matestop()

    def doextra(self):
        self.checkmessage()
        self.checkcommand()
        self._jnmqtt.checkconnection()

    def _findndid(self, eqid):
        return self._eqs[eqid][4]

    def _finddevid(self, eqid):
        return self._eqs[eqid][5]

    def _finddev(self, eqid):
        return self._eqs[eqid]

    def _findeqid(self, topic, devid):
        # devs[":".join([str(dev[2]), str(dev[5])])] = dev[0]    # couple_id, dev_id
        #print ("findeqid", topic, devid)
        key = ":".join(map(str, [topic[1], devid]))
        if key in self._devs:
            return self._devs[key]
        else:
            #self._logger.warn("Need to check devices. " + key)
            return None

    def checkcommand(self):
        senteqs = []
        for cmd in self._dbm.readrequests(self._serials):
            if cmd[1] in senteqs:
                self._dbm.processrequest([cmd[0], cmd[1]], ResCode.IGNORED)
                continue

            if cmd[4] < datetime.datetime.now() - datetime.timedelta(minutes=self._timelimit):
                self._dbm.processrequest([cmd[0], cmd[1]], ResCode.TOO_LATE)
                continue

            dev = self._finddev(cmd[1])
            req = Request(dev[4])       # self._findndid(cmd[1]))
            time = cmd[4].strftime("%Y-%m-%d %H:%M:%S")
            req.setcommand(dev[5], cmd[2], json.loads(cmd[3] if cmd[3] else '{}'), cmd[0], time)
            self._jnmqtt.writeblk(req, dev)
            self._dbm.processrequest([cmd[0], cmd[1]])
            senteqs.append (cmd[1])

    def checkmessage(self):
        while True:
            try:
                topic, msg = self._msgq.popleft()
            except:
                return

            topics = topic.split('/')
            if _JNMQTT._SELF == topics[2] and _JNMQTT._STAT == topics[3]:
                self._logger.info("Update Mate: " + topic + msg.stringify())
                self._dbm.updatemate(self._findeqid(topics, None), topics[1], msg.getcontent()['status'])

            elif BlkType.isnotice(msg.gettype()):
                if msg.getcode() == NotiCode.OBSERVATIONS:
                    tm = msg.gettime()
                    for devid, obs in msg.getcontent().items():
                        if devid in ['time', 'code']:
                            continue
                        eqid = self._findeqid(topics, devid)
                        if eqid:
                            if eqid in self._option["flowsensors"]:
                                idx = self._option["flowsensors"].index(eqid)
                                tmp = self._dbm.selectflowsensorobservation(eqid, tm)

                                self._dbm.updateobservation([eqid, tm, obs[1], obs[2]])
                                self._dbm.updateobservation([self._option["dailyflowsensors"][idx], tm, obs[1] - tmp[0], obs[2] - tmp[0]])

                                self._rda.updateobservation(eqid, tm, obs[1])
                                self._rda.updateobservation(self._option["dailyflowsensors"][idx], tm, obs[1] - tmp[0])

                                #self._dbm.updateflowsensorobservation([self._option["dailyflowsensors"][idx], tm, obs[1], eqid, tm])
                            else:
                                self._dbm.updateobservation([eqid, tm, obs[1], obs[2]])
                                self._rda.updateobservation(eqid, tm, obs[1])

                            self._dbm.updatestatus([eqid, tm, obs[0], ''])

                elif msg.getcode() == NotiCode.ACTUATOR_STATUS:
                    tm = msg.gettime()
                    for devid, stat in msg.getcontent().items():
                        if devid in ['time', 'code']:
                            continue
                        eqid = self._findeqid(topics, devid)
                        if eqid:
                            self._dbm.updatestatus([eqid, tm, stat["status"], json.dumps(stat)])
                    eqid = self._findeqid(topics, topics[2])
                    if eqid:
                        self._dbm.updatestatus([eqid, tm, StatCode.READY.value, ''])

            elif BlkType.isresponse(msg.gettype()):
                devid = msg.getdevid()
                eqid = self._findeqid(topics, devid)

                if "camera" in self._option and int(devid) in self._option["camera"]:
                    content = msg.getcontent()
                    print ("camera", content)
                    if 'fname' in content:
                        extra = {'newfname' : str(eqid) + "/" + content["fname"]}
                        fname = self._option["upload"]["SFTP"]["base"] + "/" + content["fname"]
                        self._logger.info("camera image was saved on " + fname + " " + str(extra))
                        self.uploadsftp(Mate.IMGTYPE, fname, extra)
                    else:
                        self._logger.warn("It's a response from camera but no file name.: " + msg.stringify())

                self._dbm.updateresponse([msg.getresult(), json.dumps(msg.getcontent()), msg.getopid(), eqid])
                """
            # Observation 에 대해서는 작업하지 않는다.
            elif BlkType.isobservation(msg.gettype()):
                tm = msg.gettime()
                eqid = self._findeqid(topics, topics[2])
                if eqid:
                    self._dbm.updatestatus([eqid, tm, StatCode.READY.value, ''])
                eqid = self._findeqid(topics, msg.getnodeid())
                if eqid:
                    self._dbm.updatestatus([eqid, tm, StatCode.READY.value, ''])
                """
            else:
                #self._logger.warn("Wrong message : " + topic + msg.stringify())
                pass


    def onmsg(self, client, obj, blk):
        """ MQTT 로 받는 메세지는 Request """
        print(("JNCtrlMate Received mblock '" + str(blk.payload) + "' on topic '"
              + blk.topic + "' with QoS " + str(blk.qos)))

        msg = self._jnmqtt.getpropermsg(blk)
        if msg is None:
            self._logger.warn("The message is not proper " + str(blk))
            return None

        self._msgq.append((blk.topic, msg))

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        'serials': ['78bf-8e1e-0ace'],
        'conn': {'host': 'farmos003.jinong.co.kr', 'port': 1883, 'keepalive': 60}, 
        'db': {'host': '220.90.133.21', 'user': 'smartLink', 'password': 'smartLink1!', 'db': 'smartLink'}, 
        'mqtt': {'svc': 'cvtgate', 'id': '#'}
    }

    devinfo = []

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = JNCtrlMate(option, devinfo, "1", quelist)
    dsmate = DSMate(option, devinfo, "1", quelist)
    ssmate.start()
    dsmate.start()

    cmd = Request(1, None)
    cmd.setcommand(12, 'on', {})

    publish.single("cvtgate/1/1/req/1", cmd.stringify(), hostname="farmos003.jinong.co.kr")
    print("published")

    time.sleep(60)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    dsmate.stop()
    ssmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

    print("local tested.")
