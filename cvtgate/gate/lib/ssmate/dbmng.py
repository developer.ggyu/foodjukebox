#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 FarmOS, Inc.
# All right reserved.
#
# mate for interaction with farmos 

import json
import time
import datetime
import pymysql
from cachetools import cached, TTLCache
from .farmos import FarmOS

'''
option : {
    "db" : {"host" : "localhost", "user" : "root",
              "password" : "pass", "db" : "db"}
}

'''

class DBManager:
    def __init__(self, option, logger):
        self._isconnected = False
        self._conn = None
        if "db" in option:
            self._option = option["db"]
        else:
            self._option = {"host" : "localhost", "user" : "farmos", "password" : "farmosv2@", "db" : "farmos"}
        self._logger = logger
        self._retries = []
        self._cache = {}

    def connect(self):
        pass

    def close(self):
        pass

    def _select(self, query, params):
        pass

    def _execute(self, query, params):
        pass

    def _commit(self):
        pass

class MySQLManager(DBManager):
    def _connect(self, opt):
        conn = pymysql.connect(host=opt["host"], user=opt["user"],
                                     password=opt["password"], db=opt["db"])
        cur = conn.cursor()
        return (conn, cur)

    def connect(self):
        self._conn, self._cur = self._connect(self._option)
        self._isconnected = True
        self._logger.info("DB Connection initialized.")
        temp = self._retries
        self._retries = []
        for q, p in temp:
            self._execute(q, p)

    def close(self):
        self._cur.close()
        self._conn.close()
        self._isconnected = False
        self._logger.info("DB Connection closed.")

    def _select(self, query, params, skiperr=False):
        try:
            if self._isconnected is False:
                self.connect()
            self._conn.ping(True)
            self._cur.execute(query, params)
            rows = self._cur.fetchall()
            print("_select", query, params, rows)
            return rows
        except pymysql.IntegrityError as ex:
            #self._logger.info("DB Integrity exception : " + str([ex, query, params]))
            pass
        except Exception as ex:
            print("DB other exception : " + str([ex, query, params]))
            self._logger.warn("DB other exception : " + str([ex, query, params]))
            if skiperr:
                return None
            self.close()
        return None

    def _execute(self, query, params, skiperr=False):
        try:
            if self._isconnected is False:
                self.connect()
            self._conn.ping(True)
            self._cur.execute(query, params)
            self._conn.commit()
            #print(query, params)
        except pymysql.IntegrityError as ex:
            #self._logger.info("DB Integrity exception : " + str([ex, query, params]))
            pass
        except Exception as ex:
            self._logger.warn("DB other exception : " + str([ex, query, params]))
            self._retries.append([query, params])
            if skiperr:
                return None
            self.close()

    def _commit(self):
        try:
            if self._isconnected is False:
                self.connect()
            self._conn.commit()
        except Exception as ex:
            self._logger.info("commit exception : " + str(ex))

class FDBManager(MySQLManager):
    _CODESET = {
        "status" : 0, 
        "value" : 1,
        "position" : 2, 
        "state-hold-time" : 3,
        "remain-time": 4,
        "ratio" : 5,
        "control" : 6,
        "area" : 7,
        "alert" : 8,
        "rawvalue" : 9,
        "image" : 10,
        "availability" : 11,
        "estimated" : 12,
        "vcnt" : 21,
        "vavg" : 22,
        "vmax" : 23,
        "vmin" : 24,
        "vstd" : 25,
        "diffstd" : 26
    }

    @cached(cache=TTLCache(maxsize=1024, ttl=60))
    def isextcontrollable(self, did, farmid=None):
        _QUERY = "select control from devices where id = %s and deleted = 0"
        self._commit()
        rows = self._select(_QUERY, [did])
        if rows[0][0] >= 10:
            return True
        return False

    @cached(cache=TTLCache(maxsize=1024, ttl=180))
    def istransferable(self, did, farmid=None):
        _QUERY = "select control from devices where id = %s and deleted = 0"
        self._commit()
        rows = self._select(_QUERY, [did])
        if rows[0][0] >= 10 or rows[0][0] == 1:
            return True
        return False

    def isgooddeviceid(self, did, farmid=None):
        _QUERY = "select id from devices where id = %s and deleted = 0"
        rows = self._select(_QUERY, [did])
        if rows:
            return True
        return False

    def getlastsensorinfo(self, sid, farmid=None):
        valid = FarmOS.getdataid(sid, "value")
        sigmaid = FarmOS.getdataid(sid, "diffstd")
        _SENINFO_QUERY = "select data_id, obs_time, nvalue from current_observations where data_id in %s"
        rows = self._select(_SENINFO_QUERY, [[valid, sigmaid]])
        if rows and len(rows) == 2:
            if rows[0][0] == valid:
                return (rows[0][2], rows[0][1], rows[1][2])
            else:
                return (rows[1][2], rows[1][1], rows[0][2])
        elif rows and len(rows) == 1: # no diffstd
            return (rows[0][2], rows[0][1], None)
        return (None, None, None)

    def insertrequest(self, request, farmid=None):
        _REQINS_QUERY = "insert requests(opid, device_id, command, params) values(%s, %s, %s, %s)"
        content = request.getcontent()
        params = [content["opid"], content["id"], content["cmd"], json.dumps(content["param"])]
        self._execute(_REQINS_QUERY, params)

    def execrequest(self, response, farmid=None):
        _REQUPS_QUERY = "update requests set status = %s, exectime = now() where opid = %s and device_id = %s and senttime = (select stime from (select max(senttime) stime from requests where opid = %s and device_id = %s) tmp)"
        content = response.getcontent()
        params = [content["res"], content["opid"], content["id"], content["opid"], content["id"]]
        self._execute(_REQUPS_QUERY, params)

    def finishrequest(self, did, opid, farmid=None):
        _REQFIN_QUERY = "update requests set finishtime = now() where opid = %s and device_id = %s and senttime = (select stime from (select max(senttime) stime from requests where opid = %s and device_id = %s) tmp)"
        self._execute(_REQFIN_QUERY, [opid, did, opid, did])

    def unfinishrequest(self, did, opid, farmid=None):
        _REQFUP_QUERY = "update requests set finishtime = null where opid = %s and device_id = %s and senttime = (select stime from (select max(senttime) stime from requests where opid = %s and device_id = %s) tmp)"
        self._execute(_REQFUP_QUERY, [opid, did, opid, did])

    def writeobservation(self, sid, tm, values, farmid=None):
        dataid = FarmOS.getdataid(sid, "status")
        codes = [FarmOS._CODESET["status"], FarmOS._CODESET["value"], 
                FarmOS._CODESET["rawvalue"], FarmOS._CODESET["estimated"]]
        for idx, val in enumerate(values):
            #self._logger.debug("Write data: " + str([tm, val, dataid + codes[idx]]))
            self._writedata([tm, val, dataid + codes[idx]])

    def updatestatus(self, devid, content, farmid=None):
        self.writestatus(devid, content, option="onlyupdate")

    def writestatus(self, devid, content, option=None, farmid=None):
        tm = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        #self._logger.debug("Write status : " + str([devid, tm]))
        for key, value in content.items():
            if key == "opid":
                continue
            dataid = FarmOS.getdataid(devid, key)
            self._writedata([tm, value, dataid], option=option)

    def _writedata(self, params, option=None):
        # params = [time, nvalue, dataid]
        if params[2] is None:
            return 0

        _UPDOBS_QUERY = "update current_observations set obs_time = %s, nvalue = %s where data_id = %s"
        _INSOBS_QUERY = "insert observations(obs_time, nvalue, data_id) values(%s, %s, %s)"
        _DUPOBS_QUERY = "insert observations(obs_time, nvalue, data_id) values(%s, %s, %s) ON DUPLICATE KEY UPDATE nvalue = %s"
        #params = [time, nvalue, dataid]
        self._execute(_UPDOBS_QUERY, params)
        if option == "updatable":
            params.append(nvalue)
            self._execute(_DUPOBS_QUERY, params)
        elif option != "onlyupdate":
            self._execute(_INSOBS_QUERY, params)
        return 0

    # Special Method for DataEstimationModel
    def getcurrentdata(self, dataid, farmid=None):
        _SELOBS_QUERY = "select unix_timestamp(obs_time), nvalue from current_observations where data_id = %s" 
        rows = self._select(_SELOBS_QUERY, [dataid])
        return rows[0]

    # Special Method for DataEstimationModel
    def getdataduring(self, dataid, fromtime, totime, farmid=None):
        _SELOBS_QUERY = "select unix_timestamp(obs_time), nvalue from observations where data_id = %s and obs_time between from_unixtime(%s) and from_unixtime(%s)"
        return self._select(_SELOBS_QUERY, [dataid, fromtime, totime])

    def writedevicecontrol(self, ndid, control, farmid=None):
        if control == 3: # 3 이면 전체다 업데이트
            _QUERY = "update devices set control = %s where nodeid = (select nodeid from (select id, nodeid from devices) t where id = %s) and spec like '%%actuator%%'"
        else:   # 3 이었던 것들만 업데이트
            _QUERY = "update devices set control = %s where control = 3 and nodeid = (select nodeid from (select id, nodeid from devices) t where id = %s) and spec like '%%actuator%%'"
        return self._execute(_QUERY, [control, ndid])

if __name__ == "__main__":
    import time
    import logging
    opt = {"db" : {"host" : "localhost", "user" : "farmos", "password" : "farmosv2@", "db" : "farmos"}}
    dm = FDBManager(opt, logging)
    dm.connect()
    ret = dm.getdataduring("10000201", time.time() - 7 * 60, time.time())
    tm = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ret[0][0]))
    print ("getduring", tm, len(ret), ret)

