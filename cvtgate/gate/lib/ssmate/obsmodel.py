#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 FarmOS, Inc.
# All right reserved.
#
# Model Estimating Sensor Observation

import json
import time
import traceback

from sklearn.linear_model import LinearRegression
from collections import deque
from .dbmng import FDBManager

""" 
이 모델에서 x 는 1차원 데이터로 시간이고,
y는 역시 1차원 데이터로 관측치에 해당한다.
"""
class DataEstimationModel:
    def __init__(self, dataid, farmid=None):
        self._dataid = dataid
        self._farmid = farmid

    # 모델 설정을 위한 초기 데이터를 공급받기 위해서 데이터메니저가 필요하다.
    def setdatamanager(self, dm):
        pass

    #  모델 개선을 위해 값 변경시마다 지속적으로 호출되어야 한다.
    def setvalue(self, x, y):
        pass

    # 모델로부터 추정관측치를 가져온다.
    def getvalue(self, x):
        return None

class YesterdayPropModel(DataEstimationModel):
    # x 는 epoch time 이어야 한다.
    def __init__(self, dataid, farmid=None):
        self._dm = None
        self._buffer = []
        self._ratio = None
        self._last = None
        super(YesterdayPropModel, self).__init__(dataid, farmid)

    def setdatamanager(self, dm):
        self._dm = dm

    def setvalue(self, x, y):
        self._last = [x, y]

    def isvalid(self, x):
        if self._buffer[0][0] < x <= self._buffer[-1][0]:
            return True
        else:
            return False

    def rebalance(self):
        while True:
            item = self._buffer.popleft()
            if item[0] >= x:
                  break

    def getvalue(self, x):
        if len(self._buffer) == 0 or self.isvaild(x) is False:
            # 어제 같은시간 1분전부터 어제 같은 시간 30분 후까지
            self._buffer = deque(self._dm.getdataduring(self._dataid, x - 86460, x - 84600, self._farmid)) 
            print ("getduring", self._buffer)
            if self._buffer is None:
                return None
            item = self._buffer.popleft()
            if x - self._last[0] < 180:
                self._ratio = self._last[1] / item[1]
            assert self._ratio is not None, "YesterdayPropModel 에서 비율 세팅을 할 수 없음." + str(x) + " " + str(self._last) + " " + str(item)
        else:
            self.rebalance()

        item = self._buffer.popleft()
        print ("YesterdayPropModel", x, item, item[1] * self._ratio)
        return item[1] * self._ratio

class LinearRegModel(DataEstimationModel):
    def __init__(self, dataid, farmid=None):
        self._model = None
        self._xlist = []
        self._ylist = []
        super(LinearRegModel, self).__init__(dataid, farmid)

    def _fit(self):
        self._xlist = self._xlist[-5:]
        self._ylist = self._ylist[-5:]
        model = LinearRegression()
        self._model = model.fit(self._xlist, self._ylist)
        print("model fit : ", self._xlist, self._ylist, str(self._model))

    def setdatamanager(self, dm):
        cur = time.time()
        tmp = dm.getdataduring(self._dataid, cur - 600, cur, self._farmid)
        data = list(map(list, zip(*tmp)))
        if len(data) > 0:
            xlist = list(map(lambda x : [x], data[0]))
            self._xlist = []
            self._ylist = []
            for idx in range(len(data[1])):
                if data[1][idx] != None:
                    self._ylist.append(data[1][idx])
                    self._xlist.append(xlist[idx])
        else:
            self._xlist = []
            self._ylist = []
        self._model = None
        print("setdatamanager", self._xlist, self._ylist)

    def setvalue(self, x, y):
        self._xlist.append(x)
        self._ylist.append(y)
        self._model = None

    def getvalue(self, x):
        try:
            if self._model is None: 
                if len(self._xlist) > 0:
                    self._fit()
                else:
                    return None
            v = self._model.predict([[x]])
            return float(v[0])
        except Exception as ex:
            print ("getvalue exception", self._dataid, str(ex))
            print (traceback.format_exc())
            return None

class DefaultModel(DataEstimationModel):
    def __init__(self, dataid, farmid):
        self._shortmodel = LinearRegModel(dataid)
        self._longmodel = YesterdayPropModel(dataid)
        self._dm = None
        super(DefaultModel, self).__init__(dataid, farmid)

    def setdatamanager(self, dm):
        self._dm = dm
        self._longmodel.setdatamanager(dm)
        self._shortmodel.setdatamanager(dm)
        self._last = time.time()

    def setvalue(self, x, y):
        self._shortmodel.setvalue(x, y)
        self._longmodel.setvalue(x, y)
        self._last = time.time()

    def getvalue(self, x):
        if time.time() > self._last + 600: # 10분을 초과하면 Long Term
            return self._longmodel.getvalue(x)
        else:
            return self._shortmodel.getvalue(x)

if __name__ == "__main__":
    import random
    import logging

    option = {"db" : {"host" : "localhost", "user" : "farmos", "password" : "farmosv2@", "db" : "namwon1"}}
    dm = FDBManager(option, logging)
    dm.connect()

    (x, y) = dm.getcurrentdata(10000901)
    print ("current", x, y)

    model = LinearRegModel(10000901)
    print (model.getvalue(time.time()))
    model.setdatamanager(dm)
    print (time.time(), model.getvalue(time.time()))

    ymodel = YesterdayPropModel(10000901)
    ymodel.setdatamanager(dm) 
    ymodel.setvalue(x, y)
    print (time.time(), ymodel.getvalue(time.time()))

    ymodel = DefaultModel(10000901)
    ymodel.setdatamanager(dm) 
    ymodel.setvalue(x, y)
    print (time.time(), ymodel.getvalue(time.time()))


