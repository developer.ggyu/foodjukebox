#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 FarmOS, Inc.
# All right reserved.
#

import json
import sys
import time
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from collections import deque
import requests
from .. import *

from .farmosdb import FarmosDB

'''
option : {
    "conn" : {"host" : "dev.jinong.co.kr", "port" : 1883, "keepalive" : 60},
    "mqtt" : {"svc" : "cvtgate", "id" : "1"},
    "area" : "local"
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

class KDAPIMate(SSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(KDAPIMate, self).__init__(option, devinfo, coupleid, quelist)
        self._jnmqtt = JNMQTT(option, DevInfo(devinfo), self._logger)
        self._farmosdb = FarmosDB(option, DevInfo(devinfo), self._logger)
        self._reqs = deque()

    def connect(self):
        self._jnmqtt.connect(True)
        self._farmosdb.connect()
        super(KDAPIMate, self).connect()
        return True

    def close(self):
        self._jnmqtt.close()
        self._farmosdb.close()
        super(KDAPIMate, self).close()

    def matestart(self):
        self._jnmqtt.start(self.onmsg)
        self._farmosdb.start()
        super(KDAPIMate, self).matestart()
        self.connect()

    def matestop(self):
        self.close()
        self._jnmqtt.stop()
        self._farmosdb.stop()
        super(KDAPIMate, self).matestop()

    def processresponse(self, blk):
        self._jnmqtt.writeblk(blk)
        self._farmosdb.writeblk(blk)

    def processobsnoti(self, blk):
        self._jnmqtt.writeblk(blk)
        self._farmosdb.writeblk(blk)

    def onmsg(self, client, obj, blk):
        """ MQTT 로 받는 메세지는 Request """
        print(("KDAPIMate Received mblock '" + str(blk.payload) + "' on topic '"
              + blk.topic + "' with QoS " + str(blk.qos)))

        msg = self._jnmqtt.getpropermsg(blk)
        if msg is None:
            self._logger.warn("The message is not proper " + str(blk))
            return None

        self._reqs.append((blk.topic, msg))
        
    def loaddata(self):
        api = "http://52.78.109.164/farmcube/sensordataforward/fixed_geumsan_be3xxx.php"
        response = requests.get(api)
        if response.status_code == 200:
            data = json.loads(response.text)
        else:
            data = None
        return data

    def check(self):
        data = self.loaddata()
        f = open('/root/api.json','w')
        json.dump(data,f)

    def doextra(self):
        if self._timetocheck(Mate.RELOADTYPE):
            self.check()
            self._updatetime(Mate.RELOADTYPE)

        while True:
            try:
                topic, msg = self._reqs.popleft()
            except:
                return 

            tmp = topic.split('/')

            if _JNMQTT._SVR == tmp[2] and _JNMQTT._STAT == tmp[3]:
                pass

            if BlkType.isrequest(msg.gettype()):
                self._farmosdb.writeblk(msg)
                self._writereq(msg)
            else:
                self._logger.warn("Wrong message : " + msg.stringify())

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "conn" : {"host" : "localhost", "port" : 1883, "keepalive" : 60},
        "db" : {"host": "localhost", "user": "farmos", "password": "farmosv2@", "db": "farmos"},
        "mqtt" : {"svc" : "cvtgate", "id" : "d99bc0eb-6368-401e-bc38-922db31a8d27"},
        "area" : "local"
    }

    devinfo = [
        {"id" : "0", "dk" : "0", "dt": "gw", "children" : [
            {"id" : "1", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "11", "dk" : "11", "dt": "sen"},
                {"id" : "12", "dk" : "12", "dt": "act"}
            ]}
        ]}
    ]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = KDAPIMate(option, devinfo, "1", quelist)
    dsmate = DSMate(option, devinfo, "1", quelist)
    ssmate.start()
    dsmate.start()

    cmd = Request(1, None)
    cmd.setcommand(12, 'on', {})

    publish.single("cvtgate/1/req/1", cmd.stringify(), hostname="dev.jinong.co.kr")
    print("published")

    time.sleep(5)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    dsmate.stop()
    ssmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

    print("local tested.")
