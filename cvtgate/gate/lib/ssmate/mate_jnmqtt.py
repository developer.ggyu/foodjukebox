#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 FarmOS, Inc.
# All right reserved.
#

import json
import sys
import time
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

from .. import *

'''
option : {
    "conn" : {"host" : "dev.jinong.co.kr", "port" : 1883, "keepalive" : 60},
    "mqtt" : {"svc" : "cvtgate", "id" : "1"},
    "area" : "local"
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

class JNMQTTMate(SSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(JNMQTTMate, self).__init__(option, devinfo, coupleid, quelist)
        self._jnmqtt = JNMQTT(option, DevInfo(devinfo), self._logger)

    def connect(self):
        self._jnmqtt.connect(True)
        super(JNMQTTMate, self).connect()
        return True

    def close(self):
        self._jnmqtt.close()
        super(JNMQTTMate, self).close()

    def matestart(self):
        self._jnmqtt.start(self.onmsg)
        super(JNMQTTMate, self).matestart()
        self.connect()

    def matestop(self):
        self.close()
        self._jnmqtt.stop()
        super(JNMQTTMate, self).matestop()

    def processresponse(self, blk):
        self._jnmqtt.writeblk(blk)

    def processobsnoti(self, blk):
        self._jnmqtt.writeblk(blk)

    def onmsg(self, client, obj, blk):
        print(("JNMQTTMate Received mblock '" + str(blk.payload) + "' on topic '"
              + blk.topic + "' with QoS " + str(blk.qos)))

        msg = self._jnmqtt.getpropermsg(blk)
        if msg is None:
            self._logger.warn("The message is not proper " + str(blk))
            return None

#        try:
        tmp = blk.topic.split('/')

        if _JNMQTT._SVR == tmp[2] and _JNMQTT._STAT == tmp[3]:
            if blk.payload == _STAT_ON:
                self._logger.info("The server is OK now.")
            else:
                self._logger.warn("The server might have some problems.")

        if BlkType.isrequest(msg.gettype()):
            self._logger.info ("Request : " + msg.stringify())
            self._writereq(msg)
        else:
            self._logger.warn("Wrong message : " + blk.payload)
#        except Exception as ex:
#            self._logger.warn("fail to call onmsg : " + str(ex) + " "  + blk.payload)

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "conn" : {"host" : "kist.jinong.co.kr", "port" : 1883, "keepalive" : 60},
        "mqtt" : {"svc" : "cvtgate", "id" : "34db8790-c0b2-419c-9670-89348f50ba18"},
        "area" : "local"
    }

    devinfo = [
        {"id" : "0", "dk" : "0", "dt": "gw", "children" : [
            {"id" : "1", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "11", "dk" : "11", "dt": "sen"},
                {"id" : "12", "dk" : "12", "dt": "act"}
            ]}
        ]}
    ]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = JNMQTTMate(option, devinfo, "1", quelist)
    dsmate = DSMate(option, devinfo, "1", quelist)
    ssmate.start()
    dsmate.start()

    cmd = Request(1, None)
    cmd.setcommand(12, 'on', {})

    publish.single("cvtgate/1/req/1", cmd.stringify(), hostname="dev.jinong.co.kr")
    print("published")

    time.sleep(5)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    ssmate.stop()
    dsmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

    print("local tested.")
