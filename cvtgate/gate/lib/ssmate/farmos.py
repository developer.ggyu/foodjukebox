#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 FarmOS, Inc.
# All right reserved.
#
# utility for farmos 

class FarmOS:
    _CODESET = {
        "status" : 0, 
        "value" : 1,
        "position" : 2, 
        "state-hold-time" : 3,
        "remain-time": 4,
        "ratio" : 5,
        "control" : 6,
        "area" : 7,
        "alert" : 8,
        "rawvalue" : 9,
        "image" : 10,
        "availability" : 11,
        "estimated" : 12,
        "nst" : 13,         # next supply time
        "stp" : 14,         # short term predicted
        "irrigation-count" : 15,
        "vcnt" : 21,
        "vavg" : 22,
        "vmax" : 23,
        "vmin" : 24,
        "vstd" : 25,
        "diffstd" : 26,
        "davg" : 27,
        "navg" : 28
    }
    _SYSCODESET = {
        "loads_1m" : 51, "loads_5m": 52, "loads_15m" : 53,
        "vmem" : 54, "swap": 55, "disk" : 56,
        "ioread" : 57, "iowrite" : 58, "netsent" : 59, "netrecv" : 60
    }

    def getdataid(devid, code):
        if code in FarmOS._CODESET:
            return 10000000 + int(devid) * 100 + int(FarmOS._CODESET[code])

    def getsysdataid(hwid, code):
        if code in FarmOS._SYSCODESET:
            return 70000000 + int(hwid) * 100 + int(FarmOS._SYSCODESET[code])

if __name__ == "__main__":
    print ("sensor", 2, "observation dataid", FarmOS.getdataid(2, "value"))

