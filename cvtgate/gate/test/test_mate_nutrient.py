from lib.mblock import CmdCode, Request
from lib.dsmate.mate_nutrient import NutrientMate
from multiprocessing import Queue

import logging
FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)

def operate_numate():
    devinfo = [{
    "id": "1", "dk": "t127.0.0.1:2882", "dt": "gw", "children": [{
        "id": "101", 
        "dk": [1,1,["status"]], 
        "dt": "nd", 
        "children": [
            {"id": "102", 
             "dk": [1,210,["control","status","area","alert","opid"]],
             "dt": "nutrient-supply/level1"},
            {"id": "103",
             "dk": [1,204,["value","status"]], 
             "dt": "sen"},
            {"id": "104",
             "dk": [1,207,["value","status"]], 
             "dt": "sen"}
        ]}
    ], "method": "tcp"}]
    
    opt ={
        'conn' : [{
            "name" : 'ACM0',
            "method": 'tcp',
            "host" : '127.0.0.1',
            "port" : 2882,
            "timeout": 5
        }]
    }
    
    log_queue = Queue()
    request_queue = Queue()
    response_queue = Queue()
    observation_queue = Queue()
    quelist = [log_queue, request_queue, response_queue, observation_queue]
    numate = NutrientMate(opt, devinfo, coupleid="1", quelist=quelist)
    numate.connect()
    return numate
    
def test_process_nutrient_mate():
    numate = operate_numate()
    numate.process()

def test_detect_device_request():
    numate = operate_numate()
    req = Request("101")
    req.setcommand(103,CmdCode.DETECT_DEVICE,{})
    numate.processrequest(req)
    
def test_cancel_detect():
    numate = operate_numate()
    req = Request("101")
    req.setcommand(103,CmdCode.CANCEL_DETECT,{})
    numate.processrequest(req)

def test_once_watering():
    numate = operate_numate()
    req = Request("101")
    req.setcommand(103,CmdCode.ONCE_WATERING,{})
    numate.processrequest(req)
    
def test_area_watering():
    numate = operate_numate()
    req = Request("101")
    req.setcommand(103,CmdCode.AREA_WATERING,{})
    numate.processrequest(req)
    
def test_area_watering():
    numate = operate_numate()
    req = Request("101")
    req.setcommand(103,CmdCode.PARAMED_WATERING,{})
    numate.processrequest(req)

if __name__ == "__main__":
    operate_numate()