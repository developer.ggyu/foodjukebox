#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#

import sys
import time
import json
import pymysql
import traceback
import argparse
from lib.mblock import Notice, NotiCode
from datetime import date, timedelta, datetime

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

from lib.util import SunTime

import statsutil

class DayNightStats:
    def __init__(self, fname):
        self._util = statsutil.StatsUtil(fname)
        self._cur = self._util.getcursor()

    def execute(self, datestr, dataids=None, isnight=None):
        if datestr is None:
            datestr = date.today().isoformat()
            isupdate = True
        else:
            isupdate = False

        if dataids is None:
            dataids = self._util.loaddataid()

        print (dataids)
        stats = self.getstats(datestr, dataids, isnight)
        if stats is None:
            # Send some warning
            print ("Fail to get stats.")
            return

        print("stats", stats)
        self._util.writestats(stats, isupdate)

    def getcoordinate(self, fldid):
        qry = "select data_id, nvalue from current_observations where data_id in %s"
        try:
            print(qry, [fldid * 100000 + 1, fldid * 100000 + 2])
            self._cur.execute(qry, [(fldid * 100000 + 1, fldid * 100000 + 2)])
            longitude = None
            latitude = None

            for row in self._cur.fetchall():
                if row['data_id'] == 1:
                    latitude = row['nvalue']
                elif row['data_id'] == 2:
                    longitude = row['nvalue']
            return [longitude, latitude]

        except Exception as ex:
            print ("Fail to get coordinate: " + str(ex))
            print (traceback.format_exc())
            return [None, None]

    def gettimes(self, datestr, isnight):
        longitude, latitude = self.getcoordinate(0)    # 우선 농장의 위치기반으로 계산한다.

        if isnight:
            st = SunTime(longitude, latitude, datestr)
            ttime = datestr + " "  + time.strftime('%H:%M:%S', time.gmtime(st.getsunrise()))

            fdate = (datetime.strptime(datestr, "%Y-%m-%d") - timedelta(days=1)).isoformat()
            print ("yesterday", fdate)
            st = SunTime(longitude, latitude, fdate[:10])
            ftime = fdate[:10] + " "  + time.strftime('%H:%M:%S', time.gmtime(st.getsunset()))
        else:
            st = SunTime(longitude, latitude, datestr)
            ftime = datestr + " "  + time.strftime('%H:%M:%S', time.gmtime(st.getsunrise()))
            ttime = datestr + " "  + time.strftime('%H:%M:%S', time.gmtime(st.getsunset()))

        print (ftime, ttime)
        return [ftime, ttime]
           
    def getstats(self, datestr, dataids, isnight):
        qry = "SELECT data_id, count(*) vc, avg(nvalue) va from observations "\
              "WHERE data_id in %s and obs_time between %s and %s group by data_id"

        try:
            ftime, ttime = self.gettimes(datestr, isnight)
            print (qry, [dataids, ftime, ttime])
            self._cur.execute(qry, [dataids, ftime, ttime])
            stats = [] 
            for row in self._cur.fetchall():
                if isnight:
                    stats.append ([row['data_id'], datestr, row['va'], 28])
                else:
                    stats.append ([row['data_id'], datestr, row['va'], 27])
            return stats
        except Exception as ex:
            print ("Fail to make data statistics : " + str(ex))
            print (traceback.format_exc())
            return None

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='make statistics for sensor observations on day and night.')
    parser.add_argument('datestr', nargs='?', default=None, help='date to make statistics YYYY-MM-DD. if night, the period is from sunset time of yesterday to sunrise time of today')
    parser.add_argument('--night', action='store_true', help='statistics of night of specific date')
    parser.add_argument('--ids', metavar='DATA_ID', type=int, nargs='*', help='dataids (default: None)')
    parser.set_defaults(night=False)

    args = parser.parse_args()
    print (args)
    dstats = DayNightStats("conf/util.conf")
    dstats.execute(args.datestr, args.ids, args.night)
