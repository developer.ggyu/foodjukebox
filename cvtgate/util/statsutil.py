#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#

import sys
import time
import json
import pymysql
import traceback
import argparse
from lib.mblock import Notice, NotiCode
from datetime import date, timedelta

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

class StatsUtil:
    def __init__(self, fname):
        self._auth = None
        with open(fname, "r") as fp:
            self._option = json.loads(fp.read())

        copt = self._option["db"]
        self._conn = pymysql.connect(host=copt["host"], user=copt["user"],
                         password=copt["password"], db=copt["db"], 
                         cursorclass=pymysql.cursors.DictCursor, charset='utf8')
        self._cur = self._conn.cursor()

    def getcursor(self):
        return self._cur

    def loaddataid(self):
        qry = "SELECT 10000000 + id * 100 + 1 as data_id, spec from devices "\
              "where spec like '%\"sensor\"%' and deleted = 0" 
        try:
            self._cur.execute(qry)
            dataids = []
            for row in self._cur.fetchall():
                spec = json.loads(row['spec'])
                if spec["Class"] == "sensor":
                    dataids.append(row['data_id'])
            return dataids
        except Exception as ex:
            print ("Fail to get dataids: " + str(ex))
            print (traceback.format_exc())
            return None

    def makedataindex(self, dataid, code):
        names = {
            21: "일간 개수", 22: "일간 평균", 23: "일간 최고", 24: "일간 최저", 
            25: "일간 표준편차", 26: "일간 변동편차", 27: "주간 평균", 28: "야간 평균"
        }
        qry1 = "INSERT INTO dataindexes(id, name, unit, sigdigit, device_id, field_id) select %s, %s, unit, sigdigit, device_id, field_id from dataindexes where id = %s"
        qry2 = "INSERT INTO current_observations(data_id, obs_time, nvalue) values (%s, now(), NULL)"
        newid = dataid + code - 1
        print ("Try to make new dataindex", newid)
        try:
            print(qry1, [newid, names[code], dataid])
            self._cur.execute(qry1, [newid, names[code], dataid])
            self._cur.execute(qry2, [newid])
            self._conn.commit()
        except Exception as ex:
            print ("Fail to insert dataindex : " + str(ex))
            print (traceback.format_exc())
            return None

    def writestats(self, stats, isupdate=False):
        notitime = stats[0][1]
        data = {}
        for param in stats:
            newid = param[0] + param[3] - 1
            data[newid] = param[2]
            if self.writeone(newid, param, isupdate, False) is False:
                self.makedataindex(param[0], param[3])
                if self.writeone(newid, param, isupdate, True) is False:
                    print ("Fail to write stats", param)
        self.sendnotice(data, notitime)

    def writeone(self, dataid, param, isupdate=False, log=False):
        iqry = "REPLACE INTO observations(obs_time, nvalue, data_id, source_id) VALUES (%s, %s, %s, NULL)"
        uqry = "UPDATE current_observations set obs_time=%s, nvalue=%s, modified_time=now() WHERE data_id = %s"
        try:
            print(iqry, [param[1], param[2], dataid])
            self._cur.execute(iqry, [param[1], param[2], dataid])
            if isupdate:
                print(uqry, [param[1], param[2], dataid])
                self._cur.execute(uqry, [param[1], param[2], dataid])
            self._conn.commit()
            return True
        except Exception as ex:
            if log:
                print ("Fail to insert data statistics : " + str(ex))
                print (traceback.format_exc())
            return False

    def loadserial(self, farmid = None):
        query = "select uuid from gate_info"
        self._cur.execute(query, [])
        for row in self._cur.fetchall():
            return row['uuid']

    def sendnotice(self, data, notitime):
        # 데이터가 없거나 디피라면 노티스를 전송하지 않는다.
        # 추후 DP에서 경보이외의 값을 생성한다면 수정되어야 한다.
        serial = self.loadserial()

        if len(data) == 0 or serial is None:
            print ("Fail to send notice", data, serial)
            return

        topic = "/".join(["cvtgate", serial, '', "noti", ''])
        noti = Notice(None, NotiCode.STATS_DATA)
        noti.settime(notitime)
        
        for key, value in data.items():
            noti.setkeyvalue(key, value)
        ret = publish.single(topic, payload=noti.stringify(), qos=2, hostname=self._option["mqtt"]["host"])
        print("Sent a notice to " + topic + " : " + noti.stringify())

if __name__ == '__main__':
    pass
