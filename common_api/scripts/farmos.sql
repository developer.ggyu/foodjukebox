/*
 Navicat Premium Data Transfer

 Source Server         : 오픈소스 서버
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : 192.168.1.151:3306
 Source Schema         : farmos

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 17/01/2020 19:10:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

--
-- Database: `farmos`
--
DROP DATABASE IF EXISTS farmos;
CREATE DATABASE IF NOT EXISTS `farmos` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `farmos`;

-- ----------------------------
-- Table structure for configuration
-- ----------------------------
DROP TABLE IF EXISTS `configuration`;
CREATE TABLE `configuration` (
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `configuration` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for core_rule_applied
-- ----------------------------
DROP TABLE IF EXISTS `core_rule_applied`;
CREATE TABLE `core_rule_applied` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `field_id` int(11) DEFAULT NULL,
  `used` int(1) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `constraints` text,
  `configurations` text,
  `inputs` text,
  `controllers` text,
  `outputs` text,
  `autoapplying` int(1) NOT NULL DEFAULT '0',
  `groupname` varchar(255) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_core_rule_applied_fields_1` (`field_id`),
  CONSTRAINT `fk_core_rule_applied_fields_1` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for core_rule_template
-- ----------------------------
DROP TABLE IF EXISTS `core_rule_template`;
CREATE TABLE `core_rule_template` (
  `idx` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `autoapplying` int(1) DEFAULT NULL,
  `constraints` text,
  `configurations` text,
  `controllers` text,
  `outputs` text,
  `groupname` varchar(255) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idx`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for core_timespan
-- ----------------------------
DROP TABLE IF EXISTS `core_timespan`;
CREATE TABLE `core_timespan` (
  `id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `timespan` text,
  `name` varchar(50) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`id`,`field_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for current_observations
-- ----------------------------
DROP TABLE IF EXISTS `current_observations`;
CREATE TABLE `current_observations` (
  `data_id` int(11) NOT NULL,
  `obs_time` datetime NOT NULL,
  `nvalue` double DEFAULT NULL,
  `modified_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`data_id`) USING BTREE,
  CONSTRAINT `fk_current_observations_dataindexes` FOREIGN KEY (`data_id`) REFERENCES `dataindexes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of current_observations
-- ----------------------------
BEGIN;
INSERT INTO `current_observations` VALUES (1, '2019-08-22 16:39:11', 37.7091842052891, '2019-09-06 17:49:02');
INSERT INTO `current_observations` VALUES (2, '2019-08-22 16:39:11', 126.448555072944, '2019-08-22 16:39:11');
COMMIT;

-- ----------------------------
-- Table structure for dataindexes
-- ----------------------------
DROP TABLE IF EXISTS `dataindexes`;
CREATE TABLE `dataindexes` (
  `id` int(11) NOT NULL,
  `rule_id` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `unit` varchar(10) DEFAULT NULL,
  `sigdigit` int(11) DEFAULT '0',
  `device_id` int(11) DEFAULT NULL,
  `field_id` int(11) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dataindexes
-- ----------------------------
BEGIN;
INSERT INTO `dataindexes` VALUES (1, NULL, '위도', '°', 0, NULL, 0, 0);
INSERT INTO `dataindexes` VALUES (2, NULL, '경도', '°', 0, NULL, 0, 0);
COMMIT;

-- ----------------------------
-- Table structure for device_field
-- ----------------------------
DROP TABLE IF EXISTS `device_field`;
CREATE TABLE `device_field` (
  `device_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `sort_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`device_id`,`field_id`),
  KEY `FK_device_field_field_id_fields_id` (`field_id`),
  CONSTRAINT `FK_device_field_device_id_devices_id` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  CONSTRAINT `FK_device_field_field_id_fields_id` FOREIGN KEY (`field_id`) REFERENCES `fields` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for devices
-- ----------------------------
DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `spec` text NOT NULL,
  `gateid` varchar(255) NOT NULL,
  `coupleid` varchar(255) NOT NULL,
  `nodeid` int(11) NOT NULL,
  `compcode` int(11) NOT NULL,
  `devcode` int(11) DEFAULT NULL,
  `devindex` int(11) DEFAULT NULL,
  `deleted` int(1) DEFAULT '0',
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for farm
-- ----------------------------
DROP TABLE IF EXISTS `farm`;
CREATE TABLE `farm` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `info` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farm
-- ----------------------------
BEGIN;
INSERT INTO `farm` VALUES (1, '농장', '{\"telephone\":\"01011111111\",\"gps\":\"37.397962970070104,126.93206214966011\",\"address\":\"경기 안양시 동안구 관악대로 69\",\"postcode\":\"13956\"}');
COMMIT;

-- ----------------------------
-- Table structure for farmos_user
-- ----------------------------
DROP TABLE IF EXISTS `farmos_user`;
CREATE TABLE `farmos_user` (
  `userid` varchar(50) NOT NULL,
  `passwd` varchar(50) NOT NULL,
  `privilege` varchar(50) NOT NULL,
  `basicinfo` text,
  `appinfo` text,
  `loginip` varchar(50) DEFAULT NULL,
  `lastupdated` datetime NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of farmos_user
-- ----------------------------
BEGIN;
INSERT INTO `farmos_user` VALUES ('farmos', '*6CAEE806BFEDCC0A60717AD88280DC01B9A0CA66', 'user', '{\"name\" :\"관리자\"}', NULL, NULL, '2019-09-02 15:36:00');
COMMIT;

-- ----------------------------
-- Table structure for fields
-- ----------------------------
DROP TABLE IF EXISTS `fields`;
CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `farm_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `fieldtype` text NOT NULL,
  `uiinfo` text,
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_fields_farm_1` (`farm_id`),
  CONSTRAINT `fk_fields_farm_1` FOREIGN KEY (`farm_id`) REFERENCES `farm` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fields
-- ----------------------------
BEGIN;
INSERT INTO `fields` VALUES (0, 1, '온실외부', 'local', '{\"index\":{\"local\":{\"isFull\":true,\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":\"max\",\"device\":{},\"data\":[]},\"greenhouse\":{\"isFull\":true,\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":\"max\",\"device\":{},\"data\":[]},\"actuator\":{\"isFull\":true,\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":\"max\",\"device\":{},\"data\":[]}},\"dashboard\":{\"temp\":{\"idfmt\":{\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]1\",\"data\":\"\"},\"max\":2,\"device\":{},\"data\":[],\"isFull\":false},\"ventilation\":{\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":1,\"device\":{},\"data\":[],\"isFull\":false},\"heating\":{\"idfmt\":{\"device\":\"\",\"data\":\"\"},\"max\":1,\"device\":{},\"data\":[],\"isFull\":false},\"retractable\":{\"max\":5,\"idfmt\":{\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]2\",\"data\":\"\"},\"device\":{\"14\":[],\"18\":[]},\"data\":[],\"isFull\":false},\"switch\":{\"max\":5,\"idfmt\":{\"device\":\"1[0-9][0-9][0-9][0-9][0-9][0-9]4\",\"data\":\"\"},\"device\":{\"22\":[]},\"data\":[],\"isFull\":false}}}', 0);
COMMIT;

-- ----------------------------
-- Table structure for gate_info
-- ----------------------------
DROP TABLE IF EXISTS `gate_info`;
CREATE TABLE `gate_info` (
  `uuid` varchar(255) NOT NULL,
  `couple` varchar(255) NOT NULL,
  `detect` text,
  PRIMARY KEY (`uuid`,`couple`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gate_info
-- ----------------------------
BEGIN;
INSERT INTO `gate_info` VALUES ('c315cb82-0f6c-4ed6-b8cc-b00331789494', '4157859e-df55-48e5-b3ac-8e6288f2165e', '{}');
COMMIT;

-- ----------------------------
-- Table structure for observations
-- ----------------------------
DROP TABLE IF EXISTS `observations`;
CREATE TABLE `observations` (
  `data_id` int(11) NOT NULL,
  `obs_time` datetime NOT NULL,
  `nvalue` double DEFAULT NULL,
  PRIMARY KEY (`data_id`,`obs_time`),
  CONSTRAINT `fk_observations_dataindexes_1` FOREIGN KEY (`data_id`) REFERENCES `dataindexes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests` (
  `opid` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `command` int(11) NOT NULL,
  `params` text NOT NULL,
  `sentcnt` int(11) NOT NULL DEFAULT '1',
  `senttime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exectime` timestamp NULL DEFAULT NULL,
  `finishtime` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
