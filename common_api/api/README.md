# fcore API : FCORE REST API

## Introduction

fcore에 접근하기 위한 RESTful API이다. farmos에 대한 접근은 farmos.js를 사용하고, fcore에 대한 접근은 fcore.js 를 사용한다. farmos.js 는 별도의 [리포지토리](https://gitlab.com/jinong/farmos.js)를 가지고 있다.

## Maintainers

* 김준용 (joonyong.jinong@gmail.com) 

## 개발 환경
* nodejs
* swagger-node
 ```
 sudo npm install -g swagger
 ```
* forever
 ```
 sudo npm install -g forever forever-service
 ```

* farmos.js 
 ```
 git submodule init
 git submodule update --remote --merge
 ```
## API 문서수정
다음의 명령으로 API 문서 수정이 가능하다.
 ```
 swagger project edit
 ```

## API 실행
다음과 같은 명령으로 API 실행이 가능하다.
 ```
 swagger project start
 ```
API를 이용한 개발중 API 문서를 확인하고자 하는 경우에는 [http://localhost:5522/docs] 로 접근하여 확인이 가능하다.

실제 API 실행시에는 /usr/local/farmos/conf/에 farmos-server.ini 파일이 있어야 한다. 단, 당분간은 테스트로 contoller 폴더에 farmos-server.ini 파일을 사용하기 때문에 상관없다.

## 데몬 구동
 ```
 forever start app.js
 ```
