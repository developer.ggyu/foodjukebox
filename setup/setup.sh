#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
cd ../..
SHELL_PATH=`pwd -P`

cd cvtgate/fjbox

BeforeInstallDate=$(date)
echo $BeforeInstallDate


echo -e "\n monit stop \n"
sudo service monit stop

echo -e "\n fui stop \n"
sudo /etc/init.d/fui stop

echo -e "\n fcore stop \n"
sudo ${SHELL_PATH%}/scripts/fcore stop

echo -e "\n cvtgate stop \n"
sudo ${SHELL_PATH%}/scripts/cvtgate stop


echo -e '\n\n 1. Load Device Information \n'

ret=$(python3 fjbox-serial.py)
sleep 5s
ret=$(python3 fjbox-serial.py)
temp=($ret)
devcode=${temp[0]}
serial=${temp[1]}
hashkey=${temp[2]}
echo  -e $devcode, $serial, $hashkey

equipment=${temp[4]}

echo  -e 'equipment : ' $equipment

connServer=${temp[6]}

sshport=${temp[7]}
uiport=${temp[8]}
mqttport=${temp[9]}

echo -e 'SSH Connect Port : ' $sshport, ' UI Connect Port : ' $uiport, ' MQTT Connect Port : '$mqttport
echo -e 'Connect Server :' $connServer ;

userid=${temp[19]}
userpw=${temp[20]}

echo -e 'userid :' $userid , 'userpw :' $userpw


rm ${SHELL_PATH%}'/common_api/conf/config.json'
rm ${SHELL_PATH%}'/cvtgate/gate/conf/cpmng.conf'
sudo mv config.json /${SHELL_PATH%}'/common_api/conf/config.json'
sudo mv cpmng.conf /${SHELL_PATH%}'/cvtgate/gate/conf/cpmng.conf'

oldhost=$(cat /etc/hostname)
newhost="fjbox"$serial

if [[ "$oldhost" != "$newhost" ]];then
    sed -i "s/success/fail/g" status.txt
    sed -i "/FJDevice/d" status.txt
    sed -i "/Time/d" status.txt
fi


lineNum="$(grep -n "fail" status.txt | cut -d: -f1)"
echo $lineNum


fail_list=(`echo $lineNum | tr ',' ' '`)

if [ -z "$fail_list" ]
then
    sudo monit
    sudo monit reload
    sudo /etc/init.d/fui start
    exit
else
    echo "Setup Restart"
fi

if [ $? -eq 0 -a -n "$temp" ]; then
    echo "1. success"
    sed -i "2s/fail/success/" status.txt
else
    echo "1. fail"
    sed -i "2s/success/fail/" status.txt
fi


echo -e '\n\n 2. Set Up Autossh \n'

su - pi -c "
echo -e '@reboot autossh -N -f -R '$sshport':localhost:22 fjbox@'$connServer'\n@reboot autossh -N -f -R 0.0.0.0:'$uiport':localhost:8081 fjbox@'$connServer'\n@reboot autossh -N -f -R 0.0.0.0:'$mqttport':localhost:9001 fjbox@'$connServer | crontab
"
if [ $? -eq 0 ];then
    echo "2. success"
    sed -i "3s/fail/success/" status.txt
else
    echo "2. fail"
    sed -i "3s/success/fail/" status.txt
fi


echo -e '\n\n 3. Set Up mqtt\n'


cat << "EOF" > "/etc/mosquitto/mosquitto.conf"
# Place your local configuration in /etc/mosquitto/conf.d/
#
# A full description of the configuration file is at
# /usr/share/doc/mosquitto/examples/mosquitto.conf.example

pid_file /var/run/mosquitto.pid

persistence true
persistence_location /var/lib/mosquitto/

log_dest file /var/log/mosquitto/mosquitto.log

include_dir /etc/mosquitto/conf.d

port 1883
protocol mqtt

listener 9001
protocol websockets

connection external-bridg
EOF

echo "address $connServer:1883" >> "/etc/mosquitto/mosquitto.conf"

cat << "EOF" >> "/etc/mosquitto/mosquitto.conf"
cleansession true
topic # out 2
remote_username jinong
remote_password jinong1234%

EOF

echo "topic cvtgate/$equipment/# in 2" >> "/etc/mosquitto/mosquitto.conf"
#echo "topic cvtgate/$camera/# in 2" >> "/etc/mosquitto/mosquitto.conf"

if [ $? -eq 0 ];then
    echo "3. success"
    sed -i "4s/fail/success/" status.txt
else
    echo "3. fail"
    sed -i "4s/success/fail/" status.txt
fi


echo -e '\n\n 4. monit service\n'

sed -i '/fjbox/d' /etc/monit/monitrc
echo "set mailserver smtp.gmail.com port 587 username "'"fjbox.jinong@gmail.com"'" password "'"fjbox1234%"'" using tlsv12 with timeout 30 seconds" | sudo tee -a /etc/monit/monitrc
echo "set alert fjbox.jinong@gmail.com" | sudo tee -a /etc/monit/monitrc

if [ $? -eq 0 ];then
    echo "4. success"
    sed -i "5s/fail/success/" status.txt
else
    echo "4. fail"
    sed -i "5s/success/fail/" status.txt
fi



echo -e '\n\n 5. Set Up Databases \n'

mysql -u farmos -pfarmosv2@ farmos -e "UPDATE configuration SET configuration = '"$connServer":"$mqttport"' WHERE type='mqtt'";
mysql -u farmos -pfarmosv2@ farmos -e "UPDATE gate_info SET uuid = '"$hashkey"'";

mysql -u farmos -pfarmosv2@ farmos -e "UPDATE devices SET coupleid = '"$equipment"' where nodeid = '100001'";
mysql -u farmos -pfarmosv2@ farmos -e "UPDATE devices SET coupleid = '"$equipment"', devindex = 16, deleted = 0, nodeid = 100001 where id = 19";
mysql -u farmos -pfarmosv2@ farmos -e "update farmos_user set userid = '"$userid"' where id = 1";
mysql -u farmos -pfarmosv2@ farmos -e "update fields set name = '외부' where id = '0'";

if [ $? -eq 0 ];then
    echo "5. success"
    sed -i "6s/fail/success/" status.txt
else
    echo "5. fail"
    sed -i "6s/success/fail/" status.txt
fi


echo -e '\n\n 6. Change Conf \n'


cat << "EOF" > "menu_pc.json"
["data/reference","data/correlation","data/statistics","research","device","setting/process","setting/farm","control/auto/0","control/manual/nutrient-supply","setting/timespan","rule/template","add/template","modify/timespan"]
EOF

cat << "EOF" > "menu_m.json"
["device/state","setting/place","setting/farm"]
EOF

rm ${SHELL_PATH%}'/common_api/api/public/menu_m.json'
sudo mv menu_m.json ${SHELL_PATH%}'/common_api/api/public/menu_m.json'

rm ${SHELL_PATH%}'/common_api/api/public/menu_pc.json'
sudo mv menu_pc.json ${SHELL_PATH%}'/common_api/api/public/menu_pc.json'

if [ $? -eq 0 ];then
    echo "6. success"
    sed -i "7s/fail/success/" status.txt
else
    echo "6. fail"
    sed -i "7s/success/fail/" status.txt
fi


echo -e '\n\n 7. Change HostName \n'

echo "Existing hostname is $oldhost"
echo $newhost

#change hostname in /etc/hosts & /etc/hostname
sudo sed -i "s/$oldhost/$newhost/g" /etc/hosts
sudo sed -i "s/$oldhost/$newhost/g" /etc/hostname

#display new hostname
echo "Your new hostname is $newhost"

if [ $? -eq 0 ];then
    echo "7. success"
    sed -i "8s/fail/success/" status.txt
else
    echo "7. fail"
    sed -i "8s/success/fail/" status.txt
fi


echo -e '\n\n 8. Camera Check \n'

raspistill -o image.jpg

if [ $? -eq 0 ]; then
    echo "8. success"
    sed -i "9s/fail/success/" status.txt
else
    echo "8. fail"
    sed -i "9s/success/fail/" status.txt
fi

rm image.jpg

EndInstallDate=$(date)
echo $EndInstallDate


sed -i "/FJDevice/d" status.txt
sed -i "/Time/d" status.txt

echo "FJDevice Serial : $serial" >> "status.txt"
echo "FJDevice Hashkey : $hashkey" >> "status.txt"

echo "Start Time : $BeforeInstallDate" >> "status.txt"
echo "Complete Time : $EndInstallDate" >> "status.txt"

echo "FJDevice userid : '$userid' , FJDevice userpw : '$userpw'" >> "status.txt"

echo -e '\n\n 9. Send Email \n'

cat << "EOF" >> "sendmail.py"
#! /usr/bin/python
# -*- coding: utf-8 -*-

import smtplib
import datetime
import socket

status = open('status.txt')
data=status.read()

TO = 'fjbox@jinong.co.kr'
EOF

echo "SUBJECT = '$newhost'" >> "sendmail.py"

cat << "EOF" >> "sendmail.py"

gmail_sender = 'fjbox@jinong.co.kr'
gmail_passwd = 'KistKDJN#2020'

server = smtplib.SMTP_SSL('smtp.mailplug.co.kr', 465)
server.login(gmail_sender, gmail_passwd)

BODY = '\r\n'.join(['To: %s' % TO,
                        'From: %s' % gmail_sender,
                        'Subject: %s' % SUBJECT,
                        'result', data])

try:
    server.sendmail(gmail_sender, [TO], BODY)
    print ('email sent')

except:
    print ('error sending mail')

server.quit()

EOF

ret=$(python sendmail.py)

if [ $? -eq 0 ];then
    echo "email sent"
fi
rm sendmail.py


sudo monit
sudo monit reload
sudo /etc/init.d/fui start


sudo reboot
