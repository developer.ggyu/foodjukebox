#!/usr/bin/env python -*- coding: utf-8 -*-

import shutil
import numpy as np
import pandas as pd
import calendar
from datetime import datetime, timedelta
import time
import pymysql
import os

from fpdf import FPDF
import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib import font_manager, rc

rcParams['axes.spines.top'] = False
rcParams['axes.spines.right'] = False

class CustomPDF(FPDF):
    def header(self):
        # Set up a logo
#        self.image('snakehead.jpg', 10, 8, 33)
        self.add_font('NanumGothic', "", 'NanumGothic.ttf', uni=True)
        self.set_font('NanumGothic', '', 14)
#        self.set_font('Arial', 'B', 15)

        # Add an address
        self.cell(80)
        today = date=datetime.today().strftime('%y.%m.%d')
        title = '푸드 쥬크박스 주간보고서({date})'.format(date=today)
        self.cell(30, 10, title, 0, 0, 'C')
#        self.cell(0, 5, 'Mike Driscoll', ln=1)

        # Line break
        self.ln(20)

    def footer(self):
        self.set_y(-10)
        self.set_font('NanumGothic', '', 8)
        # Add a page number
        page = 'Page ' + str(self.page_no()) + '/{nb}'
        self.cell(0, 10, page, 0, 0, 'C')

def is_digit(str):
    try:
        tmp = float(str)
        return True
    except ValueError:
        return False

def makeTable(pdf, data, maxValue, minValue, spacing=1):
    col_width = pdf.w / (len(data[0])+0.5)
    row_height = pdf.font_size
    for row in data:
        for item in row:
            if is_digit(item) :
                if float(item) >= maxValue:
                    pdf.set_text_color(255,0,0)
                    pdf.cell(col_width, row_height*spacing,txt=item, border=1)
                    pdf.set_text_color(0,0,0)
                elif float(item) <= minValue:
                    pdf.set_text_color(0,0,255)
                    pdf.cell(col_width, row_height*spacing,txt=item, border=1)
                    pdf.set_text_color(0,0,0)
                else :
                    pdf.cell(col_width, row_height*spacing,txt=item, border=1)
            else:
                pdf.cell(col_width, row_height*spacing,txt=item, border=1)

        pdf.ln(row_height*spacing)


def weekDate():
    startday = (datetime.today() - timedelta(7)).strftime('%Y-%m-%d')
    endday = (datetime.today()).strftime('%Y-%m-%d')
    start = datetime.strptime(startday, '%Y-%m-%d')
    end = datetime.strptime(endday, '%Y-%m-%d')
    dates = [(start + timedelta(days=i)).strftime('%Y-%m-%d') for i in range((end-start).days+1)]
    del dates[-1]
    return dates

def parserSensor(conn,sql, id, dbnum):
    curs = conn.cursor()
    curs.execute(sql, id)
    testdata = curs.fetchall()
    sensorValue = []
    for i in range(len(testdata)):
        sensorValue.append(testdata[i][dbnum])
        if dbnum == 1 :
            sensorValue[i] = str(round(sensorValue[i],2))
    return sensorValue


def createBasicData(conn):
    def parseDB(sql):
        curs.execute(sql)
        data = curs.fetchall()
        return data[0][0]

    curs = conn.cursor()
    sql = "SELECT userid from farmos_user";
    parseDB(sql)
    fjboxSerial = parseDB(sql)

    sql = "select converted from units where nvalue = (select nvalue from current_observations where data_id = 100003) and unit = 'crop'";
    parseDB(sql)
    plantName = parseDB(sql)

    sql = "select nvalue from current_observations where data_id = 100004";
    parseDB(sql)
    temp = datetime.fromtimestamp(parseDB(sql))
    startDay = temp.strftime('%Y-%m-%d')

    now = datetime.now()
    plantDay = str((now - temp).days)

    basicData = [['장비 번호','작물 이름','정식일','재배일수'],[fjboxSerial, plantName, startDay, plantDay]]
    return basicData

def createSensorData(conn,id):
    curs = conn.cursor()
    sensordates = []
    dates = weekDate()
    for i in range(len(dates)):
        sensordates.append(dates[i][6:])
    sensordates.insert(0,'')

    sql = "select date_format(obs_time, '%%m-%%d'), avg(nvalue) from observations where data_id = %s and obs_time > date_add(curdate(),interval -7 day) and obs_time < curdate() group by  date_format(obs_time, '%%Y-%%m-%%d');"
    allDayAvg = parserSensor(conn,sql, id, 1)
    allDayAvg.insert(0, '24시간')

    sql = "select date_format(obs_time, '%%Y-%%m-%%d'), avg(nvalue) from observations where data_id = %s and obs_time > date_add(curdate(),interval -7 day) and obs_time < curdate() and date_format(obs_time, '%%H') between '08:00:00' and '18:00:00' group by  date_format(obs_time, '%%Y-%%m-%%d');"
    dayAvg = parserSensor(conn,sql, id, 1)
    dayAvg.insert(0, '주간')

    sql = "select date_format(obs_time, '%%Y-%%m-%%d'), avg(nvalue) from observations where data_id = %s and obs_time > date_add(curdate(),interval -7 day) and obs_time < curdate() and date_format(obs_time, '%%H') not between '08:00:00' and '18:00:00' group by  date_format(obs_time, '%%Y-%%m-%%d');"
    nightAvg = parserSensor(conn,sql, id, 1)
    nightAvg.insert(0, '야간')

    sensorData = [sensordates, allDayAvg, dayAvg, nightAvg]
    return sensorData

def createDif(dayValue, nightValue) :
    diff = []
    for i in range(len(dayValue)-1):
        diff.append(float(dayValue[i+1]) - float(nightValue[i+1]))
        diff[i] = str(round(diff[i],2))
    diff.insert(0, 'diff')
    return diff

def avgPerHour(conn, data_id):
    valuePerHour = []
    sql = "select date_format(obs_time, '%%y/%%m/%%d %%H'), avg(nvalue) from observations where data_id = %s and obs_time > date_add(curdate(),interval -7 day) and obs_time < curdate() group by  date_format(obs_time, '%%Y-%%m-%%d %%H');"
    dtdate = parserSensor(conn ,sql, data_id[0], 0)
    for k in range(len(dtdate)):
        dtdate[k] = datetime.strptime(dtdate[k],'%y/%m/%d %H')
    valuePerHour.append(dtdate)
    for i in range(len(data_id)):
        strdata = parserSensor(conn ,sql, data_id[i], 1)
        floatdata = []
        for j in range(len(strdata)):
            floatdata.append(float(strdata[j]))
        valuePerHour.append(floatdata)
    return valuePerHour

def makestandard(max, min, data):
    maxData = []
    minData = []
    for i in range(len(data)):
        maxData.append(max)
        minData.append(min)
    standardData = [maxData, minData]
    return standardData

def createSystemData(conn):
#    curs = conn.cursor()
    systemdates = []
    dates = weekDate()
    for i in range(len(dates)):
        systemdates.append(dates[i][6:])
    systemdates.insert(0,'')

    sql = "select date_format(obs_time, '%%m/%%d'), avg(nvalue) from observations where data_id = %s and obs_time > date_add(curdate(),interval -7 day) and obs_time < curdate() group by  date_format(obs_time, '%%Y-%%m-%%d');"
    cpuLoad = parserSensor(conn, sql, 53, 1)
    cpuLoad.insert(0, 'CPU 부하')

    sql = "select date_format(obs_time, '%%m/%%d'), avg(nvalue) from observations where data_id = %s and obs_time > date_add(curdate(),interval -7 day) and obs_time < curdate() group by  date_format(obs_time, '%%Y-%%m-%%d');"
    diskUsage = parserSensor(conn, sql, 54, 1)
    diskUsage.insert(0, '디스크 사용률')

    sql = "select date_format(obs_time, '%%m/%%d'), avg(nvalue) from observations where data_id = %s and obs_time > date_add(curdate(),interval -7 day) and obs_time < curdate() group by  date_format(obs_time, '%%Y-%%m-%%d');"
    memoryUsage = parserSensor(conn, sql, 56, 1)
    memoryUsage.insert(0, '메모리 사용률')

    systemData = [systemdates, cpuLoad, diskUsage, memoryUsage]
    return systemData

def plantData(conn):
    plantData=[]
    sql = "select date_format(obs_time,'%%Y-%%m-%%d'), nvalue from observations where data_id = %s group by  date_format(obs_time, '%%Y-%%m-%%d');"
    plantData.append(parserSensor(conn, sql, 30100092, 0))
    for j in range(30100092,30100097):
        plantData.append(parserSensor(conn, sql, j, 1))
    return plantData

def parseImage(startdayImg):
    imgpath = '/home/pi/farmosv2-script/backup/image/18/'
    starttemp = time.mktime(datetime.strptime(startdayImg, '%Y-%m-%d').timetuple())
    print (starttemp)
    startDayImg = str(starttemp)[:-2]+'000'
    curtime = str(int(time.time()))+'000'
    imglist = []
    myimages = [] #list of image filenames
    dirFiles = os.listdir(imgpath) #list of directory files
    dirFiles.sort() #good initial sort but doesnt sort numerically very well
    sorted(dirFiles) #sort numerically in ascending order
    for files in dirFiles: #filter out all non jpgs
        if '.png' in files:
            myimages.append(files)
    for i in range(len(myimages)):
        if myimages[i][:-4] >= startDayImg and myimages[i][:-4] <= curtime:
            imglist.append(myimages[i])
    startimg=copyimg(imgpath+imglist[0])
    curimg=copyimg(imgpath+imglist[-1])
    return (startimg, curimg)

def copyimg(imgpath):
    changeimgpath = (imgpath[:-3])+'jpeg'
    shutil.copy(imgpath,changeimgpath)
    return changeimgpath

def makeLightChart(conn):
    curs = conn.cursor()
    sql = "select date_format(obs_time, '%%y/%%m/%%d'), avg(nvalue) from observations where data_id = %s and obs_time > date_add(curdate(),interval -7 day) and obs_time < curdate() group by  date_format(obs_time, '%%Y-%%m-%%d');"
    perwhite = []
    perblue = []
    perred = []

    lightsum = parserSensor(conn,sql,30120051,1)
    lightwhite = parserSensor(conn,sql,30120052,1)
    lightblue = parserSensor(conn,sql,30120053,1)
    lightred = parserSensor(conn,sql,30120054,1)

    for i in range(len(lightsum)):
        perwhite.append(float(lightwhite[i])/float(lightsum[i]))
        perred.append(float(lightred[i])/float(lightsum[i]))
        perblue.append(float(lightblue[i])/float(lightsum[i]))

    lightdata = parserSensor(conn,sql,30120051,0)
    lightchart = [lightdata, perwhite, perred,perblue]
    return lightchart


def makeChart(datas, type, name, pdf, charttype):
    dates = datas[0]
    plt.rc('font', family='NanumGothic')
    plt.figure(figsize=(12, 4))
    plt.grid(color='#F2F2F2', alpha=1, zorder=0)
    for j in range(len(datas)-1):
        if not datas[j+1]:
            print("pass")
            pdf.cell(0, 10, txt="check devices",ln=1)
            return
        else:
            if charttype == "bar":
                plt.bar(dates, datas[j+1])
                for i, v in enumerate(dates):
                    # verticalalignment (top, center, bottom) # horizontalalignment (left, center, right)
                    plt.text(v, float(datas[j+1][i]), round(float(datas[j+1][i]),3), fontsize = 9, color='black', horizontalalignment='center',  verticalalignment='center')
            else :
                plt.plot(dates, datas[j+1])
#    plt.title(f'FoodJukeBox weekly {type}'.format(type=type), fontsize=17)
#    plt.xlabel('Dates', fontsize=13)
    plt.xticks(fontsize=9)
#    plt.ylabel(type, fontsize=13)
    plt.yticks(fontsize=9)
#    plt.legend(['Max'], loc = 0)
    plt.grid(True, color='grey', alpha=0.5, linestyle='--')
    plt.savefig(name, dpi=300, bbox_inches='tight', pad_inches=0)
    plt.close()
    return

def create_pdf(pdf_path):
    pdf = CustomPDF()
    conn = pymysql.connect(host='localhost', user='farmos', password='farmosv2@', db='farmos')
    # Create the special value {nb}
    pdf.alias_nb_pages()
    pdf.add_page()
    pdf.set_font('NanumGothic', '', 12)
    line_no = 1
    pdf.cell(0, 10, txt="1. 기초 정보",ln=1)
    basicData = createBasicData(conn)
    makeTable(pdf, basicData,99999999,0)

    pdf.cell(0, 10, txt="2. 환경정보",ln=1)
    pdf.set_font('NanumGothic', '', 10)

    pdf.cell(0, 10, txt="1) 온도",ln=1)
    dataTemp = createSensorData(conn, 10000201)
    tempDif = createDif(dataTemp[2], dataTemp[3])
    dataTemp.append(tempDif)
    makeTable(pdf, dataTemp, 40, -40)
    pdf.cell(0, 6, txt=" ",ln=1)

    tempChart = avgPerHour(conn, [10000201, 30010030, 30010031])
    makeChart(tempChart, '기준 온도 및 온도 관측치','temprature.png',pdf, 'line')
    pdf.image('temprature.png', w=pdf.w/1.1, h = pdf.h/4)

    pdf.cell(0, 10, txt="2) 습도",ln=1)
    dataHum = createSensorData(conn,10000301)
    makeTable(pdf, dataHum, 85, 40)
    pdf.cell(0, 6, txt=" ",ln=1)

    humChart=avgPerHour(conn, [10000301])
    humChart.extend(makestandard(85, 50, humChart[1]))
    makeChart(humChart, '기준 습도 및 습도 관측치','hum.png',pdf,'line')
    pdf.image('hum.png', w=pdf.w/1.1, h = pdf.h/4)

    pdf.cell(0, 10, txt="3) CO2",ln=1)
    dataCO2 = createSensorData(conn,10000401)
    makeTable(pdf, dataCO2, 2000, 300)

    pdf.set_font('NanumGothic', '', 12)
    pdf.cell(0, 10, txt="3. 제어 정보",ln=1)
    pdf.set_font('NanumGothic', '', 10)

    pdf.cell(0, 10, txt="EC",ln=1)
    dataEC = createSensorData(conn,10000501)
    makeTable(pdf, dataEC, 3 , 1)
    pdf.cell(0, 6, txt=" ",ln=1)

    makeChart(makeLightChart(conn), '광 비율(화이트, 레드, 블루)','Light.png',pdf,'bar')
    pdf.image('Light.png', w=pdf.w/1.1, h = pdf.h/4)
    pdf.set_font('NanumGothic', '', 12)
    pdf.cell(0, 10, txt="4. 생육 정보",ln=1)

    leafData=plantData(conn)
    makeChart(leafData, 'leaf area','LeafArea.png',pdf,'line')
    pdf.image('LeafArea.png', w=pdf.w/1.1, h = pdf.h/4)

    pdf.add_page()
    pdf.cell(0, 10, txt="startday",ln=1)
    pdf.image(parseImage(basicData[1][2])[0], w = pdf.w/1.5, h = pdf.w/2.1)
    pdf.cell(0, 10, txt="CurrentDay picture",ln=1)
    pdf.image(parseImage(basicData[1][2])[1], w = pdf.w/1.5, h = pdf.w/2.1)

    pdf.cell(0, 10, txt="5. 시스템 정보",ln=1)
    pdf.set_font('NanumGothic', '', 10)
    dataSystem = createSystemData(conn)
    makeTable(pdf, dataSystem, 80, 0)

    pdf.output(pdf_path,'F')


if __name__ == '__main__':
    create_pdf('header_footer.pdf')
