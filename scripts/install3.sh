#!/bin/bash

echo -e '\n\n'
echo -e '    ______                                   _    _____ 
   / ____/___ __________ ___  ____  _____   | |  / /__ \
  / /_  / __ `/ ___/ __ `__ \/ __ \/ ___/   | | / /__/ /
 / __/ / /_/ / /  / / / / / / /_/ (__  )    | |/ // __/ 
/_/    \__,_/_/  /_/ /_/ /_/\____/____/     |___//____/ 
                                                        '

dirpath=`dirname $0`
echo -e $dirpath 
cd $dirpath
SHELL_PATH=`pwd -P`

echo -e "Input install folder name."
cd ..
INSTALL_PATH=`pwd -P` ;

echo $INSTALL_PATH

echo -e '\n\n 0. check packages \n'
echo -e '\n\n apt update\n';sudo apt update
echo -e '\n\n apt upgrade\n';sudo apt upgrade -y

echo -e '\n\n 1. install from git \n'

rm -rf opensource_ui
git clone -b ui --single-branch https://gitlab.com/jinong/fjbox_opensource_ui_build.git opensource_ui
mv opensource_ui/ui common_api/api/public


echo -e '\n\n 2. apt install -y build-essential\n';sudo apt install -y build-essential
echo -e '\n\n 3. apt install curl\n'; sudo apt install -y curl rdate

echo -e '\n\n 4. apt install python-pip\n'; sudo apt install python-pip -y
echo -e '\n\n 4-1. pip install --upgrade pip\n'; pip install --upgrade pip
echo -e '\n\n 4-2. pip install --upgrade setuptools\n'; pip uninstall -y distribute; pip install --upgrade setuptools
echo -e '\n\n 4-3. pip install requests\n'; pip install requests
echo -e '\n\n 4-4. pip install pymysql\n'; pip install pymysql
echo -e '\n\n 4-5. pip install paho-mqtt\n'; pip install paho-mqtt
echo -e '\n\n 4-6. pip install simpleeval\n'; pip install simpleeval
echo -e '\n\n 4-7. pip install subprocess32\n'; pip install subprocess32
echo -e '\n\n 4-8. pip install future\n'; pip install future
echo -e '\n\n 4-9. apt-get install python-dev\n'; apt-get install python-dev
echo -e '\n\n 4-10. pip install psutil\n'; pip install psutil
echo -e '\n\n 4-11. pip install --upgrade pip enum34\n';pip install --upgrade pip enum34
echo -e '\n\n 4-12. apt-get install monit\n'; apt-get install monit
echo -e '\n\n 4-13. apt install mosquitto mosquitto-clients autossh openssh-server\n'; apt install mosquitto mosquitto-clients autossh openssh-server
echo -e '\n\n 4-14. apt install -y python-matplotlib python-opencv imagemagick\n'; apt install -y python-matplotlib python-opencv imagemagick
echo -e '\n\n 4-15. pip install tailer\n'; pip install tailer

echo -e '\n\n 5. apt install python-pip\n'; sudo apt install python3-pip -y
echo -e '\n\n 5-1. pip3 install requests\n'; pip3 install requests
echo -e '\n\n 5-2. pip3 install prometheus_client\n'; pip3 install prometheus_client
echo -e '\n\n 5-3. pip3 install pyserial\n'; pip3 install pyserial
echo -e '\n\n 5-4. pip3 install paho-mqtt\n'; pip3 install paho-mqtt 
echo -e '\n\n 5-5. pip3 install pymysql pymysql-pooling\n'; pip3 install pymysql pymysql-pooling
echo -e '\n\n 5-6. pip3 install pymodbus\n'; pip3 install pymodbus
echo -e '\n\n 5-7. pip3 install future subprocess32 simpleeval psutil\n'; pip3 install future subprocess32 simpleeval psutil

echo -e '\n\n 6. mysql check\n'
which mysql
if [ $? -eq 1 ];then
#echo -e "\n\n apt install mysql-server\n"; sudo apt install -y mysql-server
   echo -e "\n\n apt install mariadb-server\n"; sudo apt-get install -y mariadb-server 
   echo -e "\n\n systemctl start mysql\n"; sudo systemctl start mysql
   echo -e "\n\n systemctl enable mysql\n"; sudo systemctl enable mysql
else
    echo -e "mysql installed"
fi
echo -e "\nend"

echo -e '\n\n 7. node check\n'
which node
if [ $? -eq 1 ];then
    echo -e "apt purge node\n"; sudo apt purge node
fi


echo -e "curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -\n"; curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -;
echo -e "apt install -y nodejs\n"; sudo apt install -y nodejs
echo -e "npm install forever -g\n"; npm install forever -g
echo -e "\nend"

echo -e '\n\n 8. Mosquitto check\n'
which mosquitto
if [ $? -eq 1 ];then
   echo -e "\n\n apt install mosquitto\n"; sudo apt install -y mosquitto 
   echo -e "\nport 1883\nprotocol mqtt\n\nlistener 9001\nprotocol websockets" | sudo tee -a /etc/mosquitto/mosquitto.conf
   echo -e "\n\n sudo systemctl restart mosquitto\n"; sudo systemctl restart mosquitto
   sleep 2
   echo -e "\n\n sudo mosquitto -c /etc/mosquitto/mosquitto.conf -d\n"; sudo mosquitto -c /etc/mosquitto/mosquitto.conf -d
else
    echo -e "mosquitto installed"
fi
echo -e "\nend"

echo -e '\n\n 9. database script run \n'
cd ${SHELL_PATH%} 
sudo mysql -u root < farmos.sql
cd $INSTALL_PATH


echo -e '\n\n 10. npm install \n'

sudo npm install --prefix ./common_api --unsafe-perm

echo -e '\n\n 11. ui installation \n'
cat << "EOF" > "fui"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          farmos_ui
# Required-Start:    mysql
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable farmos UI service provided by daemon.
### END INIT INFO

EOF

echo "WORK_DIR=\"${INSTALL_PATH%}/common_api/api\"" >> fui

cat << "EOF" >> "fui"
cd "$WORK_DIR"

case "$1" in
  start)
    echo "Starting server"
    forever start --uid "farmosV2" -a "${WORK_DIR}/app.js"
    ;;
  stop)
    echo "Stopping server"
    forever stop farmosV2
    ;;
  *)
    echo "Usage: /etc/init.d/fui {start|stop}"
    exit 1
    ;;
esac
exit 0
EOF

sudo chmod +x fui
sudo mv fui /etc/init.d/fui


echo -e '\n\n 12. gate installation \n'
cat << "EOF" > "gate"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          farmos_gate
# Required-Start:    mysql
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable farmos gate service provided by daemon.
### END INIT INFO

EOF

echo "WORK_DIR=\"${INSTALL_PATH%}/cvtgate/gate\"" >> gate

cat << "EOF" >> "gate"
cd "$WORK_DIR"

case "$1" in
  start)
    echo -e "Starting server"
    python3 couplemng.py start
    ;;
  stop)
    echo -e "Stopping server"
    cd "$WORK_DIR"
    python3 couplemng.py stop
    ;;
  *)
    echo -e "Usage: /etc/init.d/cvtgate {start|stop}"
    exit 1
    ;;
esac
exit 0
EOF

sudo chmod +x gate
sudo mv gate ${SHELL_PATH%}/cvtgate

echo -e '\n\n 13. core installation \n'
cat << "EOF" > "core"
#!/bin/bash

### BEGIN INIT INFO
# Provides:          farmos_core
# Required-Start:    mysql
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable farmos core service provided by daemon.
### END INIT INFO

EOF

echo "WORK_DIR=\"${INSTALL_PATH%}/fcore\"" >> core

cat << "EOF" >> "core"
cd "$WORK_DIR"

case "$1" in
  start)
    echo "Starting server"
    python3 fcore.py start
    ;;
  stop)
    echo "Stopping server"
    python3 fcore.py stop
    ;;
  *)
    echo "Usage: /etc/init.d/fcore {start|stop}"
    exit 1
    ;;
esac
exit 0
EOF

sudo chmod +x core
sudo mv core ${SHELL_PATH%}/fcore


echo -e '\n\n 14. service registration\n'

sudo update-rc.d fui defaults
#sudo update-rc.d fcore defaults
#sudo update-rc.d cvtgate defaults

#sudo /etc/init.d/fui start
#sudo ${INSTALL_PATH%}/fcore start
#sudo ${INSTALL_PATH%}/cvtgate start

echo -e "\n\n farmos service is not started. check configuration and start them.\n"
echo -e "\n # sudo /etc/init.d/fui start\n # sudo ${INSTALL_PATH%}/fcore start \n # sudo ${INSTALL_PATH%}/cvtgate start\n"

echo -e '\n\n 15. monit service\n'


echo "set mailserver smtp.gmail.com port 587 username "'"issue.jinong@gmail.com"'" password "'"password"'" using tlsv12 with timeout 30 seconds" | sudo tee -a /etc/monit/monitrc    
echo "set alert service.jinong@gmail.com" | sudo tee -a /etc/monit/monitrc


cat << "EOF" > "monitcvtgate"
check process cpmng with pidfile /var/run/cpmng.pid
EOF
echo "    start = \"$SHELL_PATH/cvtgate start\"" >> "monitcvtgate"
echo "    stop = \"$SHELL_PATH/cvtgate stop\"" >> "monitcvtgate"

sudo mv monitcvtgate /etc/monit/conf.d/monitcvtgate


cat << "EOF" > "monitfcore"
check process fcore with pidfile /var/run/fcore.pid
EOF
echo "    start = \"$SHELL_PATH/fcore start\"" >> "monitfcore"
echo "    stop = \"$SHELL_PATH/fcore stop\"" >> "monitfcore"

sudo mv monitfcore /etc/monit/conf.d/monitfcore

#echo -e "\n\n sudo systemctl restart monit\n"; sudo systemctl restart monit
echo -e "\n\n You should change password for notice email and restart monit!!\n"
