#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 FarmOS, Inc.
# All right reserved.
#

from __future__ import print_function
import sys
import time
import importlib
import json
import requests
import pymysql
from datetime import datetime

from daemon import Daemon, Runner
import util
import rulemng

class FCore(Runner):
    def __init__(self, configfile, logger, mode, rid=-1):
        fp = open(configfile, 'r')
        self._option = json.loads(fp.read())
        fp.close()

        if mode == "sched":
            self._option["dname"] = self.getdname() + '_sched'

        self._logger = logger
        self._rules = rulemng.RuleManager(self._option, logger, mode, rid)
   
    def getdname(self):
        if "dname" in self._option:
            return self._option["dname"]
        return "fcore"

    def stop(self):
        self._logger.info("Try to stop")
        self._isrunning = False

    def run(self, debug=False):
        self._isrunning = True

        while self._isrunning:
            tmp = datetime.now()

            self._rules.process()

            tmp = self._option["sleep"] - (datetime.now() - tmp).total_seconds()
            print("time left", tmp, self._option["sleep"])
            if tmp > 0:
                time.sleep(tmp)
            elif tmp < 0:
                self._logger.warning ("It's hard to execute for fcore in proper time. " + str(tmp * -1) + " seconds were over.")
                # self._rules.makealert ()
            if debug:
                break

        self._rules.finalize()

    
if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage : python fcore.py [start|stop|restart|run|debug|sched|{ruleid}]")
        sys.exit(2)

    mode = sys.argv[1]
    if 'sched' == mode:
        util.setprocname("fcore_sched")
        runner = FCore('conf/fcore.json', util.getdefaultlogger(), mode)
    else:
        util.setprocname("fcore")
        try:
            rid = int(mode)
            mode = 'debug'
            runner = FCore('conf/fcore.json', util.getdefaultlogger(), mode, rid)
        except:
            runner = FCore('conf/fcore.json', util.getdefaultlogger(), mode)

    adaemon = Daemon(runner.getdname(), runner)
    if 'start' == mode:
        adaemon.start()
    elif 'stop' == mode:
        adaemon.dstop()
    elif 'restart' == mode:
        adaemon.restart()
    elif 'run' == mode:
        adaemon.run()
    elif 'debug' == mode:
        adaemon.run(True)
    elif 'sched' == mode:
        adaemon.setfilelogger()
        adaemon.run(True)
    else:
        print("Unknown command")
        sys.exit(2)
    sys.exit(0)
