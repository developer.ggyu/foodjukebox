#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
from builtins import object

import json
import pymysql
import psutil
import time
import socket
import traceback
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from datetime import datetime, timedelta

import util
from code import VarCode, RetCode, AlertCode
from result import ProcResult, AlertResult, RuleResult
from variable import Variable
from datamng import DBManager, DPManager
from device import DeviceInfo

class Rule:
    _MAX_PRIORITY = 7
    _DEF_PRIORITY = 6
    _DEF_PERIOD = 60

    def __init__(self):
        self._id = None
        self._name = None
        self._used = False
        self._deleted = True
        self._executed = 0
        self._tmpperiod = None
        self._outputs = {}
        self._conf = {"basic" : [], "advanced" : [], "timespan": {"id" : 0, "used" : [True]}}

    def _setbase(self, base):
        self._id = base["id"]
        self._name = base["name"]
        self._used = (base["used"] > 0)
        self._deleted = (base["deleted"] > 0)
        self._mode = base["mode"]
        self._constraints = None
        self._configurations = None
        self._controllers = None
        self._outputs = None

    def __repr__(self):
        return "{} {}".format(self._id, self._name)

    def getruledataid(self, idx, outcode):
        return 30000000 + self._id * 10000 + idx * 100 + outcode

    def loadfromschema(self, schema, datamng):
        self._setbase(schema)
        if "fieldid" in schema: self._fieldid = schema["fieldid"]
        if "sched" in schema: self._sched= schema["sched"]

        if "constraints" in schema: self._constraints = schema["constraints"]
        if "configurations" in schema: self._conf = schema["configurations"]
        if "controllers" in schema: self._controllers = schema["controllers"]
        if "outputs" in schema: self._outputs = schema["outputs"]

        if "inputs" in schema:
            inputs = schema["inputs"]
        else:
            inputs = []

        inputs.extend(self.getdevconfig(datamng))

        self.setvariables(inputs, datamng)

    def loadfromdb(self, row, sched, datamng):
        self._setbase(row)

        if row["used"] == 0 or row["deleted"] > 0 or row["mode"] > 0:
            return 

        self._fieldid = row["field_id"]
        self._constraints = json.loads(row["constraints"])
        self._conf = json.loads(row["configurations"])

        if sched and "sched" in row:
            self._sched = row["sched"]
        else:
            self._sched = False

        self._controllers = json.loads(row["controllers"])
        for proc in self._controllers['processors']:
            proc["pvalues"] = None

        self._outputs = json.loads(row["outputs"])

        if row["inputs"] is not None:
            inputs = json.loads(row["inputs"])
        else:
            inputs = []

        inputs.extend(self.getdevconfig(datamng))

        self.setvariables(inputs, datamng)

    def getdevconfig(self, datamng):
        configs = []

        if self._constraints is None or "devices" not in self._constraints:
            return configs

        for dev in self._constraints["devices"]:
            if "inputs" not in dev:
                continue
            devstr = dev["class"] + "/" + dev["type"]
            keys = DeviceInfo.getconfig(devstr) 
            for key in keys:
                tmp = dev["inputs"]["key"] + "." + DeviceInfo.getconfigname(key)
                configs.append({"key" : tmp, "name" : tmp, "dataid" : datamng.getdataidfordevconf(dev["deviceid"], key)})
        return configs

    def setvariables(self, inputs, datamng):
        dataids = {}
        outids = {}
        kv = {}
        for datum in self._conf["basic"]:
            if "type" in datum:
                if datum["type"] == "table":
                    for k, v in datum["values"].items():
                        kv[datum["key"] + "." + k] = Variable(v, vcode=VarCode.LIST)
                elif datum["type"] in ("ts_float", "ts_time", "ts_check"):
                    kv[datum["key"]] = Variable(datum["value"], vcode=VarCode.TSPAN)
                elif datum["type"] == "string":
                    kv[datum["key"]] = Variable(datum["value"]) #, vcode=VarCode.STRING)  # STRING is same with NORM
                elif datum["type"] == "line":
                    pass
                elif "value" in datum:
                    kv[datum["key"]] = Variable(datum["value"])
                else:
                    kv[datum["key"]] = Variable()
            else:
                if "value" in datum:
                    kv[datum["key"]] = Variable(datum["value"])
                else:
                    kv[datum["key"]] = Variable()
                
        for datum in self._conf["advanced"]:
            kv[datum["key"]] = Variable(datum["value"])

        if "priority" not in kv:
            kv["priority"] = Variable(Rule._DEF_PRIORITY)
            self._priority = Rule._DEF_PRIORITY
        else:
            priority = kv["priority"].getvalue()
            if priority < 0: priority = 0
            if priority > Rule._MAX_PRIORITY: priority = Rule._DEF_PRIORITY
            self._priority = priority

        if "period" not in kv:
            kv["period"] = Variable(Rule._DEF_PERIOD)

        if "multicond" in self._conf: # 다중작동규칙
            self._conf["multicond"]["data"] = datamng.getdata(self._conf["multicond"]["dataid"])
        else:
            self._conf["multicond"] = None
            
        for datum in inputs:
            if "count" in datum:
                val = datamng.getdata(datum["dataid"], datum["count"], fldid=self._fieldid)
            else:
                val = datamng.getdata(datum["dataid"], fldid=self._fieldid)

            print(datum, val)
            if val:
                kv[datum["key"]] = val
                dataids[datum["key"]] = datum["dataid"]

        if self._outputs is not None and "data" in self._outputs:
            for out in self._outputs["data"]:
                dataid = self.getruledataid(0, out["outcode"])
                kv[out["outputs"]] = datamng.getdata(dataid)
                dataids[out["outputs"]] = dataid
                if out["outputs"] in outids:
                    outids[out["outputs"]].append(dataid)
                else:
                    outids[out["outputs"]] = [dataid]

        if self._outputs is not None and "field" in self._outputs:
            for out in self._outputs["field"]:
                dataid = datamng.getdataidforfield(self._fieldid, out["outcode"])
                kv[out["outputs"]] = datamng.getdata(dataid)
                dataids[out["outputs"]] = dataid
                if out["outputs"] in outids:
                    outids[out["outputs"]].append(dataid)
                else:
                    outids[out["outputs"]] = [dataid]

        if self._outputs is not None and "dev" in self._outputs:
            for out in self._outputs["dev"]:
                devid = self.finddeviceid(out["targets"])
                dataid = datamng.getdataidfordev(devid, out["outcode"])
                kv[out["outputs"]] = datamng.getdata(dataid)
                dataids[out["outputs"]] = dataid
                if out["outputs"] in outids:
                    outids[out["outputs"]].append(dataid)
                else:
                    outids[out["outputs"]] = [dataid]

        if self._outputs is not None and "req" in self._outputs:
            for out in self._outputs["req"]:
                if out["targets"] == "all":
                    out["targets"] = self.getalloutputdevices()

        self._inputs = kv
        self._dataids = dataids
        self._outids = outids

    def isaschedulerule(self):
        return self._sched

    def getpriority(self):
        return self._priority

    def getid(self):
        return self._id

    def isavailable(self):
        return self._used is True and self._deleted is False and self._mode == 0

    def updateinputs(self, datamng):
        for key, did in self._dataids.items():
            self._inputs[key] = datamng.getdata(did, fldid=self._fieldid)

    def updatetemporaryinputs(self, datadic):
        self._inputs.update(datadic)

    def updateinputsbydatadic(self, items):
        if items:
            self._inputs.update(items)

    def settemporaryperiod(self, period):
        self._tmpperiod = period

    def getexecutabletime(self):
        if self._tmpperiod:
            ret = self._executed + self._tmpperiod
            self._tmpperiod = None
            return ret
        else: 
            return self._executed + self._inputs["period"].getvalue()

    def getname(self):
        return self._name

    def executed(self, tm):
        self._executed = tm

    def finddeviceid(self, name):
        if "devices" not in self._constraints:
            return None
        for dev in self._constraints["devices"]:
            if "outputs" in dev and dev["outputs"] == name and "deviceid" in dev and dev["deviceid"] != "":
                return dev["deviceid"]
        return None

    def getalloutputdevices(self):
        if "devices" not in self._constraints:
            return []
        ret = []
        for dev in self._constraints["devices"]:
            if "outputs" in dev and "deviceid" in dev and dev["deviceid"] != "":
                ret.append (dev["outputs"])
        print("all output devices", ret)
        return ret

    def getinputs(self):
        return self._inputs

    def getoutputs(self):
        return self._outputs

    def checkmulticondition(self):
        if self._conf["multicond"] is None:
            return True

        value = self._conf["multicond"]["data"].getvalue()
        cond = self._conf["multicond"]["cond"]

        if cond["type"] == "range":
            if "from" in cond and cond["from"] > value:
                return False
            if "to" in cond and cond["to"] <= value:
                return False
            return True

        elif cond["type"] == "equal":
            if "value" in cond and cond["value"] == value:
                return True
            return False

        return False

    def isavailableontimespan(self, tsidx):
        if "used" in self._conf["timespan"]:
            print("ontimespan", tsidx, self._conf["timespan"]["used"])
            return self._conf["timespan"]["used"][tsidx]
        return True

    def gettimespanid(self):
        return self._conf["timespan"]["id"]

    def gettimespan(self):
        if "timespan" in self._conf["timespan"]:
            return self._conf["timespan"]["timespan"]
        return None

    def getfieldid(self):
        return self._fieldid

    def settimespanindex(self, tsidx):
        self._tsidx = tsidx

    def gettimespanindex(self):
        return self._tsidx

    def getdataid(self, key):
        if key in self._dataids:
            return self._dataids[key]
        return None

    def getoutids(self, key):
        if key in self._outids:
            return self._outids[key]
        return []

    def getcontrollers(self):
        return self._controllers

class DefaultRuleFactory:
    _rules = [{
        "id" : 901, "name" : "데이터임계치체크", "used" : 1, "deleted" : 0, "mode" : 0, "fieldid" : 0,
        "sched" : 1, 
        "controllers" : {"processors" : [{"type" : "dc"}]}
    }]

    def getdefaultrules(datamng, guide):
        rules = []
        if "disallows" in guide:
            for schema in DefaultRuleFactory._rules:
                if schema["id"] in guide["disallows"]:
                    continue
                rule = Rule()
                rule.loadfromschema(schema, datamng)
                rules.append(rule)
        elif "allows" in guide:
            for schema in DefaultRuleFactory._rules:
                if schema["id"] in guide["allows"]:
                    rule = Rule()
                    rule.loadfromschema(schema, datamng)
                    rules.append(rule)
        else:
            for schema in DefaultRuleFactory._rules:
                rule = Rule()
                rule.loadfromschema(schema, datamng)
                rules.append(rule)
        return rules

if __name__ == '__main__':
    pass

