#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 FarmOS, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
import datetime
import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from result import RetCode
from variable import Variable
from sklearn.preprocessing import MinMaxScaler
from pickle import load
import tensorflow as tf
import numpy as np

def AIpredict(inputs, pvalues, dbcur):
    query = "select data_id, nvalue, date_format(obs_time, '%%Y-%%m-%%d %%H:%%i') tm, timestampdiff(MINUTE, %s, obs_time) idx from observations where data_id in %s and obs_time > %s order by obs_time asc"

    tm = datetime.datetime.now() - datetime.timedelta(minutes=180)
    keys = ["#ltwin2", "#rtwin2", "#sunscr2", "#thmscr2", "#fan0", "#co2val0", "#fog0", "#drain0", "#heatval0", "#heattemp1", "#outtemp1", "#outhum1", "#outrad1", "#outws1", "#outwd1", "#outrain1", "#intemp1", "#inhum1", "#inco21"]

    ids = []
    for k in keys:
        ids.append (inputs[k].getid())

    #print (query, [ids, tm])

    rows = dbcur.select(query, [tm, ids, tm])
    if rows is None:
        return (RetCode.PROCESSOR_EXCEPTION, None)

    midnight = datetime.datetime.combine(datetime.date.today(), datetime.datetime.min.time())
    data = np.zeros(shape=(180, 20))
    for row in rows:
        #print (row)
        data[row["idx"]][0] = (datetime.datetime.strptime(row['tm'], "%Y-%m-%d %H:%M") - midnight).total_seconds() / 60 / 1440.0
        data[row["idx"]][ids.index(row['data_id']) + 1] = row['nvalue']

    X_scaler = load(open('modules/kist-ai/results/X_scaler.pkl', 'rb'))
    Y_scaler = load(open('modules/kist-ai/results/Y_scaler.pkl', 'rb'))
    load_model = tf.keras.models.load_model('modules/kist-ai/CNN_model-2')

    X_test = X_scaler.transform(data)
    X_test = X_test.reshape(1, X_test.shape[0], X_test.shape[1], 1)
    y_pred = load_model.predict(X_test)

    y_pred_unscaled = Y_scaler.inverse_transform(y_pred)

    values = [Variable(), Variable(), Variable()]
    values[0].setvalue(float(y_pred_unscaled[0][0]), datetime.datetime.now() + datetime.timedelta(minutes=30))
    values[1].setvalue(float(y_pred_unscaled[0][1]), datetime.datetime.now() + datetime.timedelta(minutes=30))
    values[2].setvalue(float(y_pred_unscaled[0][2]), datetime.datetime.now() + datetime.timedelta(minutes=30))

    return (RetCode.OK, values)

if __name__ == '__main__':
    pass
