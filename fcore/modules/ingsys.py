#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 FarmOS, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
from datetime import datetime

import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from result import RetCode
from variable import Variable
import farmos_util

def schedulenutri(inputs, pvalues, dbcur):
    """
    inputs : table.time, table.worktime, table.areabit, table.plainwater, table.ec, table.ph
    outputs : cmd, worktime, areabit, ec, ph, nextime
    """

    if farmos_util.isproperweekday(inputs) is False:
        return (RetCode.NOT_PROPER_TIME, None)

    idx = farmos_util.ispropertime(inputs)
    if idx is None:
        return (RetCode.NOT_PROPER_TIME, None)

    if inputs["#table.used"].getvalue()[idx] is False:
        return (RetCode.NOT_PROPER_TIME, None)

    if inputs["#table.plainwater"].getvalue()[idx] is False:
        cmd = CmdCode.SPARAMED_WATERING.value
    else:
        cmd = CmdCode.SAREA_WATERING.value
    worktime = inputs["#table.worktime"].getvalue()
    areabit = inputs["#table.areabit"].getvalue()
    ec = inputs["#table.ec"].getvalue()
    ph = inputs["#table.ph"].getvalue()

    times = inputs["#table.time"].getvalue()
    if idx == len(times) - 1: # idx is the last index
        nextime = None
    else:
        nexttime = times[idx + 1]
            
    return (RetCode.OK, [cmd, worktime[idx], int(areabit[idx]), ec[idx], ph[idx], nextime])

def _generatecmd(inputs, cmd, tsaccrad, lastsupply = None):
    tsidx = inputs["tsidx"].getvalue()
    if cmd == CmdCode.PARAMED_WATERING.value:
        lastsupply = time.time()

    return (RetCode.OK, [cmd,
        inputs["#worktime"].getvalue(tsidx),
        inputs["#sfield"].getvalue(tsidx),
        inputs["#efield"].getvalue(tsidx),
        inputs["#ec"].getvalue(tsidx),
        inputs["#ph"].getvalue(tsidx),
        lastsupply, tsaccrad, tsidx])

def accradnutri(inputs, pvalues, dbcur):
    """
    inputs : accrad, stdrad, worktime, sfield, efield, ec, ph, startsupply, limit, forcesupply
    outputs : cmd, worktime, sfield, efield, ec, ph, lastsupply, tsaccrad, lastts
    """
    tsidx = inputs["tsidx"].getvalue()
    if pvalues is None or len(pvalues) == 0:
        tsaccrad = 0
        lastsupply = None
    else: 
        if pvalues[-1] != tsidx:
            # 새로운 시간대 진입
            tsaccrad = 0
            if inputs["#startsupply"].getvalue(tsidx) is True:
                return _generatecmd(inputs, CmdCode.PARAMED_WATERING.value, tsaccrad)
        else:
            tsaccrad = pvalues[-2]
        lastsupply = pvalues[-3]

    print (tsaccrad)
    tsaccrad = tsaccrad + inputs["#accrad"].getdelta()
    print (tsaccrad)

    if lastsupply is not None:
        force = inputs["#forcesupply"].getvalue(tsidx)
        if force > 0 and force * 60 < time.time() - lastsupply:
            # 강제 관수시간에 진입
            return _generatecmd(inputs, CmdCode.PARAMED_WATERING.value, tsaccrad)

    if tsaccrad > inputs["#stdrad"].getvalue():
        # 누적일사 충족
        if lastsupply is None:
            # 마지막 관수시간을 알수 없어서 일단 관수. 누적일사는 0으로 리셋
            tsaccrad = 0
            return _generatecmd(inputs, CmdCode.PARAMED_WATERING.value, tsaccrad)

        limit = inputs["#limit"].getvalue(tsidx)
        if limit > 0 and limit * 60 < time.time() - lastsupply:
            # 관수제한시간 지나서 관수 가능. 누적일사는 0으로 리셋
            tsaccrad = 0
            return _generatecmd(inputs, CmdCode.PARAMED_WATERING.value, tsaccrad)

    return _generatecmd(inputs, None, tsaccrad, lastsupply)

if __name__ == '__main__':
    pass
