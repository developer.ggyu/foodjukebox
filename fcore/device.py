#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 FarmOS, Inc.
# All right reserved.
#

# Device 관련한 정적인 정보를 다룸
class DeviceInfo:
    inputkey = {0: "stat", 1: "obs", 2:"pos", 3:"shtm", 4:"retm", 
                5:"rat", 6:"ctrl", 7:"area", 8:"alt", 9:"raw",
                10: "img", 11:"opr", 12:"est", 13:"nst",
                21: "vcnt", 22:"vavg", 23:"vmax", 24:"vmin", 25:"vstd", 26:"dstd"}
    configkey = {1: "tirr"}

    inputinfo = { 
        "node" : [0, 6],
        "sensor" : [0, 1, 9, 12, 21, 22, 23, 24, 25, 26],
        "actuator/switch" : [0, 3, 4, 5],
        "actuator/retractable/level2" : [0, 2, 3, 4],
        "actuator/nutrient-supply" : [0, 4, 7, 8],
        "actuator/controller" : [0]
    }
    configinfo = {
        "actuator/nutrient-supply" : [1]
    }

    def getinputname(key):
        if key in DeviceInfo.inputkey:
            return DeviceInfo.inputkey[key]

    def getconfigname(key):
        if key in DeviceInfo.configkey:
            return DeviceInfo.configkey[key]

    def getinput(devicestr):
        for key, val in DeviceInfo.inputinfo.items():
            if key == devicestr[:len(key)]:
                return val
        return []
        
    def getconfig(devicestr):
        for key, val in DeviceInfo.configinfo.items():
            if key == devicestr[:len(key)]:
                return val
        return []
        

