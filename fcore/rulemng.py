#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 FarmOS, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
from builtins import object

import json
import pymysql
import psutil
import time
import socket
import traceback
import paramiko
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from datetime import datetime, timedelta

import util
import processors
from rule import Rule, DefaultRuleFactory
from code import VarCode, RetCode, AlertCode, AlertGrade
from result import ProcResult, AlertResult, RuleResult
from mblock import Request, CmdCode, Notice, NotiCode
from variable import Variable
from managers import TimeSpanManager, DeviceManager
from datamng import DBManager, DPManager

class RuleManager:
    def __init__(self, option, logger, mode, rid=-1):
        self._option = option
        self._logger = logger
        self._sftp = None

        if "remote" in option:
            self._sftp = self.connectsftp()
            option["sftp"] = self._sftp

        if "dp" in option:
            self._datamng = DPManager(option, logger)
            farmids = self._datamng.getfarmids()
        else:
            self._datamng = DBManager(option, logger)
            farmids = [1]

        self._frulemng = []
        for fid in farmids:
            self._frulemng.append(FarmRuleManager(self._datamng, option, logger, mode, rid, fid))
            
    def connectsftp(self):
        try:
            opt = self._option["remote"]
            transport = paramiko.Transport(opt["host"], 22)
            transport.connect(username = opt["user"], password = opt["password"])
            return paramiko.SFTPClient.from_transport(transport)
        except Exception as ex:
            self._logger.warn("Fail to connect SFTP : " + str(ex) + str(self._option))
            return None

    def process(self):
        for frmng in self._frulemng:
            frmng.process()

    def finalize(self):
        self._datamng.close()
        if self._sftp:
            self._sftp.close()

class FarmRuleManager:
    def __init__(self, datamng, option, logger, mode, rid, farmid):
        self._option = option
        self._logger = logger
        self._datamng = datamng
        self._farmid = farmid
        self._timespan = TimeSpanManager(datamng, option, logger, farmid)
        self._devices = DeviceManager(datamng, option, logger, farmid)
        self._lastupdated = 0
        self._mode = mode
        self._sched = ("sched" == mode)
        self._prireqtimer = {}

        self._serial = datamng.loadserial(farmid)

        #self._rid = rid
        if "rules" not in option:
            if rid == -1:
                self._option["rules"] = {"disallows" : []}
            else:
                self._option["rules"] = {"allows" : [rid]}
        else:
            if "disallows" in option["rules"] and "allows" in option["rules"]:
                del self._option["rules"]["allows"]     # disallows 가 우선합니다.
            if "disallows" not in option["rules"] and "allows" not in option["rules"]:
                self._option["rules"] = {"disallows" : []}

        self._rules = []
        for _ in range(Rule._MAX_PRIORITY):
            self._rules.append ({})
        self.loaddefaultrules()

        self._procs = {
            "int" : processors.InternalProcessor(logger),
            "dc" : processors.DataCheckProcessor(logger),
            "mod" : processors.ModuleProcessor(logger),
            "ext" : processors.ExternalProcessor(logger),
            "eq" : processors.EquationProcessor(logger),
            "ai" : processors.AIProcessor(logger)
        }
        self._procs["int"].setup(self._datamng, self._devices, self._timespan)
        self._procs["dc"].setup(self._datamng, self._devices, self._timespan)

    def process(self):
        self._datamng.loadpostfixes(self._farmid)
        self._logger.info ("Rule processing for Farm " + str([self._farmid, self._serial]))
        updated = self._lastupdated
        if self._datamng.getnumberofrules(self._sched, self._option["rules"], self._farmid) == 0:
            self._logger.info ("No rule for processing.")
            return

        self._devices.loaddevices(updated)
        self._datamng.loaddata(self._farmid)
        self._timespan.updatetimespans()
        self.loadappliedrules(updated)
        self.processrules()
        self._datamng.writedata()
        self._lastupdated = int(time.time()) - 30

    def getpriorityfromruleset(self, ruleid):
        # find
        for idx in range(Rule._MAX_PRIORITY):
            if ruleid in self._rules[idx]:
                return idx
        return -1

    def loaddefaultrules(self):
        for rule in DefaultRuleFactory.getdefaultrules(self._datamng, self._option["rules"]):
            self.updateruleset(rule)

    def updateruleset(self, rule):
        ruleid = rule.getid()

        found = self.getpriorityfromruleset(ruleid)
        if found >= 0: # should be deleted from ruleset
            del self._rules[found][ruleid]

        if rule.isavailable():
            priority = rule.getpriority()
            self._rules[priority][ruleid] = rule

    def loadappliedrules(self, updated):
        self._timespan.loadtimespan(updated)

        print("loadappliedrules", updated)
        rows = self._datamng.loadappliedrules(updated, self._sched, self._option["rules"], self._farmid)

        for row in rows:
            try:
                # temporarily change for one rule test
                if "debug" == self._mode and "allows" in self._option["rules"] and len(self._option["rules"]["allows"]) == 1:
                    row["used"] = 1
                    row["deleted"] = 0
      
                rule = Rule()
                rule.loadfromdb(row, self._sched, self._datamng)
                self.updateruleset(rule)

            except Exception as ex:
                self._logger.warn ("Fail to load a rule : " + str(ex) + " " + str(row))
                self._logger.warn(traceback.format_exc())

    def processrules(self):
        ret = []
        now = int(time.time())
        reqs = {}                    # {devid : (dev, req)}
        datadic = {}
        alerts = []

        print("now", now, self._rules)
        for rules in self._rules:
            for rid, rule in rules.items():
                try:
                    print("rule", rule.getname(), rule.getexecutabletime(), now)
                    if rule.getexecutabletime() < now:      # if executable time was passed .
                        self._logger.info(rule.getname() + " is executing.")
                        rule.updateinputs(self._datamng)

                        ruleret = self.executerule(rule)

                        # 우선순위상 먼저 만들어진 Request 를 살리고, 새로 만들어진 Request는 무시함.
                        ruleret.updaterequests(reqs)

                        reqs = ruleret.getrequests()
                        self._logger.info("Requests for devices " + str(reqs.keys()) + " are ready.")

                        datadic.update(ruleret.getdatadic())
                        self._logger.info("Data for rules " + str(datadic.keys()) + " are ready.")

                        alerts.extend(ruleret.getalerts())
                        self._logger.info("Alerts for rules " + str(len(alerts)) + " are ready.")

                        ret.append({"ruleid": rid, "result": ruleret})
                        rule.executed(now)
                        self._logger.info(rule.getname() + " is executed. The result is " + str(ruleret))

                except Exception as ex:
                    self._logger.warn ("Fail to execute a rule : " + str(rid) + " " + str(ex))
                    self._logger.warn(traceback.format_exc())
                    ret.append({"ruleid": rid, "result": {"code" : RetCode.UNKNOWN_ERROR}})
                    #alerts.append(AlertResult(AlertCode.FOS_AUTO_FAIL, AlertGrade.MID_WARNING, json.dumps({"rule": rule.getname()}), rule.getfieldid(), None, None))

        # sending requests
        for devid, vals in reqs.items():
            dev = self._devices.getdevice(devid)
            # 기존 명령을 생성한 작동규칙이 이제 유효하지 않거나 새로운 명령의 우선순위가 더 높다면 (더 작은 숫자라면) 명령전송
            if devid not in self._prireqtimer or self._prireqtimer[devid][0] < now or self._prireqtimer[devid][1] > vals[1].getpriority():
                self.sendrequest(dev, vals[0])
                self._prireqtimer[devid] = (vals[1].getexecutabletime(), vals[1].getpriority())
            else:
                self._logger.warn("Privious or Upper Priority Rule is working. New request from [" + vals[1].getname() + "] would not be sent." + str(vals[0]))

        # sending data notice
        self.sendnotice(datadic)

        # writing alerts
        for alert in alerts:
            alert.setfarmid(self._farmid)
            self._datamng.writealert(alert)

        return ret

    def makealerts(self, rule, datadic):
        alerts = []
        outputs = rule.getoutputs()
        if outputs is None or "alert" not in outputs:
            return alerts

        for out in outputs["alert"]:
            acode = datadic[out["alert_code"]]
            grade = datadic[out["grade"]]
            info= datadic[out["info"]]
            devid = datadic[out["device_id"]]
            hwid = datadic[out["hwid"]]
            fldid = datadic[out["field_id"]]
            alerts.append(AlertResult(acode, grade, info, fldid, hwid, devid, self._farmid))

        return alerts

    def makeonerequest(self, dev, cmd, params, out):
        if dev and "nodeid" in dev and dev["nodeid"]:
            req = Request(dev["nodeid"]) 
            if "force" in out and out["force"] is True:
                req.setcommand(dev["id"], cmd, params, force=True)
            else:
                req.setcommand(dev["id"], cmd, params)
            return req
        else:
            self._logger.warn("Not enough infomation of a device for making a request .", dev)
            return None

    def sendrequest(self, dev, req):
        topic = "/".join(["cvtgate", dev["coupleid"], str(dev["gateid"]), "req", str(dev["nodeid"])])
        if dev["control"] in [0, 2] or req.isforce(): # normal or rule or force
            ret = publish.single(topic, payload=req.stringify(), qos=2, hostname=self._option["mqtt"]["host"])
            self._logger.info("Sent a request to " + topic + " : " + req.stringify() + str(dev["control"]) + " " + str(req.isforce()))
        else:
            self._logger.info("A request is not sent to " + topic + " because of control [" + str(dev["control"]) +"] : " + req.stringify())

    def sendnotice(self, data):
        # 데이터가 없거나 디피라면 노티스를 전송하지 않는다. 
        # 추후 DP에서 경보이외의 값을 생성한다면 수정되어야 한다.
        if len(data) == 0 or self._serial is None:
            return
        topic = "/".join(["cvtgate", self._serial, '', "noti", ''])
        noti = Notice(None, NotiCode.FCORE_DATA)
        for key, value in data.items():
            #noti.setkeyvalue(key, value.getvalue())
            noti.setkeyvalue(value.getid(), value.getvalue())
        ret = publish.single(topic, payload=noti.stringify(), qos=2, hostname=self._option["mqtt"]["host"])
        self._logger.info("Sent a notice to " + topic + " : " + noti.stringify())

    def makerequests(self, rule, datadic):
        """
            return : dict(key is device id, value is request)
        """
        reqs = {}
        outputs = rule.getoutputs()
        if outputs is None or "req" not in outputs:
            return {} 

        for out in outputs["req"]:
            if out["cmd"] not in datadic:
                self._logger.info("A command is not found. Therefore, it would not make a request. " + str(out))
                continue

            cmd = datadic[out["cmd"]].getvalue()
            if cmd is None:
                self._logger.info("A command is None. Therefore, it would not make a request. " + str(out))
                continue

            params = {}
            inputs = rule.getinputs()
            if "params" in out and len(out["params"]) > 0:
                for idx in range(len(out["params"])):
                    param = out["params"][idx]
                    if "pnames" in out:
                        pname = out["pnames"][idx]
                    else:
                        pname = param[1:]

                    if isinstance(param, (int, float)):
                        params[pname] = param

                    elif param in datadic:
                        params[pname] = datadic[param].getvalue()   # remove '#'

                    elif param in inputs:
                        params[pname] = inputs[param].getvalue()   # remove '#'

                    else:
                        print("param (", param, ") is not found. Do not make a request.")
                        break
                        
                if idx < len(out["params"]) -1:
                    continue

            print("rule outputs", outputs)
            for target in out["targets"]:
                devid = rule.finddeviceid(target)
                print("target", target, devid)
                if devid is None:
                    newtarget = target + str(int(datadic[target].getvalue()))
                    print("finddeviceid", newtarget)
                    devid = rule.finddeviceid(newtarget)
                    if devid is None:
                        print("No proper device for ", newtarget)
                        continue

                dev = self._devices.getdevice(devid)
                req = self.makeonerequest(dev, cmd, params, out)
                if req:
                    reqs[devid] = (req, rule)

        return reqs

    def checktrigger(self, rule):
        ctrl = rule.getcontrollers()
        if 'trigger' not in ctrl:
            return ProcResult(RetCode.OK)
        else:
            try:
                procret = self._procs[ctrl["trigger"]["type"]].evaluate(rule, ctrl["trigger"], None)
                if procret.getretcode() != RetCode.OK:
                    self._logger.info(rule.getname() + " trigger is failed : " + str(procret.getretcode()))

                elif procret.getvalues()[0] == 0:
                    procret.setretcode(RetCode.TRIGGER_NOT_ACTIVATED)
                    self._logger.info(rule.getname() + " trigger is not activated.")

            except Exception as ex:
                self._logger.warn(rule.getname() + " trigger is failed to execute. " + str(ex))
                self._logger.warn(traceback.format_exc())
                return ProcResult(RetCode.PROCESSOR_EXCEPTION) # trigger is failed to execute

            return procret

    def executeprocessors(self, rule):
        """
        return (list of ProcResult)
        """
        self._logger.debug("execute processors of a rule : " + str(rule))

        ret = [self.checktrigger(rule)]

        if ret[0].getretcode() != RetCode.OK:
            return ret
            
        processors = rule.getcontrollers()["processors"]

        for proc in processors:
            try:
                print("exec proc", proc)
                procret = self._procs[proc["type"]].evaluate(rule, proc, self._datamng)
                print("executed", procret)
                if procret.getretcode() == RetCode.OK:
                    rule.updatetemporaryinputs(procret.getoutputs())
                    rule.settemporaryperiod(procret.getperiod())
                proc["pvalues"] = procret.getvalues()

            except Exception as ex:
                self._logger.warn("Processors (" + str(proc) + ") is failed to execute. " + str(ex))
                self._logger.warn(traceback.format_exc())
                procret = ProcResult(RetCode.PROCESSOR_EXCEPTION, alerts=[AlertResult(AlertCode.FOS_AUTO_FAIL, AlertGrade.MID_WARNING, json.dumps({"rule": rule.getname(), "msg": rule.getname() + " 실행시 오류발생"}, ensure_ascii=False), rule.getfieldid(), None, None)])

            ret.append(procret)

        return ret


    def executerule(self, rule):
        """
        return
           { "results of processes", "requests", "new data"}
        """
        # 다중작동규칙 작동조건 체크
        if rule.checkmulticondition() is False:
            return RuleResult([ProcResult(RetCode.MULTIRULE_NOT_ACTIVATED)])
                
        # 시간대 체크
        tsid = rule.gettimespanid()
        fldid = rule.getfieldid()

        if tsid == -1: # self timespan
            # add self timespan
            self._timespan.addtimespan(tsid, fldid, rule.gettimespan())

        (tsidx, nsec, times) = self._timespan.getcurrentindex(tsid, fldid)
        if tsidx is None:
            self._logger.warn(rule.getname() + " is failed to get timespan.")
            return RuleResult([ProcResult(RetCode.WRONG_TIMESPAN)])

        if rule.isavailableontimespan(tsidx) is False:
            self._logger.info(rule.getname() + " is not used now. (" + str(tsidx) + ")")
            return RuleResult([ProcResult(RetCode.NOT_USED)])

        rule.settimespanindex(tsidx)
        times["#nsec"] = Variable(nsec)
        rule.updateinputsbydatadic(times)

        # 시간대 임계치 읽어오기
        thresholds = self._timespan.getthresholds(tsid, fldid, tsidx, nsec)
        rule.updateinputsbydatadic(thresholds)

        # 프로세서 실행
        procrets = self.executeprocessors(rule)

        # 프로세서 결과 처리
        datadic = {}
        alerts = []
        for idx, ret in enumerate(procrets):
            # update processors' status on data manager
            dataid = rule.getruledataid(idx, 0)
            self._datamng.updatedata(dataid, ret.getretcode().value)

            # update results of processors
            if ret.getretcode() == RetCode.OK:
                for key, var in ret.getoutputs().items():
                    datadic[key] = var
                    for dataid in rule.getoutids(key):
                        var.setid(dataid)
                        self._datamng.updatedatavariable(dataid, var)

            # extract alerts
            alerts.extend(ret.getalerts())

        reqs = self.makerequests(rule, datadic)
        alerts.extend(self.makealerts(rule, datadic))

        return RuleResult(procrets, reqs, datadic, alerts)

if __name__ == '__main__':
    ts = TimeSpanManager({}, util.getdefaultlogger())
    ts.addtimespan(1, 1, [{"to" : 10000}])
    assert ts.getcurrentts((0, 0)) == 0, "timespan is not matched #1"
    now = datetime.now()
    nsec = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()
    ans = 0 if nsec < 10000 else -1
    assert ts.getcurrentts((1, 1)) == ans, "timespan is not matched #2" 

    rulemng = RuleManager({}, util.getdefaultlogger())
